<?php 

function set_session($username){
	$data = array();
	$CI = & get_instance();
	$CI->load->library('nativesession');
	$CI->load->model('M_login');
	// $username = $CI->nativesession->get('username');
	$user_info = $CI->M_login->get_info($username);

    $data = array(
        'LOGIN'             => TRUE,
        'id_employee'       => $user_info->id,
        'id_group'          => $user_info->id_group,
        'group_name'        => $user_info->group_name,
        'group_suppervisor' => $user_info->group_suppervisor,
        'id_div'            => $user_info->id_div,
        'div_name'          => $user_info->div_name,
        'div_identity'      => $user_info->div_identity,
        'group_level'       => $user_info->group_level,
    	'nama_user'			=> $user_info->nama,
    	'photo'				=> $user_info->photo,
        'npp'               => $user_info->npp
    );

    foreach ($data as $row => $value) {
        $CI->nativesession->set($row, $value);
    }
}

function get_session_login(){
    $data   = array();
    $CI     = & get_instance();
    $data   = array(
        "id_div"    => $CI->nativesession->get("id_div"),
        "id_group"  => $CI->nativesession->get("id_group")
    );
    return $data;
}

function limit_to_numwords($string, $numwords) {
    $excerpt = explode(' ', $string, $numwords + 1);
    if (count($excerpt) >= $numwords) {
        array_pop($excerpt);
    }
    $excerpt = implode(' ', $excerpt);
    return $excerpt;
}

function gen_menu(){

	$CI = & get_instance();
	$CI->load->library('nativesession');
	$CI->load->model('m_login');
	$CI->load->model('m_main');

	$group_level 	= $CI->nativesession->get('group_level');
    $menu_utama 	= $CI->m_main->generate_menu($group_level);
    
    $image_properties = array(
        'src'   => 'assets/photos/'.$CI->nativesession->get('photo'),
        'class' => 'img-circle',
        'width' => '55',
        'height'=> '55',
	);
    
	echo '<div class="navbar-default sidebar" role="navigation">';
    echo '<div class="sidebar-nav navbar-collapse">';
    echo '<ul class="nav" id="side-menu">';
    echo '<li class="headinfo" >
    		<div style="text-align: center; padding: 10px;">
    			 <div align="center">'.img($image_properties).' <div>'.$CI->nativesession->get('nama_user') .'<footer>('.$CI->session->userdata('username').')</footer></div></div>
    		</div>
          </li>';
    echo '<li class="header2"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>
            	'.strtoupper($CI->nativesession->get('group_name')).'<br>'.$CI->nativesession->get('div_name').'</li>';
    echo '<li class="header">NAVIGASI</li>';
    
    foreach ($menu_utama as $rows) {
        if($rows->parent != 1){ // check if menu has sub-menu
            echo "<li>";
            echo anchor($rows->url, $rows->nama);
            echo "</li>";
        } else {
            echo '<li>';
            echo '<a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> '.$rows->nama.'<span class="fa arrow"></span></a>';
            $sub_menu = $CI->m_main->get_submenu($rows->id_menu); // generate sub-menu
            foreach ($sub_menu as $rowz ) {
                echo '<ul class="nav nav-second-level">';
                echo '<li>';
                echo '<a href='.$rowz->url.'>'.$rowz->nama.'</a>';
                echo '</li>';
                echo '</ul>';
            }
            echo '</li>';
        }
    }
    echo '</ul>';
    echo '</div>';
    echo '</div>';
}

function insert_tujuan($values, $flag, $lastid){
    $CI = & get_instance();
    $CI->load->model('m_main');
    $arrValues = array();
    foreach ($values as $row) {
        $arrValues[] = '("'.$row[0].'", '.$flag.' ,'.$lastid.')';
    }
    $sql = 'INSERT INTO '.TBL_TUJUAN.' (id_div, flag_tujuan, id_surat) VALUES '.implode(',',$arrValues).';';

    $CI->m_main->insert($sql); // insert into database
}

function insert_attach($attachmentdata, $month, $lastid){
    $CI = & get_instance();
    $CI->load->model('m_main');
    $arrValues = array();
    foreach ($attachmentdata as $key => $value) {
        $arrValues[] = '('.$lastid.', "'.$key.'", '.$value.' ,'.$month.')';
    }
    $sql = 'INSERT INTO '.TBL_UPLOAD.' (id_surat, nama_file, ukuran_file, month) VALUES'.implode(',', $arrValues).';';
    $CI->m_main->insert($sql); // insert into database
}

function tujuan_terkirim($tujuan_surat, $flow_terkirim){
    $CI = & get_instance();
    $CI->load->model('m_main');
    $arrValues = array();
    $tujuan_surat = array_map("unserialize", array_unique(array_map("serialize", $tujuan_surat))); // filter duplicate value
    foreach ($tujuan_surat as $row) {
        $arrValues[] = '('.$flow_terkirim['id_surat'].', '.$flow_terkirim['id_status'].', '.$flow_terkirim['id_pengirim'].
        ', "'. $flow_terkirim['id_group'] .'", '.$row->id_div.' , "'.$flow_terkirim['tgl_approve'].
        '", '.$flow_terkirim['id_pengirim'].', '.$flow_terkirim['npp_pengirim'].', "'.$flow_terkirim['id_approval'].'", 
        "'.$flow_terkirim['sent_status'].'")';
    }
    $sql = 'INSERT INTO '.TBL_FLOW.' (id_surat, id_status, id_divisi, id_group, id_tujuan, tgl_approve, id_pengirim, npp_pengirim, id_approval, sent_status) VALUES'.implode(',', $arrValues).';';

    // echo $sql;

	$CI->m_main->insert($sql);
}   

function tujuan_terkirim_event($tujuan_surat, $flow_terkirim){
    $CI = & get_instance();
    $CI->load->model('m_main');
    $arrValues = array();
    $tujuan_surat = array_map("unserialize", array_unique(array_map("serialize", $tujuan_surat))); // filter duplicate value
    foreach ($tujuan_surat as $row) {
        $arrValues[] = '('.$flow_terkirim['id_surat'].', '.$flow_terkirim['id_status'].', '.$row->id_div.
        ', "'. $flow_terkirim['id_group'] .'", '.$row->id_div.' , "'.$flow_terkirim['tgl_approve'].
        '", '.$flow_terkirim['id_pengirim'].', '.$flow_terkirim['npp_pengirim'].', "'.$flow_terkirim['id_approval'].'", 
        "'.$flow_terkirim['sent_status'].'")';
    }
    $sql = 'INSERT INTO '.TBL_FLOW.' (id_surat, id_status, id_divisi, id_group, id_tujuan, tgl_approve, id_pengirim, npp_pengirim, id_approval, sent_status) VALUES'.implode(',', $arrValues).';';

	$CI->m_main->insert($sql);
}  

function tujuan_terkirim_pesan($tujuan_surat, $flow_terkirim){
    $CI = & get_instance();
    $CI->load->model('m_main');
    $arrValues = array();
    $tujuan_surat = array_map("unserialize", array_unique(array_map("serialize", $tujuan_surat))); // filter duplicate value
    foreach ($tujuan_surat as $row) {
        $arrValues[] = '('.$flow_terkirim['id_surat'].', '.$flow_terkirim['id_status'].', '.$flow_terkirim['id_pengirim'].
        ', "'. $flow_terkirim['id_group'] .'", '.$row->id_div.' , "'.$flow_terkirim['tgl_approve'].
        '", '.$flow_terkirim['id_pengirim'].', '.$flow_terkirim['npp_pengirim'].', "'.$flow_terkirim['id_approval'].'", 
        "'.$flow_terkirim['sent_status'].'")';
    }
    $sql = 'INSERT INTO '.TBL_FLOW.' (id_surat, id_status, id_divisi, id_group, id_tujuan, tgl_approve, id_pengirim, npp_pengirim, id_approval, sent_status) VALUES'.implode(',', $arrValues).';';

    // echo $sql;

	$CI->m_main->insert($sql);
} 

function tujuan_disposisi($tujuan_surat, $flow_terkirim){
    $CI = & get_instance();
    $CI->load->model('m_main');
    $arrValues = array();
    $tujuan_surat = array_map("unserialize", array_unique(array_map("serialize", $tujuan_surat))); // filter duplicate value
    foreach ($tujuan_surat as $row) {
        $arrValues[] = '('.$flow_terkirim['id_surat'].', '.$flow_terkirim['id_status'].', "'.$flow_terkirim['id_approval'].'", "'.$flow_terkirim['pesan'].'", "'.$row.'" , "'.$flow_terkirim['tgl_disposisi'].'" , '.$flow_terkirim['npp_disposisi'].', '.$flow_terkirim['div_tracking'].')';
    }
    $sql = 'INSERT INTO '.TBL_FLOW.' (id_surat, id_status, id_approval, pesan, id_tujuan, tgl_disposisi, npp_disposisi, div_tracking) VALUES'.implode(',', $arrValues).';';

    // echo $sql;

    $CI->m_main->insert($sql);
}

function get_atasan($param){
    // do {
        $val = recurrsive($param);
        return $val;
    // } while()
}

function recurrsive($value){
    $CI = & get_instance();
    $CI->load->model('M_inbox');
    $arrValues = array();

    if($value != null){
        $get_atasan = $CI->M_inbox->get_atasan($id_group)->row();
        $atasan = $get_atasan->group_suppervisor;
        $arrValues[] = $atasan;
        $result = implode(',', $arrValues);
        return $result; 
    } else {
        return 1;
    }
    
}

function recurrsive2($param){
	$CI = & get_instance();
    $CI->load->model('M_inbox');
    
	$get_atasan = $CI->M_inbox->get_atasan($param)->row();
    $atasan = $get_atasan->group_suppervisor;    

    return $atasan; 
	
}

$dos = array();

function recurrsive3($param){
    $arrVal = array();
    $arrVal[] = $param;
    $newparam = recurrsive2($param);

    if(!empty($newparam)){
        //while(!empty($newparam)){
            recurrsive3($newparam);    
        //}
        
    } 
    //print_r($data);
    return $arrVal;

    //return $data;
}

function get_parents ($pid, $found = array()) {
	array_push ($found, $pid);
   	$CI = & get_instance();
    $CI->load->model('M_inbox');
	$get_atasan = $CI->M_inbox->get_atasan($pid);
 	if( $get_atasan->num_rows() > 0){
   		foreach($get_atasan->result_array() as $row){
   			$found[] = get_parents($row['group_suppervisor'], $found);
   		} 
 	} 
    array_unique($found);
   	return implode(", ", $found);
}

//inputan yyyy-mm-dd H:i:s
function format_datetime($datetime = ''){
    if($datetime != ''){
        $waktu = explode(' ', $datetime);
        $tanggal = explode('-',$waktu[0]);
        return $tanggal[2].'-'.$tanggal[1].'-'.$tanggal[0].' '.$waktu[1];
    }else return '';
}

function get_date_now() {
    $tanggal = date('Y-m-d');
    return $tanggal;
}

?>
