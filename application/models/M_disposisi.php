<?php

class M_disposisi extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}
	
	function get_data($num, $offset, $id_group){
		$group_level 	= $this->nativesession->get('group_level');
		$id_div			= $this->nativesession->get('id_div');
		
		if($group_level == 1){
			$tujuan = $id_div;
		} else {
			$tujuan = $id_group;
		}
		
		$this->db->select("ppof_tbl_flow.id, ppof_tbl_flow.id_surat, ppof_tbl_flow.tgl_disposisi, ppof_tbl_surat.tipe_surat, ppof_tbl_surat.subyek_surat");
		$this->db->from('ppof_tbl_flow');
		$this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
		$this->db->where('id_status', 8);
		$this->db->where('id_tujuan', $tujuan);
		$this->db->where('tgl_disposisi IS NOT NULL');
		$this->db->order_by('tgl_disposisi', "desc"); 
		$query = $this->db->get();
		return $query->result();
	}

    function get_data_new($param = array(),$offset = null,$limit = null){
        $this->db->select("ppof_tbl_flow.id, ppof_tbl_flow.id_surat, ppof_tbl_flow.tgl_disposisi, ppof_tbl_surat.tipe_surat, ppof_tbl_surat.subyek_surat");
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
        $this->db->where('id_status', 8);
        //$this->db->where('id_tujuan', $tujuan);
        $this->db->where('tgl_disposisi IS NOT NULL');
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'id_tujuan'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        $this->db->order_by('tgl_disposisi', "desc");
        if(($offset!=null) && ($limit!=null)) $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result();
    }
	
	function count_data($id_group){
		
		$group_level 	= $this->nativesession->get('group_level');
		$id_div			= $this->nativesession->get('id_div');
		
		if($group_level == 1){
			$tujuan = $id_div;
		} else {
			$tujuan = $id_group;
		}
		
		$this->db->select("id_surat");
		$this->db->from("ppof_tbl_flow");
		$this->db->where('id_status', 8);
		$this->db->where('id_tujuan', $tujuan);
		$this->db->where('tgl_disposisi IS NOT NULL');
		return $this->db->count_all_results();
	}

    function count_data_new($param = array()){
        $this->db->select("id_surat");
        $this->db->from("ppof_tbl_flow");
        $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
        $this->db->join('ppof_tbl_generate', 'ppof_tbl_surat.id = ppof_tbl_generate.id_surat', 'left');
        $this->db->where('id_status', 8);
        //$this->db->where('id_tujuan', $tujuan);
        $this->db->where('tgl_disposisi IS NOT NULL');
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'id_tujuan'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        return $this->db->count_all_results();
    }
	
	function show_data($id, $id_surat, $id_group){
		$this->db->select("ppof_tbl_surat.tipe_surat, ppof_tbl_surat.subyek_surat, ppof_tbl_surat.content, ppof_tbl_surat.tgl_create, 
        ppof_tbl_flow.tgl_disposisi, ppof_tbl_flow.pesan, ppof_tbl_generate.generate_number, ppof_tbl_flow.npp_disposisi, 
        ppof_tbl_emp.nama, ppof_tbl_surat.waktuawal, ppof_tbl_surat.waktuakhir, ppof_tbl_surat.tglawal, ppof_tbl_surat.tglakhir,
        ppof_tbl_surat.dukungan, ppof_tbl_surat.biro, ppof_tbl_surat.biro, ppof_tbl_surat.dukungan");
		$this->db->from("ppof_tbl_flow");
		$this->db->join("ppof_tbl_surat", "ppof_tbl_flow.id_surat = ppof_tbl_surat.id", "left");
		$this->db->join("ppof_tbl_generate", "ppof_tbl_surat.id = ppof_tbl_generate.id_surat", "left");
		$this->db->join("ppof_tbl_emp", "ppof_tbl_flow.npp_disposisi = ppof_tbl_emp.npp", "left");
		$this->db->where("ppof_tbl_flow.id", $id);
		$this->db->where("ppof_tbl_flow.id_surat", $id_surat);
		$this->db->where("ppof_tbl_flow.id_approval", $id_group);
		$query = $this->db->get();
		return $query->row();
	}
	
	function tracking_disposisi($id_surat, $div_tracking, $id_group){

		$group_level = $this->nativesession->get('group_level');
		
		$sql = "select 
            CASE 
	           WHEN ppof_tbl_group.group_level = '2' THEN ppof_tbl_group.group_name ELSE ppof_tbl_emp.nama 
            END AS TUJUAN_DISPOSISI, 
            ppof_tbl_flow.pesan, ppof_tbl_flow.tgl_disposisi, 
           CASE 
	           WHEN ppof_tbl_flow.flag_read = '0' THEN 'UNREAD' ELSE 'READ' END AS STATUS_BACA, 
            ppof_tbl_emp.photo, ppof_tbl_group.group_level, ppof_tbl_emp.id
            from ppof_tbl_flow 
            left join ppof_tbl_group ON ppof_tbl_flow.id_tujuan = ppof_tbl_group.id_group 
            left join ppof_tbl_emp ON ppof_tbl_flow.id_tujuan = ppof_tbl_emp.id 
            where ppof_tbl_flow.id_surat = '$id_surat' AND ppof_tbl_flow.div_tracking = '$div_tracking' 
            AND ppof_tbl_flow.id_status <> '8' order by ppof_tbl_group.group_level desc";

		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function tracking_pesan_disposisi($id_surat, $div_tracking){

        $this->db->select("ppof_tbl_flow.*, ppof_tbl_group.group_name as nama");
        $this->db->from("ppof_tbl_flow");
        $this->db->join("ppof_tbl_group","ppof_tbl_flow.id_approval = ppof_tbl_group.id_group","left");
        $this->db->where("ppof_tbl_flow.id_surat", $id_surat);
        $this->db->where("ppof_tbl_flow.id_status", '8');
        $this->db->where("ppof_tbl_flow.div_tracking", $div_tracking);
        $this->db->order_by('ppof_tbl_group.group_level', "asc");
        $query = $this->db->get();
		return $query->result();
	}
	
	function tracking_pesan_disposisi_gm($id_surat, $div_tracking){

        $this->db->select("ppof_tbl_flow.*, ppof_tbl_group.group_name as nama, ppof_tbl_emp.photo");
        $this->db->from("ppof_tbl_flow");
        $this->db->join("ppof_tbl_group","ppof_tbl_flow.id_approval = ppof_tbl_group.id_group","left");
        $this->db->join("ppof_tbl_emp","ppof_tbl_flow.npp_pengirim = ppof_tbl_emp.npp","left");
        $this->db->where("ppof_tbl_flow.id_surat", $id_surat);
        $this->db->where("ppof_tbl_flow.id_status", '8');
        $this->db->where("ppof_tbl_flow.div_tracking", $div_tracking);
        $this->db->order_by('ppof_tbl_group.group_level', "asc");
        $query = $this->db->get();
        return $query->result();
	}
	
	function get_upd_status2($arr_group, $id_group) {
	    $query = $this->db->query("select * from ppof_tbl_flow 
        left join ppof_tbl_emp on ppof_tbl_flow.id_tujuan = ppof_tbl_emp.id
        left join ppof_tbl_group on ppof_tbl_emp.id_group = ppof_tbl_group.id_group
        where id_surat = '1' and id_status = '6' and ppof_tbl_group.id_group IN ($arr_group)");
	    return $query->result();
	}
	
	function get_upd_status1(){
	    $query = $this->db->query("select ppof_tbl_flow.comment, ppof_tbl_emp.npp, ppof_tbl_emp.nama from ppof_tbl_flow 
        left join ppof_tbl_emp ON ppof_tbl_flow.id_tujuan = ppof_tbl_emp.id
        where id_surat = '1' and id_status = '6' and comment IS NOT NULL");
	    return $query->result();
	}
	
	function get_comment_user($id_user) {
	    $query = $this->db->query("select comment from ppof_tbl_flow where id_tujuan = '$id_user'");
        return $query->row();
	}
	
}