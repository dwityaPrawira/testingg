<?php 

class M_pesan extends CI_Model {
	
	private $tabel = 'ppof_tbl_emp';

	public function __construct() {
        parent::__construct();
    }

    public function get_data_employee(){
        $this->db->select('ppof_tbl_emp.npp, ppof_tbl_emp.nama');
        $this->db->from('ppof_tbl_emp');
        // $this->db->where('ppof_tbl_group.id_div', $id_div);
        $result = $this->db->get()->result_array();

        $ddown_emp['-- NPP --'] = "-- NAMA --";
        foreach ($result as $row) {
            $ddown_emp[$row['npp']] = $row['nama']; 
        }
        return $ddown_emp;
    }

}