<?php 

class M_group extends CI_Model
{

	private $tabel = 'ppof_tbl_group';

	public function get_data($param = array(), $offset = null, $limit = null){
	    $id_div	= $this->nativesession->get('id_div');
	    
		$this->db->select('ppof_tbl_group.id_group, ppof_tbl_group.group_name, ppof_tbl_group.group_suppervisor, grp.group_name as supervisor, ppof_tbl_group.id_div');
        $this->db->from('ppof_tbl_group');
        $this->db->join('ppof_tbl_group AS grp', 'ppof_tbl_group.group_suppervisor = grp.id_group', 'left');
        // $this->db->where('ppof_tbl_group.id_div', $id_div);
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'ppof_tbl_group.group_name'){
                    $this->db->like($idx, $data);
                }
            }
        }
        $this->db->order_by("ppof_tbl_group.group_level", "asc");
        if(($offset!=null) && ($limit!=null)) $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result();
        
	}

	public function count_data($param = array()){
	    $id_div	= $this->nativesession->get('id_div');
	    
		$this->db->select('ppof_tbl_group.id_group');
		$this->db->from('ppof_tbl_group');
		// $this->db->where('id_div', $id_div);
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'ppof_tbl_group.group_name'){
                    $this->db->like($idx, $data);
                }
            }
        }
        return $this->db->count_all_results();
	}

	public function group_by_id($id_group, $id_div){
		/*$this->db->from('ppof_tbl_group');
		$this->db->where('id_group', $id_group);
		$this->db->where('id_div', $id_div);
		return $this->db->get()->row();*/

		$this->db->select('id_group, group_name, group_suppervisor, group_level');
		$this->db->from('ppof_tbl_group');
		$this->db->where('id_group', $id_group);
		$this->db->where('id_div', $id_div);

		return $this->db->get()->row();
	}

	public function group_detail($id_group) {
		$this->db->select('id_group, group_name, group_suppervisor, group_level, id_div, idx');
		$this->db->from('ppof_tbl_group');
		$this->db->where('id_group', $id_group);
		return $this->db->get()->row();
	}

	public function group_list($id_group, $id_div){
		if(isset($add_link) && $add_link != '') { 
			$this->db->where('id_group !=', $id_group);
		}
		$this->db->where('id_div', $id_div);
		$result = $this->db->get($this->tabel)->result_array();

        $ddown_group[''] = "--";
        foreach ($result as $row) {
            $ddown_group[$row['id_group']] = $row['group_name']; 
        }
        return $ddown_group;
	}

	public function group_list_all($id_group) {
		if(isset($add_link) && $add_link != '') { 
			$this->db->where('id_group !=', $id_group);
		}
		$result = $this->db->get($this->tabel)->result_array();

        $ddown_group[''] = "--";
        foreach ($result as $row) {
            $ddown_group[$row['id_group']] = $row['group_name']; 
        }
        return $ddown_group;
	}

	public function get_data_divisi(){
        $this->db->select('ppof_tbl_div.id_div, ppof_tbl_div.div_name');
        $this->db->from('ppof_tbl_div');
        $result = $this->db->get()->result_array();

        $ddown_div[''] = "--";
        foreach ($result as $row) {
            $ddown_div[$row['id_div']] = $row['div_name']; 
        }
        return $ddown_div;
    }

    public function insert($data){
    	$this->db->insert($this->tabel, $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function update($data_upd, $idx){
        $this->db->set($data_upd);
        $this->db->where('ppof_tbl_group.idx', $idx);
        $this->db->update($this->tabel);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function get_last_group() {
    	$query = $this->db->query("select id_group from ppof_tbl_group order by idx DESC LIMIT 1");
    	return $query->row();
    }

    public function group_scope($id_group) {
        $this->db->select("scope, flag");
        $this->db->from('ppof_tbl_group');
        $this->db->where('id_group', $id_group);

        return $this->db->get()->row();


    }

}