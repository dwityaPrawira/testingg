<?php 

class M_Approval extends CI_Model {
	
	public function __construct() {
        parent::__construct();
    }

    public function data_approve($num, $offset, $id_group, $id_divisi){
    	$this->db->select('ppof_tbl_flow.id, ppof_tbl_flow.id_surat, ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_group.group_name');
    	$this->db->from('ppof_tbl_flow');
    	$this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    	$this->db->join('ppof_tbl_group', 'ppof_tbl_flow.id_approval = ppof_tbl_group.id_group', 'left');
    	$this->db->where('ppof_tbl_flow.id_status', 2);
    	$this->db->where('ppof_tbl_flow.id_group', $id_group);
    	$this->db->where('ppof_tbl_flow.id_divisi', $id_divisi);
    	$this->db->order_by("ppof_tbl_flow.id", "DESC");
    	$this->db->limit($num, $offset);
    	$query = $this->db->get();
    	return $query->result();
    }

    public function data_approve_new($param = array(),$offset = null,$limit = null){
        $this->db->select('ppof_tbl_flow.id, ppof_tbl_flow.id_surat, ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_group.group_name');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
        $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.id_approval = ppof_tbl_group.id_group', 'left');
        $this->db->where('ppof_tbl_flow.id_status', 2);
        //$this->db->where('ppof_tbl_flow.id_group', $id_group);
        //$this->db->where('ppof_tbl_flow.id_divisi', $id_divisi);
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'ppof_tbl_flow.id_group' || $idx == 'ppof_tbl_flow.id_divisi'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        $this->db->order_by("ppof_tbl_flow.id", "DESC");
        if(($offset!=null) && ($limit!=null)) $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result();
    }

    function count_data($id_group, $id_divisi){
    	$this->db->select('ppof_tbl_flow.id_surat');
    	$this->db->from('ppof_tbl_flow');
    	$this->db->where('id_status', 2);
    	$this->db->where('id_group', $id_group);
    	$this->db->where('id_divisi', $id_divisi);
		return $this->db->count_all_results();
    }

    function count_data_new($param = array()){
        $this->db->select('ppof_tbl_flow.id_surat');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
        $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.id_approval = ppof_tbl_group.id_group', 'left');
        $this->db->where('id_status', 2);
        //$this->db->where('id_group', $id_group);
        //$this->db->where('id_divisi', $id_divisi);
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'ppof_tbl_flow.id_group' || $idx == 'ppof_tbl_flow.id_divisi'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        return $this->db->count_all_results();
    }

    function get_data($id_group, $id_div, $id){
    	$this->db->select('ppof_tbl_surat.id, ppof_tbl_surat.subyek_surat, ppof_tbl_surat.waktuawal, ppof_tbl_surat.waktuakhir, ppof_tbl_surat.tglawal, ppof_tbl_surat.tglakhir, ppof_tbl_surat.biro, ppof_tbl_surat.dukungan,ppof_tbl_surat.biro, ppof_tbl_surat.dukungan,ppof_tbl_surat.tgl_create, ppof_tbl_group.group_name, ppof_tbl_surat.tipe_surat, ppof_tbl_surat.content, ppof_tbl_generate.generate_number');
    	$this->db->from('ppof_tbl_flow');
    	$this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    	$this->db->join('ppof_tbl_group', 'ppof_tbl_flow.id_pengirim = ppof_tbl_group.id_group', 'left');
    	$this->db->join('ppof_tbl_generate', 'ppof_tbl_surat.id = ppof_tbl_generate.id_surat', 'left');
    	$this->db->where('ppof_tbl_flow.id_status', 2);
    	$this->db->where('ppof_tbl_flow.id_group', $id_group);
        $this->db->where('ppof_tbl_flow.id', $id);
    	$this->db->where('ppof_tbl_flow.id_divisi', $id_div);
    	return $this->db->get()->row();
    }
    
	function get_data_done($id, $id_surat, $id_group, $id_div){
    	$this->db->select('ppof_tbl_surat.id, ppof_tbl_surat.subyek_surat, ppof_tbl_surat.waktuawal, ppof_tbl_surat.waktuakhir, ppof_tbl_surat.tglawal, ppof_tbl_surat.tglakhir,ppof_tbl_surat.biro, ppof_tbl_surat.dukungan,ppof_tbl_surat.tgl_create, ppof_tbl_group.group_name, ppof_tbl_surat.tipe_surat, ppof_tbl_surat.content, ppof_tbl_generate.generate_number');
    	$this->db->from('ppof_tbl_flow');
    	$this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    	$this->db->join('ppof_tbl_group', 'ppof_tbl_flow.id_pengirim = ppof_tbl_group.id_group', 'left');
    	$this->db->join('ppof_tbl_generate', 'ppof_tbl_surat.id = ppof_tbl_generate.id_surat', 'left');
    	$this->db->where('ppof_tbl_flow.id', $id);
    	$this->db->where('ppof_tbl_flow.id_surat', $id_surat);
    	$this->db->where('ppof_tbl_flow.id_status', 7);
    	$this->db->where('ppof_tbl_flow.id_group', $id_group);
    	$this->db->where('ppof_tbl_flow.id_divisi', $id_div);
    	$this->db->order_by("tgl_approve", "desc"); 
    	return $this->db->get()->row();
    }

    // function update_approve($id_group, $id_div, $id_surat, $tanggal, $id, $get_track){

    //   	$this->db->query("update ".TBL_FLOW." set id_status = '7', tgl_approve = '$tanggal', id_approval = '$id_group', group_tracking = '$get_track' where id_group = '$id_group' and id_divisi = '$id_div' and ppof_tbl_flow.id = '$id' and ppof_tbl_flow.id_surat = '$id_surat'");
    // }

    function update_approve($id_group, $id_div, $id_surat, $tanggal, $id, $get_track, $notes_approval){
        $this->db->query("update ".TBL_FLOW." set id_status = '7', tgl_approve = '$tanggal', id_approval = '$id_group', 
        group_tracking = '$get_track', notes_approval = '$notes_approval'
        where id_group = '$id_group' and id_divisi = '$id_div' and ppof_tbl_flow.id = '$id' 
        and ppof_tbl_flow.id_surat = '$id_surat'");
    }



    function get_flow_approve($id_group, $id_div, $id_surat){
        $this->db->select('*');
        $this->db->from('ppof_tbl_flow');
        $this->db->where('id_group', $id_group);
        $this->db->where('id_divisi', $id_div);
        $this->db->where('id_surat', $id_surat);
        $query = $this->db->get();
        return $query->row();
    }

    function tolak($id_surat, $comment, $tanggal, $id_group, $id_pengirim, $id_group_pengirim, $tujuan, $id_div){
       	$this->db->query("update ppof_tbl_flow set id_status = '4', id_group = '$id_group_pengirim', id_tujuan = '$tujuan', 
            pesan = '$comment', tgl_approve = '$tanggal',id_approval = '$id_group', div_tracking = '$id_div' 
            where id_surat = '$id_surat' and id_status = '2' and id_group = '$id_group'");
    }

    function upd_sent_tolak($id, $id_surat, $id_group){
        $this->db->query("update ppof_tbl_flow set ppof_tbl_flow.sent_status = '99' where ppof_tbl_flow.id_status = '3' and ppof_tbl_flow.id_surat = '$id_surat' and ppof_tbl_flow.group_tracking = '$id_group'");
    }

    function get_id_pengirim($id_surat, $id_group, $id_div, $id_status){
        $this->db->select('*');
        $this->db->from('ppof_tbl_flow');
        $this->db->where('id_surat', $id_surat);
        $this->db->where('id_group', $id_group);
        $this->db->where('id_status', $id_status);
        $this->db->where('id_divisi', $id_div);
        $query = $this->db->get();
        return $query->row();
    }

    function get_sent_pengirim($id_surat, $id_div, $id_status){
        $this->db->select('*');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.id_group = ppof_tbl_group.id_group', 'left');
        $this->db->where('id_surat', $id_surat);
        $this->db->where('id_status', $id_status);
        $this->db->where('id_divisi', $id_div);
        $query = $this->db->get();
        return $query->row();
    }
    
    function done_approve($num, $offset, $id_group, $id_div){
    	$this->db->select('ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_group.group_name, ppof_tbl_surat.tipe_surat, ppof_tbl_surat.content, ppof_tbl_flow.tgl_approve, ppof_tbl_flow.id, ppof_tbl_flow.id_surat');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
        $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.group_tracking = ppof_tbl_group.id_group', 'left');
        $this->db->where('id_status', 7);
        $this->db->where('ppof_tbl_flow.id_group', $id_group);
        $this->db->where('id_divisi', $id_div);
        $this->db->order_by('ppof_tbl_flow.id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    function done_approve_new($param = array(),$offset = null,$limit = null){
        $this->db->select('ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_group.group_name, ppof_tbl_surat.tipe_surat, ppof_tbl_surat.content, ppof_tbl_flow.tgl_approve, ppof_tbl_flow.id, ppof_tbl_flow.id_surat');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
        $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.group_tracking = ppof_tbl_group.id_group', 'left');
        $this->db->where('id_status', 7);
        //$this->db->where('ppof_tbl_flow.id_group', $id_group);
        //$this->db->where('id_divisi', $id_div);
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'ppof_tbl_flow.id_group' || $idx == 'ppof_tbl_flow.id_divisi'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        $this->db->order_by('ppof_tbl_flow.id', 'desc');
        if(($offset!=null) && ($limit!=null)) $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result();
    }
    
    function done_count_data($id_group, $id_div){
    	$this->db->select('ppof_tbl_flow.id_surat');
    	$this->db->from('ppof_tbl_flow');
    	$this->db->where('id_status', 7);
    	$this->db->where('ppof_tbl_flow.id_group', $id_group);
    	$this->db->where('id_divisi', $id_div);
		return $this->db->count_all_results();
    }

    function done_count_data_new($param = array()){
        $this->db->select('ppof_tbl_flow.id_surat');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
        $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.group_tracking = ppof_tbl_group.id_group', 'left');
        $this->db->where('id_status', 7);
        //$this->db->where('ppof_tbl_flow.id_group', $id_group);
        //$this->db->where('id_divisi', $id_div);
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'ppof_tbl_flow.id_group' || $idx == 'ppof_tbl_flow.id_divisi'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        return $this->db->count_all_results();
    }
    
    function get_id_surat($id){
    	$query = $this->db->query("select id_surat from ppof_tbl_flow where id = '$id'");
    	return $query->row();
    }

    function upd_sent_record($id_surat, $id_div, $data){
        $this->db->where('id_surat', $id_surat);
        $this->db->where('id_status', '3');
        $this->db->where('id_divisi', $id_div);
        $this->db->update('ppof_tbl_flow', $data);
    }

    function get_details($id, $id_surat){
        $this->db->where('id', $id);
        $this->db->where('id_surat', $id_surat);
        $query = $this->db->get('ppof_tbl_flow');
        return $query->row();
    }

    function update_biro($id_surat, $biro) {
        $this->db->query("update ppof_tbl_surat set biro = '$biro' where id = '$id_surat'");
    }

    function get_detail_surat($id_surat){
        $this->db->from('ppof_tbl_surat');
        $this->db->where('id', $id_surat);

        $query = $this->db->get();
        return $query->row();
    }
}   