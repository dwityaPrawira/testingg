<?php 

class M_employee extends CI_Model
{

    private $tabel = 'ppof_tbl_emp';

	public function get_data($param = array(), $offset = null, $limit = null){
	    $id_div	= $this->nativesession->get('id_div');
	    
		$this->db->select('ppof_tbl_emp.id, ppof_tbl_emp.npp, ppof_tbl_emp.nama, ppof_tbl_group.id_group, ppof_tbl_group.group_name, ppof_tbl_emp.flag');
        $this->db->from('ppof_tbl_emp');
        $this->db->join('ppof_tbl_group', 'ppof_tbl_emp.id_group = ppof_tbl_group.id_group', 'left');
        // $this->db->where('ppof_tbl_group.id_div', $id_div);
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'ppof_tbl_emp.npp' || $idx == 'ppof_tbl_emp.nama'){
                    $this->db->where($idx, $data);
                }else $this->db->like($idx, $data);
            }
        }
        $this->db->order_by("npp", "desc");
        if(($offset!=null) && ($limit!=null)) $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result();
        
	}

	public function count_data($param = array()){
	    $id_div	= $this->nativesession->get('id_div');
	    
		$this->db->select('ppof_tbl_emp.id');
		$this->db->from('ppof_tbl_emp');
		$this->db->join('ppof_tbl_group', 'ppof_tbl_emp.id_group = ppof_tbl_group.id_group', 'left');
		// $this->db->where('ppof_tbl_group.id_div', $id_div);
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'npp' || $idx == 'group_name'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        return $this->db->count_all_results();
	}

    public function get_data_emp($id_div, $npp, $id){

        $this->db->select('ppof_tbl_emp.id, ppof_tbl_emp.npp, ppof_tbl_emp.nama,ppof_tbl_emp.id_group, ppof_tbl_group.group_name, ppof_tbl_group.id_div');
        $this->db->from('ppof_tbl_emp');
        $this->db->join('ppof_tbl_group', 'ppof_tbl_emp.id_group = ppof_tbl_group.id_group', 'left');
        // $this->db->where('ppof_tbl_group.id_div', $id_div);
        $this->db->where('ppof_tbl_emp.npp', $npp);
        $this->db->where('ppof_tbl_emp.id', $id);

        return $this->db->get()->row();

    }

    public function get_data_group($id_group, $group_name, $id_div){
        $this->db->select('ppof_tbl_group.id_group, ppof_tbl_group.group_name');
        $this->db->from('ppof_tbl_group');
        // $this->db->where('ppof_tbl_group.id_div', $id_div);
        $result = $this->db->get()->result_array();

        $ddown_group[$id_group] = $group_name;
        foreach ($result as $row) {
            $ddown_group[$row['id_group']] = $row['group_name']; 
        }
        return $ddown_group;
    }

    public function update($data_upd, $id, $npp){
        $this->db->set($data_upd);
        $this->db->where('ppof_tbl_emp.id', $id);
        $this->db->where('ppof_tbl_emp.npp', $npp);
        $this->db->update($this->tabel);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function insert($data, $npp){

        $this->load->library('SimpleLoginSecure'); // load login library
        $this->simpleloginsecure->create($npp, '123456'); // example to create new user

        $this->db->insert($this->tabel, $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function check_npp($npp){
        $this->db->from('ppof_tbl_emp');
        $this->db->where('ppof_tbl_emp.npp', $npp);
        return $this->db->count_all_results();
    }

    public function div_list(){
        /*$get_div = $this->db->get('ppof_tbl_div')->result_array();
        $ddown_div[''] = array();
        foreach ($get_div as $row) {
            $ddown_div[$row['id_div']] = $row['div_name'];
        }
        return $ddown_div;*/

        $result = $this->db->query("SELECT * FROM ppof_tbl_div");
        return $result;
    }

    public function group_list_by_div($id){
        /*$this->db->where('id_div', $id_div);
        $this->db->order_by('group_level', 'ASC');
        $get_group = $this->db->get('ppof_tbl_group')->result_array();
        
        $ddown_group = array();
        foreach ($get_group as $row) {
            $ddown_group[$row['id_group']] = $row['group_name'];
        }
        return $ddown_group;*/

        $get_group = $this->db->query("SELECT * FROM ppof_tbl_group WHERE ppof_tbl_group.id_div = '$id'");
        return $get_group->result();
    }

    public function update_foto($namaphoto, $npp, $id_employee) {
        $this->db->query("update ppof_tbl_emp set photo = '$namaphoto' where id = '$id_employee' and npp = '$npp'");
    }

    public function reset_password($npp, $password) {
        $this->db->query("update users set user_pass = '$password' where user_email = '$npp'");
    }
    
    function get_sub_category($category_id){
        $query = $this->db->get_where('ppof_tbl_group', array('flag' => $category_id));
        return $query;
    }

    function get_employee($nip) {
        $this->db->from('ppof_tbl_emp');
        $this->db->where('npp', $nip);
        $query = $this->db->get();
        return $query->row();
    }

    function get_data_employee($nip) {
        $query = $this->db->get_where('ppof_tbl_emp', array('npp' => $nip));
        return $query->row();
    }

    function upd_flag($nip, $flag) {
        $this->db->set('flag', $flag);
        $this->db->where('npp', $nip);
        $this->db->update('ppof_tbl_emp');
    }

}