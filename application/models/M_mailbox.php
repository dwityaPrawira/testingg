<?php 

Class M_mailbox extends CI_Model {

	public function __construct() {
        parent::__construct();
    }

    
    public function get_data_pesan($param = array(),$offset = null,$limit = null){
        $this->db->select('ppof_tbl_flow.id, ppof_tbl_flow.id_surat, ppof_tbl_flow.tgl_approve,ppof_tbl_div.div_name, ppof_tbl_surat.tgl_create, ppof_tbl_surat.subyek_surat');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');


        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'id_tujuan' || $idx == 'id_status'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        $this->db->order_by("tgl_approve", "desc");
        if(($offset!=null) && ($limit!=null)) $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_data_pesan($param = array()){
        $this->db->select('ppof_tbl_flow.id');
        $this->db->from('ppof_tbl_flow');
        $this->db->where('id_tujuan', '$npp');
        $this->db->where('id_status', '9');
        
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'id_tujuan' || $idx == 'id_status'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        return $this->db->count_all_results();
    }

    

    public function show_gm($id, $id_surat, $id_div, $id_status){ // id div menandakan tujuan div
      $this->db->select('ppof_tbl_surat.tipe_surat, ppof_tbl_surat.subyek_surat, ppof_tbl_surat.waktuawal, ppof_tbl_surat.waktuakhir, ppof_tbl_surat.tglawal, ppof_tbl_surat.tglakhir,ppof_tbl_surat.biro, ppof_tbl_surat.dukungan,ppof_tbl_surat.content, ppof_tbl_surat.tgl_create, ppof_tbl_flow.id_pengirim, ppof_tbl_flow.id_divisi, ppof_tbl_flow.id_group, ppof_tbl_flow.npp_pengirim, ppof_tbl_flow.tgl_approve, ppof_tbl_generate.generate_number');
      $this->db->from('ppof_tbl_flow');
      $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
      $this->db->join("ppof_tbl_generate", "ppof_tbl_surat.id = ppof_tbl_generate.id_surat", "left");
      $this->db->where('ppof_tbl_flow.id', $id);
      $this->db->where('ppof_tbl_flow.id_surat', $id_surat);
      $this->db->where('ppof_tbl_flow.id_status', $id_status);
      $this->db->where('ppof_tbl_flow.id_tujuan', $id_div);
      return $this->db->get()->row();
    }

    public function show($id, $id_surat, $id_employee, $id_div, $group_level, $id_group){ // id emp tujuan surat

      $npp = $this->nativesession->get('npp');

      $this->db->select('ppof_tbl_surat.tipe_surat, ppof_tbl_surat.subyek_surat, ppof_tbl_surat.waktuawal, ppof_tbl_surat.waktuakhir, ppof_tbl_surat.tglawal, ppof_tbl_surat.tglakhir,ppof_tbl_surat.biro, ppof_tbl_surat.dukungan,ppof_tbl_surat.content, ppof_tbl_surat.tgl_create, ppof_tbl_flow.id_pengirim, ppof_tbl_flow.id_divisi, ppof_tbl_flow.id_group, ppof_tbl_flow.npp_pengirim, ppof_tbl_flow.tgl_approve, ppof_tbl_generate.generate_number, ppof_tbl_flow.id_status, ppof_tbl_flow.pesan');
      $this->db->from('ppof_tbl_flow');
      $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
      $this->db->join("ppof_tbl_generate", "ppof_tbl_surat.id = ppof_tbl_generate.id_surat", "left");
      $this->db->where('ppof_tbl_flow.id', $id);
      $this->db->where('ppof_tbl_flow.id_surat', $id_surat);
      $this->db->where('ppof_tbl_flow.id_status', '9');
      $this->db->where('ppof_tbl_flow.id_tujuan', $npp);
      return $this->db->get()->row();
    }

    public function tujuan($id_surat, $flag_tujuan){
      $query = $this->db->query("select ppof_tbl_div.div_name, CASE WHEN ppof_tbl_tujuan.flag_read = 0 THEN 'UNREAD' ELSE 'READ' END as FLAG from ppof_tbl_tujuan left join ppof_tbl_div on ppof_tbl_tujuan.id_div = ppof_tbl_div.id_div where id_surat = '$id_surat' and ppof_tbl_tujuan.flag_tujuan = '$flag_tujuan'");
      return $query->result();
    }

    public function count_tujuan($id_surat, $flag_tujuan){
      $query = $this->db->query("select count(*) as nilai from ppof_tbl_tujuan left join ppof_tbl_div on ppof_tbl_tujuan.id_div = ppof_tbl_div.id_div where id_surat = '$id_surat' and ppof_tbl_tujuan.flag_tujuan = '$flag_tujuan'");
      return $query->row();
    }

    public function attachment($id_surat){
      $this->db->select('id_surat, nama_file, ukuran_file, ppof_tbl_upload.month');
      $this->db->from('ppof_tbl_upload');
      $this->db->where('id_surat', $id_surat);
      return $this->db->get()->result();
    }

    public function count_attachment($id_surat){
      $query = $this->db->query("select count(*) as nilai from ppof_tbl_upload where id_surat = '$id_surat'");
      return $query->row();
    }

   public function update_read($id_div, $id_surat){
      $query = $this->db->query("update ppof_tbl_tujuan set flag_read = '1' where id_div = '$id_div' and id_surat = '$id_surat'");
   }

   public function get_bawahan_tier1($id_group){
      $query = $this->db->query("select * from ppof_tbl_group 
        left join ppof_tbl_emp on ppof_tbl_group.id_group = ppof_tbl_emp.id_group
        where group_suppervisor = '$id_group'");
      return $query->result();
   }

   public function get_bawahan_tier2($id_group){
      $query = $this->db->query("select * from ppof_tbl_emp 
        left join ppof_tbl_group on ppof_tbl_emp.id_group = ppof_tbl_group.id_group
        where ppof_tbl_group.group_suppervisor = '$id_group'");
      return $query->result();
   }

   public function update_disposisi($status_disposisi, $id_group, $pesan_disposisi, $tanggal, $id_surat, $id){
		$group_level 	= $this->nativesession->get('group_level');
		$id_div 		= $this->nativesession->get('id_div');
		$user_npp		= $this->session->userdata('user_email');
		
		if($group_level == 1){
			$flagging = '5';
			$tujuan = $id_div;
		} else {
			$flagging = '6';
			$tujuan = $id_group;
		}
      $this->db->query("update ppof_tbl_flow set  id_status = 8, id_approval = '$id_group', pesan = '$pesan_disposisi', tgl_disposisi = '$tanggal', div_tracking = '$id_div', npp_disposisi = '$user_npp' where id_surat = '$id_surat' and id_tujuan = '$tujuan' and id_status = '$flagging' and id = '$id'");
  }
   
  public function get_atasan($id_group){
   	return $this->db->query("select * from ppof_tbl_group where id_group = '$id_group'");	
  }
   
  public function get_disposisi($arrTujuan, $id_status, $id_surat, $id_div){
      /*$sql = "select pesan, tgl_disposisi, ppof_tbl_group.group_name from 
      ppof_tbl_flow left join ppof_tbl_group ON ppof_tbl_flow.id_approval = ppof_tbl_group.id_group
      where id_approval IN ($arrTujuan) and id_status = '$id_status' and id_surat = '$id_surat'";
      echo $sql;*/
   	return $this->db->query("select ppof_tbl_flow.pesan, ppof_tbl_flow.tgl_disposisi, ppof_tbl_group.group_name, ppof_tbl_emp.nama from 
   	ppof_tbl_flow 
    left join ppof_tbl_group ON ppof_tbl_flow.id_approval = ppof_tbl_group.id_group
    left join ppof_tbl_emp ON ppof_tbl_flow.npp_disposisi = ppof_tbl_emp.npp
   	where id_approval IN ($arrTujuan) and id_status = '$id_status' and id_surat = '$id_surat' and ppof_tbl_flow.div_tracking = $id_div")->result();
  }
   
  public function update_flow_status($id, $id_surat){
   	return $this->db->query("update ppof_tbl_flow set flag_read = '1' where id = '$id' and id_surat = '$id_surat'");
  }

  public function get_bawahan($id_group, $id_div) {
    $sql = $this->db->query("
      select
      case 
        when ppof.ppof_tbl_group.group_level = '2' then ppof.ppof_tbl_group.id_group
        when ppof.ppof_tbl_group.group_level = '3' then ppof.ppof_tbl_emp.id
      end as tujuan, ppof.ppof_tbl_group.group_name, ppof.ppof_tbl_emp.nama
      from ppof.ppof_tbl_emp left join 
      ppof.ppof_tbl_group on ppof.ppof_tbl_emp.id_group = ppof.ppof_tbl_group.id_group 
      where ppof.ppof_tbl_group.group_suppervisor = '$id_group'
      and ppof.ppof_tbl_group.id_div = '$id_div';
    ");

    return $sql->result();
  }

  public function get_emp_detail($npp) {
    // $this->db->select('ppof_tbl_emp.npp, ppof_tbl_emp.nama, ppof_tbl_group.group_name, ppof_tbl_div.div_name');
    // $this->db->from('ppof_tbl_emp');
    // $this->db->join('ppof_tbl_group', 'ppof_tbl_emp.id_group = ppof_tbl_group.id_group', 'left');
    // $this->db->join('ppof_tbl_div', 'ppof_tbl_group.id_div = ppof_tbl_div.id_div', 'left');
    // $this->db->where('ppof_tbl_emp.npp', '$npp');

    $sql = $this->db->query("select ppof_tbl_emp.npp, ppof_tbl_emp.nama, ppof_tbl_group.group_name, ppof_tbl_div.div_name from ppof_tbl_emp
left join ppof_tbl_group ON ppof_tbl_emp.id_group = ppof_tbl_group.id_group
left join ppof_tbl_div ON ppof_tbl_group.id_div = ppof_tbl_div.id_div
where ppof_tbl_emp.npp = '$npp'");

    // return $this->db->get()->row();
    return $sql->row();
  }

}