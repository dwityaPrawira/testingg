<?php 

Class M_inbox extends CI_Model {

	public function __construct() {
        parent::__construct();
    }

    public function get_data_gm($num, $offset, $id_div, $id_status){
    	$this->db->select('ppof_tbl_flow.id, ppof_tbl_flow.id_surat, ppof_tbl_flow.tgl_approve,ppof_tbl_div.div_name, ppof_tbl_surat.tgl_create, ppof_tbl_surat.subyek_surat, ppof_tbl_generate.generate_number');
    	$this->db->from('ppof_tbl_flow');
    	$this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
    	$this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    	$this->db->join('ppof_tbl_generate', 'ppof_tbl_surat.id = ppof_tbl_generate.id_surat', 'left');
    	$this->db->where('id_tujuan', $id_div);
    	$this->db->where('id_status', $id_status);
        $this->db->order_by("tgl_approve", "desc"); 
    	$this->db->limit($num, $offset);
    	$query = $this->db->get();
    	return $query->result();
    }
    public function get_data_gm_new($param = array(),$offset = null,$limit = null){
        $this->db->select('ppof_tbl_flow.id, ppof_tbl_flow.id_surat, ppof_tbl_flow.tgl_approve,ppof_tbl_div.div_name, ppof_tbl_surat.tgl_create, ppof_tbl_surat.subyek_surat');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');

        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'id_tujuan' || $idx == 'id_status'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        $this->db->order_by("tgl_approve", "desc");
        if(($offset!=null) && ($limit!=null)) $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_data_gm_new($param = array()){
        $this->db->select('ppof_tbl_flow.id');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
        
        if (is_array($param)) {
          foreach($param as $idx => $data){
              if($idx == 'id_tujuan' || $idx == 'id_status'){
                  $this->db->where($idx,$data);
              }else $this->db->like($idx,$data);
          }
      }
        return $this->db->count_all_results();
    }

    public function count_data_gm2($param = array()) {
        $this->db->select('ppof_tbl_flow.id');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
        
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'id_tujuan' || $idx == 'id_status'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        return $this->db->count_all_results();
    }


    public function count_data_gm($id_div, $id_status){
        $this->db->select('ppof_tbl_flow.id');
        $this->db->from('ppof_tbl_flow');
        $this->db->where('id_tujuan', $id_div);
        $this->db->where('id_status', $id_status);
        return $this->db->count_all_results();
    }

    public function get_data_two($num, $offset, $id_tujuan, $id_div, $group_suppervisor, $id_group) {
      	$flag = array('4', '6');

    	$this->db->select('ppof_tbl_flow.id, ppof_tbl_flow.id_surat, ppof_tbl_flow.tgl_approve,ppof_tbl_div.div_name, ppof_tbl_surat.tgl_create, ppof_tbl_surat.subyek_surat, ppof_tbl_generate.generate_number');
    	$this->db->from('ppof_tbl_flow');
      	$this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
      	$this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
      	$this->db->join('ppof_tbl_generate', 'ppof_tbl_surat.id = ppof_tbl_generate.id_surat', 'left');
      	$this->db->where_in('id_status', $flag);
      	$this->db->where('id_tujuan', $id_tujuan);
      	$this->db->where('div_tracking', $id_div);
      	$this->db->where('id_approval', $group_suppervisor);
      	$this->db->order_by("ppof_tbl_flow.tgl_disposisi", "desc"); 
      	$query = $this->db->get();
      	return $query->result();
    }

    public function get_data_two_new($param = array(),$offset = null,$limit = null){
        $flag = array('4', '6');

        $this->db->select('ppof_tbl_flow.id, ppof_tbl_flow.id_surat, ppof_tbl_flow.tgl_approve,ppof_tbl_div.div_name, 
        ppof_tbl_surat.tgl_create, ppof_tbl_surat.subyek_surat');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
//         $this->db->join('ppof_tbl_generate', 'ppof_tbl_surat.id = ppof_tbl_generate.id_surat', 'left');
        $this->db->where_in('id_status', $flag);
        //$this->db->where('div_tracking', $id_div);
        //$this->db->where('id_approval', $group_suppervisor);
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'id_tujuan' || $idx == 'id_approval' || $idx == 'div_tracking'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        $this->db->order_by("ppof_tbl_flow.tgl_disposisi", "desc");
        $this->db->order_by("ppof_tbl_flow.id", "desc");
        // $this->db->order_by("ppof_tbl_flow.tgl_approve", "desc");
        if(($offset!=null) && ($limit!=null)) $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_data_two_new($param = array()) {
        $flag = array('4', '6');
        $this->db->select('ppof_tbl_flow.id');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
//         $this->db->join('ppof_tbl_generate', 'ppof_tbl_surat.id = ppof_tbl_generate.id_surat', 'left');
        $this->db->where_in('id_status', $flag);
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'id_tujuan' || $idx == 'id_approval' || $idx == 'div_tracking'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        return $this->db->count_all_results();
    }

    public function count_data_two($id_tujuan, $id_div, $group_suppervisor, $id_group) {
      $flag = array('4', '6');
      $this->db->select('ppof_tbl_flow.id');
      $this->db->from('ppof_tbl_flow');
      $this->db->where_in('id_status', $flag);
//      $this->db->where('id_group', $id_group);
      $this->db->where('id_tujuan', $id_tujuan);
      $this->db->where('div_tracking', $id_div);
      $this->db->where('id_approval', $group_suppervisor);
      return $this->db->count_all_results();
    }

    public function show_gm($id, $id_surat, $id_div, $id_status){ // id div menandakan tujuan div
      $this->db->select('ppof_tbl_surat.tipe_surat, ppof_tbl_surat.subyek_surat, ppof_tbl_surat.waktuawal, ppof_tbl_surat.waktuakhir, 
        ppof_tbl_surat.tglawal, ppof_tbl_surat.tglakhir,ppof_tbl_surat.biro, ppof_tbl_surat.dukungan,ppof_tbl_surat.content, 
        ppof_tbl_surat.tgl_create, ppof_tbl_flow.id_pengirim, ppof_tbl_flow.id_divisi, ppof_tbl_flow.id_group, 
        ppof_tbl_flow.npp_pengirim, ppof_tbl_flow.tgl_approve, ppof_tbl_generate.generate_number, ppof_tbl_flow.comment');
      $this->db->from('ppof_tbl_flow');
      $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
      $this->db->join("ppof_tbl_generate", "ppof_tbl_surat.id = ppof_tbl_generate.id_surat", "left");
      $this->db->where('ppof_tbl_flow.id', $id);
      $this->db->where('ppof_tbl_flow.id_surat', $id_surat);
      $this->db->where('ppof_tbl_flow.id_status', $id_status);
      $this->db->where('ppof_tbl_flow.id_tujuan', $id_div);
      return $this->db->get()->row();
    }

    public function show($id, $id_surat, $id_employee, $id_div, $group_level, $id_group){ // id emp tujuan surat

      $flag = array('4', '6');
      
      if($group_level == 2){
        $tujuan = $id_group;
      } else {
        $tujuan = $id_employee;
      }

      $this->db->select('ppof_tbl_surat.tipe_surat, ppof_tbl_surat.subyek_surat, ppof_tbl_surat.waktuawal, ppof_tbl_surat.waktuakhir, 
        ppof_tbl_surat.tglawal, ppof_tbl_surat.tglakhir,ppof_tbl_surat.biro, ppof_tbl_surat.dukungan,ppof_tbl_surat.content, 
        ppof_tbl_surat.tgl_create, ppof_tbl_flow.id_pengirim, ppof_tbl_flow.id_divisi, ppof_tbl_flow.id_group, 
        ppof_tbl_flow.npp_pengirim, ppof_tbl_flow.tgl_approve, ppof_tbl_generate.generate_number, ppof_tbl_flow.id_status, 
        ppof_tbl_flow.pesan, ppof_tbl_flow.comment');
      $this->db->from('ppof_tbl_flow');
      $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
      $this->db->join("ppof_tbl_generate", "ppof_tbl_surat.id = ppof_tbl_generate.id_surat", "left");
      $this->db->where('ppof_tbl_flow.id', $id);
      $this->db->where('ppof_tbl_flow.id_surat', $id_surat);
      $this->db->where_in('ppof_tbl_flow.id_status', $flag);
      $this->db->where('ppof_tbl_flow.id_tujuan', $tujuan);
      return $this->db->get()->row();
    }

    public function tujuan($id_surat, $flag_tujuan){
      $query = $this->db->query("select ppof_tbl_div.div_name, CASE WHEN ppof_tbl_tujuan.flag_read = 0 THEN 'UNREAD' ELSE 'READ' END as FLAG from ppof_tbl_tujuan left join ppof_tbl_div on ppof_tbl_tujuan.id_div = ppof_tbl_div.id_div where id_surat = '$id_surat' and ppof_tbl_tujuan.flag_tujuan = '$flag_tujuan'");
      return $query->result();
    }

    public function count_tujuan($id_surat, $flag_tujuan){
      $query = $this->db->query("select count(*) as nilai from ppof_tbl_tujuan left join ppof_tbl_div on ppof_tbl_tujuan.id_div = ppof_tbl_div.id_div where id_surat = '$id_surat' and ppof_tbl_tujuan.flag_tujuan = '$flag_tujuan'");
      return $query->row();
    }

    public function attachment($id_surat){
      $this->db->select('id_surat, nama_file, ukuran_file, ppof_tbl_upload.month');
      $this->db->from('ppof_tbl_upload');
      $this->db->where('id_surat', $id_surat);
      return $this->db->get()->result();
    }

    public function count_attachment($id_surat){
      $query = $this->db->query("select count(*) as nilai from ppof_tbl_upload where id_surat = '$id_surat'");
      return $query->row();
    }

   public function update_read($id_div, $id_surat){
      $query = $this->db->query("update ppof_tbl_tujuan set flag_read = '1' where id_div = '$id_div' and id_surat = '$id_surat'");
   }

   public function get_bawahan_tier1($id_group, $group_level){
      $query = $this->db->query("select * from ppof_tbl_group 
        left join ppof_tbl_emp on ppof_tbl_group.id_group = ppof_tbl_emp.id_group
        where group_suppervisor = '$id_group' and group_level != '$group_level' 
        and ppof_tbl_group.group_level != '0' and ppof_tbl_emp.npp is not NULL");
      return $query->result();
   }

   public function get_bawahan_tier2($id_group){
      $query = $this->db->query("select * from ppof_tbl_emp 
        left join ppof_tbl_group on ppof_tbl_emp.id_group = ppof_tbl_group.id_group
        where ppof_tbl_group.group_suppervisor = '$id_group'");
      return $query->result();
   }

   public function update_disposisi($status_disposisi, $id_group, $pesan_disposisi, $tanggal, $id_surat, $id){
		$group_level 	= $this->nativesession->get('group_level');
		$id_div 		= $this->nativesession->get('id_div');
		$user_npp		= $this->session->userdata('user_email');
		
		if($group_level == 1){
			$flagging = '5';
			$tujuan = $id_div;
		} else {
			$flagging = '6';
			$tujuan = $id_group;
		}
      $this->db->query("update ppof_tbl_flow set  id_status = 8, id_approval = '$id_group', pesan = '$pesan_disposisi', tgl_disposisi = '$tanggal', div_tracking = '$id_div', 
        npp_disposisi = '$user_npp', sent_status = '5' where id_surat = '$id_surat' and id_tujuan = '$tujuan' 
        and id_status = '$flagging' and id = '$id'");
  }
   
  public function get_atasan($id_group){
   	return $this->db->query("select * from ppof_tbl_group where id_group = '$id_group'");	
  }
   
  public function get_disposisi($arrTujuan, $id_status, $id_surat, $id_div){

   	return $this->db->query("select ppof_tbl_flow.pesan, ppof_tbl_flow.tgl_disposisi, ppof_tbl_group.group_name, ppof_tbl_emp.nama from 
   	ppof_tbl_flow 
    left join ppof_tbl_group ON ppof_tbl_flow.id_approval = ppof_tbl_group.id_group
    left join ppof_tbl_emp ON ppof_tbl_flow.npp_disposisi = ppof_tbl_emp.npp
   	where id_approval IN ($arrTujuan) and id_status = '$id_status' and id_surat = '$id_surat' and ppof_tbl_flow.div_tracking = $id_div")->result();
  }
   
  public function update_flow_status($id, $id_surat){
   	return $this->db->query("update ppof_tbl_flow set flag_read = '1' where id = '$id' and id_surat = '$id_surat'");
  }

  public function kegiatan_diajukan() {
        $this->db->select('ppof_tbl_flow.id');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
        $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
        $this->db->where('ppof_tbl_flow.id_status', '3');
        
        return $this->db->count_all_results();
  }

  public function kegiatan_diajukan_user($id_pengirim, $id_div) {
    $this->db->select('ppof_tbl_flow.id');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
    $this->db->where('ppof_tbl_flow.id_status', '3');
    $this->db->where('id_pengirim', $id_pengirim);
    $this->db->where('id_divisi', $id_div);
    
    return $this->db->count_all_results();
  }

  public function kegiatan_diajukan_group($id_div, $scope_group) {
    $this->db->select('ppof_tbl_flow.id');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.id_group = ppof_tbl_group.id_group', 'left');
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
    $this->db->where('ppof_tbl_group.scope', $scope_group);
    $this->db->where('ppof_tbl_flow.id_status', '3');
    $this->db->where('id_divisi', $id_div);
    
    return $this->db->count_all_results();
  }

  public function kegiatan_disetujui_group($id_div, $scope_group) {
    $flag = array('5', '7');

    $this->db->select('ppof_tbl_flow.id');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.id_group = ppof_tbl_group.id_group', 'left');
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
    $this->db->where('ppof_tbl_group.scope', $scope_group);
    $this->db->where('ppof_tbl_flow.id_status', '3');
    $this->db->where_in('ppof_tbl_flow.sent_status', $flag);
    $this->db->where('id_divisi', $id_div);
    
    return $this->db->count_all_results();
  }

  public function kegiatan_ditolak_group($id_div, $scope_group) {
    $this->db->select('ppof_tbl_flow.id');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.id_group = ppof_tbl_group.id_group', 'left');
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
    $this->db->where('ppof_tbl_group.scope', $scope_group);
    $this->db->where('ppof_tbl_flow.id_status', '3');
    $this->db->where('ppof_tbl_flow.sent_status', '99');
    $this->db->where('id_divisi', $id_div);
    
    return $this->db->count_all_results();
  }

  public function kegiatan_berjalan_group($id_div, $scope_group){
    $flag = array('5', '7');

    $this->db->select('ppof_tbl_flow.id');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.id_group = ppof_tbl_group.id_group', 'left');
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
    $this->db->where('ppof_tbl_group.scope', $scope_group);
    $this->db->where('ppof_tbl_flow.id_status', '3');
    $this->db->where_in('ppof_tbl_flow.sent_status', $flag);
    $this->db->where('id_divisi', $id_div);
    $this->db->where('ppof_tbl_surat.tglawal <=', 'CURDATE()', FALSE);
    return $this->db->count_all_results();
  }

  public function kegiatan_disetujui() {
    $flag = array('5', '8');
      
    $this->db->select('ppof_tbl_flow.id');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
    $this->db->where_in('ppof_tbl_flow.id_status', $flag);
    $this->db->where('ppof_tbl_flow.sent_status', '5');
    
    return $this->db->count_all_results();
  }

  public function kegiatan_disetujui_user($id_pengirim, $id_div) {
    $this->db->select('ppof_tbl_flow.id');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
    $this->db->where('ppof_tbl_flow.id_status', '3');
    $this->db->where('ppof_tbl_flow.sent_status', '5');
    $this->db->where('id_pengirim', $id_pengirim);
    $this->db->where('id_divisi', $id_div);
    
    return $this->db->count_all_results();
  }

  public function kegiatan_ditolak() {
        $this->db->select('ppof_tbl_flow.id');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
        $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
        $this->db->where('ppof_tbl_flow.id_status', '4');
        
        return $this->db->count_all_results();
  }

  public function kegiatan_ditolak_user($id_pengirim, $id_div) {
    $this->db->select('ppof_tbl_flow.id');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
    $this->db->where('ppof_tbl_flow.id_status', '3');
    $this->db->where('ppof_tbl_flow.sent_status', '99');
    $this->db->where('id_pengirim', $id_pengirim);
    $this->db->where('id_divisi', $id_div);
    
    return $this->db->count_all_results();
  }

  public function kegiatan_berjalan() {

    // $this->db->select('ppof_tbl_flow.id');
    // $this->db->from('ppof_tbl_flow');
    // $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
    // $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    // $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
    // $this->db->where('ppof_tbl_flow.id_status', '5');
    // $this->db->where('ppof_tbl_surat.tglawal >=', 'CURDATE()', FALSE);

    $flag = array('5', '7');

    $this->db->select('ppof_tbl_flow.id');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.id_group = ppof_tbl_group.id_group', 'left');
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
    $this->db->where('ppof_tbl_flow.id_status', '3');
    $this->db->where_in('ppof_tbl_flow.sent_status', $flag);
    $this->db->where('id_divisi', '1');
    $this->db->where('ppof_tbl_surat.tglawal <=', 'CURDATE()', FALSE);
    return $this->db->count_all_results();
    
  }

  public function kegiatan_berjalan_user($id_pengirim, $id_div) {

    $this->db->select('ppof_tbl_flow.id');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
    $this->db->where('ppof_tbl_flow.id_status', '3');
    $this->db->where('ppof_tbl_flow.sent_status', '5');
    $this->db->where('ppof_tbl_surat.tglawal >=', 'CURDATE()', TRUE);
    $this->db->where('id_pengirim', $id_pengirim);
    $this->db->where('id_divisi', $id_div);
    
    return $this->db->count_all_results();
  }

  public function count_biro_sdm() {
    $this->db->select('ppof_tbl_flow.id');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_emp', 'ppof_tbl_flow.npp_pengirim = ppof_tbl_emp.npp', 'left');
    $this->db->join('ppof_tbl_group', 'ppof_tbl_emp.id_group = ppof_tbl_group.id_group', 'left');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->where('ppof_tbl_group.id_group', 'G3');
    $this->db->where('ppof_tbl_flow.id_status', '3');
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');

    return $this->db->count_all_results();
  }

  public function count_biro_keu() {
    $this->db->select('ppof_tbl_flow.id');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_emp', 'ppof_tbl_flow.npp_pengirim = ppof_tbl_emp.npp', 'left');
    $this->db->join('ppof_tbl_group', 'ppof_tbl_emp.id_group = ppof_tbl_group.id_group', 'left');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->where('ppof_tbl_group.id_group', 'G2');
    $this->db->where('ppof_tbl_flow.id_status', '3');
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');

    return $this->db->count_all_results();
  }

  public function count_biro_umum() {
    $this->db->select('ppof_tbl_flow.id');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_emp', 'ppof_tbl_flow.npp_pengirim = ppof_tbl_emp.npp', 'left');
    $this->db->join('ppof_tbl_group', 'ppof_tbl_emp.id_group = ppof_tbl_group.id_group', 'left');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->where('ppof_tbl_group.id_group', 'G5');
    $this->db->where('ppof_tbl_flow.id_status', '3');
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');

    return $this->db->count_all_results();
  }

  public function count_biro_sekre() {
    $this->db->select('ppof_tbl_flow.id');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_emp', 'ppof_tbl_flow.npp_pengirim = ppof_tbl_emp.npp', 'left');
    $this->db->join('ppof_tbl_group', 'ppof_tbl_emp.id_group = ppof_tbl_group.id_group', 'left');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->where('ppof_tbl_group.id_group', 'G4');
    $this->db->where('ppof_tbl_flow.id_status', '3');
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');

    return $this->db->count_all_results();
  }

  public function get_data_event(){
        $flag = array('4', '5', '8');
        $type = "event";

        $this->db->select('ppof_tbl_surat.id, ppof_tbl_flow.id_surat, ppof_tbl_surat.tipe_surat, ppof_tbl_surat.subyek_surat, 
          ppof_tbl_surat.tgl_create, ppof_tbl_surat.tglawal, ppof_tbl_flow.id_status, ppof_tbl_surat.tglawal, 
          ppof_tbl_surat.tglakhir, ppof_tbl_group.group_name, ppof_tbl_surat.dukungan');
        $this->db->from('ppof_tbl_surat');
        $this->db->join('ppof_tbl_flow', 'ppof_tbl_surat.id = ppof_tbl_flow.id_surat', 'left');
        $this->db->join('ppof_tbl_emp', 'ppof_tbl_flow.npp_pengirim = ppof_tbl_emp.npp', 'left');
        $this->db->join('ppof_tbl_group', 'ppof_tbl_emp.id_group = ppof_tbl_group.id_group', 'left');
        $this->db->where('ppof_tbl_surat.tipe_surat', $type);
        $this->db->where_in('ppof_tbl_flow.id_status', $flag);
        $this->db->where('ppof_tbl_surat.tglawal >= DATE_SUB(NOW(), INTERVAL 1 DAY)');
        $this->db->order_by("tglawal", "ASC"); 
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_data_event_user($id_pengirim, $id_div, $id_group) {

      $flag = array('3', '7');

      $this->db->select('ppof_tbl_flow.id,ppof_tbl_flow.id_status ,ppof_tbl_flow.sent_status,ppof_tbl_flow.id_surat,ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_surat.tipe_surat, CASE 
          WHEN ppof_tbl_flow.sent_status = 2 THEN CONCAT("PENDING", " - ", ppof_tbl_group.group_name) 
          WHEN ppof_tbl_flow.sent_status = 99 THEN CONCAT("DITOLAK", " - ", ppof_tbl_group.group_name) 
          WHEN ppof_tbl_flow.sent_status = 5 THEN "DISETUJUI" 
          WHEN ppof_tbl_flow.id_status = 7 THEN "DISETUJUI" 
          END as STATUS_SURAT, ppof_tbl_surat.tglawal, ppof_tbl_surat.tglakhir, ppof_tbl_flow.sent_status as id_status, 
          ppof_tbl_flow.group_pengirim as group_name, ppof_tbl_surat.dukungan');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id');
        $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.group_tracking = ppof_tbl_group.id_group');
        $this->db->where_in('id_status', $flag);
        $this->db->where('ppof_tbl_flow.id_group', $id_group);
        $this->db->where('id_divisi', $id_div);
        $this->db->where('ppof_tbl_surat.tglawal >= DATE_SUB(NOW(), INTERVAL 1 DAY)');
        $this->db->order_by("tglawal", "asc"); 
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result();
    }

    public function event_group_scope($scope_group, $id_div) {
      $query = $this->db->query("select ppof_tbl_flow.id,ppof_tbl_flow.id_status ,ppof_tbl_flow.sent_status,ppof_tbl_flow.id_surat,ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_surat.tipe_surat, CASE 
        WHEN ppof_tbl_flow.sent_status = 2 THEN CONCAT('PENDING', ' - ', ppof_tbl_group.group_name) 
        WHEN ppof_tbl_flow.sent_status = 99 THEN CONCAT('DITOLAK', ' - ', ppof_tbl_group.group_name) 
        WHEN ppof_tbl_flow.sent_status = 5 THEN 'DISETUJUI' 
        WHEN ppof_tbl_flow.id_status = 7 THEN 'DISETUJUI' 
        END as STATUS_SURAT, ppof_tbl_surat.tglawal, ppof_tbl_surat.tglakhir, ppof_tbl_flow.sent_status as id_status, 
        ppof_tbl_flow.group_pengirim as group_name, ppof_tbl_surat.dukungan
        from ppof_tbl_flow left join ppof_tbl_surat on ppof_tbl_flow.id_surat = ppof_tbl_surat.id left join ppof_tbl_group on ppof_tbl_flow.id_group = ppof_tbl_group.id_group
        where id_status = '3' and ppof_tbl_flow.sent_status IN ('2', '99') and id_divisi = '1' and ppof_tbl_group.scope = '$scope_group'
        UNION ALL
        select ppof_tbl_flow.id,ppof_tbl_flow.id_status ,ppof_tbl_flow.sent_status,ppof_tbl_flow.id_surat,ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_surat.tipe_surat, CASE 
        WHEN ppof_tbl_flow.sent_status = 2 THEN CONCAT('PENDING', ' - ', ppof_tbl_group.group_name) 
        WHEN ppof_tbl_flow.sent_status = 99 THEN CONCAT('DITOLAK', ' - ', ppof_tbl_group.group_name) 
        WHEN ppof_tbl_flow.sent_status = 5 THEN 'DISETUJUI' 
        WHEN ppof_tbl_flow.id_status = 7 THEN 'DISETUJUI'
        END as STATUS_SURAT, ppof_tbl_surat.tglawal, ppof_tbl_surat.tglakhir, ppof_tbl_flow.id_status, ppof_tbl_flow.group_pengirim as group_name, ppof_tbl_surat.dukungan
        from ppof_tbl_flow left join ppof_tbl_surat on ppof_tbl_flow.id_surat = ppof_tbl_surat.id left join ppof_tbl_group on ppof_tbl_flow.group_tracking = ppof_tbl_group.id_group
        where id_status = '7' and id_divisi = '$id_div' and ppof_tbl_group.scope = '$scope_group' group by id_surat order by tglawal ASC");
      return $query->result();
    }

    public function count_event_group_scope($scope, $id_div){
      $query = $this->db->query(
        "select ppof_tbl_flow.id,ppof_tbl_flow.id_status ,ppof_tbl_flow.sent_status,ppof_tbl_flow.id_surat,ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_surat.tipe_surat, CASE 
        WHEN ppof_tbl_flow.sent_status = 2 THEN CONCAT('PENDING', ' - ', ppof_tbl_group.group_name) 
        WHEN ppof_tbl_flow.sent_status = 99 THEN CONCAT('DITOLAK', ' - ', ppof_tbl_group.group_name) 
        WHEN ppof_tbl_flow.sent_status = 5 THEN 'DISETUJUI' 
        WHEN ppof_tbl_flow.id_status = 7 THEN 'DISETUJUI' 
        END as STATUS_SURAT, ppof_tbl_surat.tglawal, ppof_tbl_surat.tglakhir, ppof_tbl_flow.sent_status as id_status, 
        ppof_tbl_flow.group_pengirim as group_name, ppof_tbl_surat.dukungan
        from ppof_tbl_flow left join ppof_tbl_surat on ppof_tbl_flow.id_surat = ppof_tbl_surat.id left join ppof_tbl_group on ppof_tbl_flow.id_group = ppof_tbl_group.id_group
        where id_status = '3' and ppof_tbl_flow.sent_status IN ('2', '99') and id_divisi = '$id_div' and ppof_tbl_group.scope = '$scope'
        UNION ALL
        select ppof_tbl_flow.id,ppof_tbl_flow.id_status ,ppof_tbl_flow.sent_status,ppof_tbl_flow.id_surat,ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_surat.tipe_surat, CASE 
        WHEN ppof_tbl_flow.sent_status = 2 THEN CONCAT('PENDING', ' - ', ppof_tbl_group.group_name) 
        WHEN ppof_tbl_flow.sent_status = 99 THEN CONCAT('DITOLAK', ' - ', ppof_tbl_group.group_name) 
        WHEN ppof_tbl_flow.sent_status = 5 THEN 'DISETUJUI' 
        WHEN ppof_tbl_flow.id_status = 7 THEN 'DISETUJUI' 
        END as STATUS_SURAT, ppof_tbl_surat.tglawal, ppof_tbl_surat.tglakhir, ppof_tbl_flow.sent_status as id_status, ppof_tbl_flow.group_pengirim as group_name, ppof_tbl_surat.dukungan
        from ppof_tbl_flow left join ppof_tbl_surat on ppof_tbl_flow.id_surat = ppof_tbl_surat.id left join ppof_tbl_group on ppof_tbl_flow.group_tracking = ppof_tbl_group.id_group
        where id_status = '7' and id_divisi = '$id_div' and ppof_tbl_group.scope = '$scope' group by id_surat order by tglawal ASC"
      );

      return $this->db->count_all_results();
    }

    public function count_data_event(){
        $flag = array('4', '5', '8');
        $type = "event";
        $this->db->select('ppof_tbl_flow.id');
        $this->db->from('ppof_tbl_surat');
        $this->db->join('ppof_tbl_flow', 'ppof_tbl_surat.id = ppof_tbl_flow.id_surat', 'left');
        $this->db->where('ppof_tbl_surat.tipe_surat', $type);
        $this->db->where_in('ppof_tbl_flow.id_status', $flag);
        $this->db->where('ppof_tbl_surat.tglawal >= DATE_SUB(NOW(), INTERVAL 1 DAY)');
        return $this->db->count_all_results();
    }

    public function count_data_event_user($id_pengirim, $id_div, $id_group) {
      $flag = array('3', '7');
      $this->db->select('ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_surat.tipe_surat');
    	$this->db->from('ppof_tbl_flow');
    	$this->db->where_in('id_status', $flag);
    	$this->db->where('ppof_tbl_flow.id_group', $id_group);
      $this->db->where('id_divisi', $id_div);
		  return $this->db->count_all_results();
    }

    public function get_users($npp) {
        $this->db->select('success, fail');
        $this->db->from('users');
        $this->db->where('user_email', $npp);
        $query = $this->db->get();
        return $query->row();
    }

    public function get_group_pengirim() {
      $query = $this->db->query("
        SELECT group_pengirim,COUNT(*) as 'total' FROM ppof_tbl_flow where group_pengirim IS NOT NULL GROUP BY group_pengirim LIMIT 10");
      return $query->result();
    }

    public function login_count($success, $username) {
      $this->db->query("update users set success = $success where user_email = $username");
    }

    public function login_fail_count($fail, $username){
      $this->db->query("update users set fail = $fail where user_email = $username");
    }

    public function graph_success_login() {
      $query = $this->db->query("SELECT user_email,success FROM users");
      return $query->result();
    }

    public function graph_fail_login() {
      $query = $this->db->query("SELECT user_email,fail FROM users");
      return $query->result();
    }
    
    public function get_subordinat_tier2(){
      $query = $this->db->query("select  ppof_tbl_group.id_group, ppof_tbl_group.group_name, ppof_tbl_group.group_level, 
      ppof_tbl_emp.nama, ppof_tbl_emp.npp, ppof_tbl_emp.id 
      from ppof_tbl_group 
      left join ppof_tbl_emp on ppof_tbl_group.id_group = ppof_tbl_emp.id_group 
      where ppof_tbl_group.group_level = '2' 
      and ppof_tbl_group.group_level != '0' 
      and ppof_tbl_emp.npp is not NULL and ppof_tbl_group.flag = '0'");
      return $query->result();
    }

    public function get_subordinat($id_group) {
        $query = $this->db->query("select  ppof_tbl_group.id_group, ppof_tbl_group.group_name, ppof_tbl_group.group_level, ppof_tbl_emp.nama, ppof_tbl_emp.npp, ppof_tbl_emp.id from ppof_tbl_group left join ppof_tbl_emp on  ppof_tbl_group.id_group = ppof_tbl_emp.id_group where ppof_tbl_group.group_suppervisor = '$id_group' and ppof_tbl_group.group_level != '0' and ppof_tbl_emp.npp is not NULL");
        return $query->result();
    }
    
    public function upd_status($status, $id_surat, $id){
        $query = $this->db->query("update ppof_tbl_flow set comment = '$status' where id = '$id' and id_surat = '$id_surat'");
    }

    public function get_comment_approval($id_surat){
      $query = $this->db->query("select ptf.notes_approval, ptg.group_name, ptf.tgl_approve from ppof_tbl_flow ptf 
      left join ppof_tbl_group ptg on ptf.id_approval = ptg.id_group 
      where id_surat = '$id_surat' and id_status = '7'");

      return $query->result();
    }

    public function cek_subordinat($id_group){
      $this->db->from('ppof_tbl_group');
      $this->db->where('ppof_tbl_group.group_suppervisor', $id_group);
      $query = $this->db->get();
      return $query->result();
    }
    
}