<?php 

class M_Main extends CI_Model {

	//put your code here
    public function __construct() {
        parent::__construct();
    }

    function search_div($name = ''){
        $q = $this->db->query("select id_div, div_name from ppof_tbl_div where div_name like '%$name%'");
        return $q->result();
    }

    function search_employee($name = '') {
    	$group_level = $this->nativesession->get('group_level'); 
    	$nama = strtoupper($name);

    	$query = $this->db->query("select emp.id, emp.npp, emp.nama, emp.id_group, grp.group_name, grp.group_level from ppof_tbl_emp emp left join ppof_tbl_group grp on emp.id_group = grp.id_group WHERE grp.group_level >= $group_level and emp.nama like '%$nama%'");
    	
    	return $query->result();
    }

	function generate_menu($previlege){
		$q = $this->db->query("select * from ppof_tbl_menuprevilege a left join ppof_tbl_mastermenu b on a.id_menu = b.id where a.id_previlege = '$previlege' and submenu is null order by urutan asc");
		return $q->result();
	}

	function get_submenu($parent){
		$q = $this->db->query("select * from ppof_tbl_mastermenu where submenu is not null and parent = '$parent' order by urutan DESC;");
		return $q->result();
	}

	function insert($insert_query){ 
		$this->db->query($insert_query);
	}

	function insert_ar($tbl_nm, $param){
		$this->db->insert($tbl_nm, $param);
		
//		$sql = "insert into ".TBL_FLOW." (id_surat, id_status, id_divisi, id_group, id_pengirim, npp_pengirim, id_approval, tgl_approve) VALUES ()";
	}

	public function tujuan($id_surat, $flag_tujuan){
    	$query = $this->db->query("select ppof_tbl_div.div_name, CASE WHEN ppof_tbl_tujuan.flag_read = 0 THEN 'UNREAD' ELSE '<b>READ</b>' END as FLAG from ppof_tbl_tujuan left join ppof_tbl_div on ppof_tbl_tujuan.id_div = ppof_tbl_div.id_div where id_surat = '$id_surat' and ppof_tbl_tujuan.flag_tujuan = '$flag_tujuan'");
    	return $query->result();
    }

    public function count_tujuan($id_surat, $flag_tujuan){
    	$query = $this->db->query("select count(*) as nilai from ppof_tbl_tujuan left join ppof_tbl_div on ppof_tbl_tujuan.id_div = ppof_tbl_div.id_div where id_surat = '$id_surat' and ppof_tbl_tujuan.flag_tujuan = '$flag_tujuan'");
    	return $query->row();
    }

    public function attachment($id_surat){
    	$this->db->select('id_surat, nama_file, ukuran_file, ppof_tbl_upload.month');
    	$this->db->from('ppof_tbl_upload');
    	$this->db->where('id_surat', $id_surat);
    	return $this->db->get()->result();
    }

    public function count_attachment($id_surat){
    	$query = $this->db->query("select count(*) as nilai from ppof_tbl_upload where id_surat = '$id_surat'");
    	return $query->row();
    }

    public function get_tujuan($id_surat){
        $q = $this->db->query("select id_div from ppof_tbl_tujuan where id_surat = '$id_surat'");
        return $q->result();
    }
    
    public function tracking_surat($id_div, $id_surat){
    	$query = $this->db->query("SELECT ppof_tbl_group.group_name, 
		CASE 
		WHEN ppof_tbl_flow.id_status = 3 THEN 'PENGIRIM' 
		WHEN ppof_tbl_flow.id_status = 7 THEN 'APPROVAL' 
		END as JENJANG FROM ppof_tbl_flow left join ppof_tbl_group on ppof_tbl_flow.id_group = ppof_tbl_group.id_group
		where id_divisi = '$id_div' and id_surat = '$id_surat' and id_status <> '5' and div_tracking IS NULL order by tgl_approve asc");
    	return $query->result();
    }
    
    public function get_div_send($id_surat){
    	$query = $this->db->query("select ppof_tbl_flow.id_divisi from ppof_tbl_flow where id_status = '3' and id_surat = '$id_surat'");
    	return $query->row();
    }
}