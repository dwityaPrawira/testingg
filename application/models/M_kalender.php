<?php 

Class M_kalender extends CI_Model {

	public function __construct() {
        parent::__construct();
    }

    public function get_kalender($param = array(), $offset = null,$limit = null) {

        $flag = array('4', '5');
        $type = "event";

        $this->db->select('ppof_tbl_surat.id, ppof_tbl_flow.id_surat, ppof_tbl_surat.tipe_surat, ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_surat.tglawal, ppof_tbl_flow.id_status, ppof_tbl_surat.tglawal');
        $this->db->from('ppof_tbl_surat');
        $this->db->join('ppof_tbl_flow', 'ppof_tbl_surat.id = ppof_tbl_flow.id_surat', 'left');
        // $this->db->where('ppof_tbl_surat.tipe_surat', $type);
        $this->db->where_in('ppof_tbl_flow.id_status', $flag);

        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'tipe_surat'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }

        $this->db->order_by('tgl_approve', 'desc');
        if(($offset!=null) && ($limit!=null)) $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_kalender($param = array()) {

        $flag = array('4', '5');

        $this->db->select('ppof_tbl_surat.tipe_surat');
        $this->db->from('ppof_tbl_surat');
        $this->db->join('ppof_tbl_flow', 'ppof_tbl_surat.id = ppof_tbl_flow.id_surat','left');
        // $this->db->where('ppof_tbl_surat.tipe_surat', $type);
        $this->db->where_in('ppof_tbl_flow.id_status', $flag);

        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'tipe_surat'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }

        return $this->db->count_all_results();
    }

    public function filter($tglawal, $tglakhir){

        $flag = array('4', '5');
        $type = "event";

        $this->db->select('ppof_tbl_surat.id, ppof_tbl_flow.id_surat, ppof_tbl_surat.tipe_surat, ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_surat.tglawal, ppof_tbl_flow.id_status, ppof_tbl_surat.tglawal, ppof_tbl_surat.tglakhir, ppof_tbl_group.group_name');
        $this->db->from('ppof_tbl_surat');
        $this->db->join('ppof_tbl_flow', 'ppof_tbl_surat.id = ppof_tbl_flow.id_surat', 'left');
        $this->db->join('ppof_tbl_emp', 'ppof_tbl_flow.npp_pengirim = ppof_tbl_emp.npp', 'left');
        $this->db->join('ppof_tbl_group', 'ppof_tbl_emp.id_group = ppof_tbl_group.id_group', 'left');
        $this->db->where('ppof_tbl_surat.tipe_surat', $type);
        $this->db->where_in('ppof_tbl_flow.id_status', $flag);
        $this->db->where('ppof_tbl_surat.tglawal >=', $tglawal);
        $this->db->where('ppof_tbl_surat.tglawal <=', $tglakhir);
        $this->db->order_by('ppof_tbl_surat.tglawal', 'desc');

        $query = $this->db->get();
        return $query->result();
  }

  public function get_list_cal() {

    $flag = array('5', '8');

    $this->db->select('calendar.id, calendar.title, calendar.description, calendar.color, calendar.start_date, calendar.end_date, calendar.waktuawal, calendar.waktuakhir, calendar.biro, calendar.dukungan, calendar.id_surat');
    $this->db->from('calendar');
    $this->db->join('ppof_tbl_surat', 'calendar.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->join('ppof_tbl_flow', 'ppof_tbl_surat.id = ppof_tbl_flow.id_surat', 'left');
    $this->db->where_in('ppof_tbl_flow.id_status', $flag);
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
    $query = $this->db->get();

    return $query->result();
  }

  public function get_cal_scope($scope_group, $id_div) {

    $this->db->select('calendar.id, calendar.title, calendar.description, calendar.color, calendar.start_date, calendar.end_date, calendar.waktuawal, calendar.waktuakhir, calendar.biro, calendar.dukungan, calendar.id_surat');
    $this->db->from('ppof_tbl_flow');
    $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.group_tracking = ppof_tbl_group.id_group', 'left');
    $this->db->join('calendar', 'ppof_tbl_flow.id_surat = calendar.id_surat', 'left');
    $this->db->where('id_status', '7');
    $this->db->where('id_divisi', $id_div);
    $this->db->where('ppof_tbl_group.scope', $scope_group);
    $query = $this->db->get();

    return $query->result();

  }

  public function get_list_cal_user() {
    $flag = array('5', '8');

    $this->db->select('calendar.id, calendar.title, calendar.description, calendar.color, calendar.start_date, calendar.end_date, calendar.waktuawal, calendar.waktuakhir, calendar.biro, calendar.dukungan, calendar.id_surat');
    $this->db->from('calendar');
    $this->db->join('ppof_tbl_surat', 'calendar.id_surat = ppof_tbl_surat.id', 'left');
    $this->db->join('ppof_tbl_flow', 'ppof_tbl_surat.id = ppof_tbl_flow.id_surat', 'left');
    $this->db->where_in('ppof_tbl_flow.id_status', $flag);
    $this->db->where('ppof_tbl_surat.tipe_surat', 'event');
    $query = $this->db->get();

    return $query->result();
  }

  public function get_list_cal_word($word) {

    $this->db->select('calendar.id, calendar.title, calendar.description, calendar.color, calendar.start_date, calendar.end_date, calendar.waktuawal, calendar.waktuakhir, calendar.biro, calendar.dukungan, calendar.id_surat');
    $this->db->from('calendar');
    $this->db->like('biro', $word); // Produces: WHERE `title` LIKE '%match%' ESCAPE '!'
    $query = $this->db->get();

    return $query->result();
  }
}