<?php 

Class M_sentitem extends CI_Model {

	public function __construct() {
        parent::__construct();
    }

    public function get_data($num, $offset, $id_pengirim, $id_div){
        $this->db->select('ppof_tbl_flow.id,ppof_tbl_flow.id_surat,ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_surat.tipe_surat, CASE 
                WHEN ppof_tbl_flow.sent_status = 2 THEN CONCAT("PENDING", " - ", ppof_tbl_group.group_name) 
                WHEN ppof_tbl_flow.sent_status = 99 THEN CONCAT("DITOLAK", " - ", ppof_tbl_group.group_name) 
                WHEN ppof_tbl_flow.sent_status = 5 THEN "DISETUJUI" 
            END as STATUS_SURAT');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id');
        $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.group_tracking = ppof_tbl_group.id_group');
        $this->db->where('id_status', 3);
        $this->db->where('id_pengirim', $id_pengirim);
        $this->db->where('id_divisi', $id_div);
        $this->db->order_by("tgl_create", "desc"); 
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result();
    }
    public function get_data_new($param = array(),$offset = null,$limit = null){
        $this->db->select('ppof_tbl_flow.id,ppof_tbl_flow.id_surat,ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_surat.tipe_surat, CASE
                WHEN ppof_tbl_flow.sent_status = 2 THEN CONCAT("PENDING", " - ", ppof_tbl_group.group_name)
                WHEN ppof_tbl_flow.sent_status = 99 THEN CONCAT("DITOLAK", " - ", ppof_tbl_group.group_name)
                WHEN ppof_tbl_flow.sent_status = 5 THEN "DISETUJUI"
            END as STATUS_SURAT');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id');
        $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.group_tracking = ppof_tbl_group.id_group');
        $this->db->where('id_status', 3);
        // $this->db->where('id_pengirim', $id_pengirim);
        // $this->db->where('id_divisi', $id_div);
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'id_pengirim' || $idx == 'id_divisi'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        $this->db->order_by("tgl_create", "desc");
        if(($offset!=null) && ($limit!=null)) $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function count_data($id_pengirim, $id_div){
  //   	$user = $this->session->userdata('username');
		// $this->db->select("dt_memo_sent.nota_id, ");
		// $this->db->from("dt_memo_sent");	
		// $this->db->where("dt_memo_sent.`from` = '$user'", NULL, FALSE);
		$this->db->select('ppof_tbl_surat.subyek_surat, ppof_tbl_surat.tgl_create, ppof_tbl_surat.tipe_surat');
    	$this->db->from('ppof_tbl_flow');
    	$this->db->where('id_status', 3);
    	$this->db->where('id_pengirim', $id_pengirim);
        $this->db->where('id_divisi', $id_div);
		return $this->db->count_all_results();
    }

    public function count_data_new($param = array()){
        $this->db->select('ppof_tbl_flow.id');
        $this->db->from('ppof_tbl_flow');
        $this->db->join('ppof_tbl_div', 'ppof_tbl_flow.id_divisi = ppof_tbl_div.id_div', 'left');
        $this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
        $this->db->join('ppof_tbl_generate', 'ppof_tbl_surat.id = ppof_tbl_generate.id_surat', 'left');
        $this->db->where('id_status', 3);
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'id_pengirim' || $idx == 'id_divisi'){
                    $this->db->where($idx,$data);
                }else $this->db->like($idx,$data);
            }
        }
        return $this->db->count_all_results();
    }

    public function show($id, $id_surat, $id_pengirim, $id_div){

    	$this->db->select('ppof_tbl_surat.tipe_surat, ppof_tbl_surat.subyek_surat, ppof_tbl_surat.waktuawal, ppof_tbl_surat.waktuakhir, ppof_tbl_surat.tglawal, ppof_tbl_surat.tglakhir,ppof_tbl_surat.biro, ppof_tbl_surat.dukungan,ppof_tbl_surat.content, ppof_tbl_surat.tgl_create, ppof_tbl_generate.generate_number, CASE
                WHEN ppof_tbl_flow.sent_status = 2 THEN CONCAT("PENDING", " - ", ppof_tbl_group.group_name)
                WHEN ppof_tbl_flow.sent_status = 99 THEN CONCAT("DITOLAK", " - ", ppof_tbl_group.group_name)
                WHEN ppof_tbl_flow.sent_status = 5 THEN "DISETUJUI"
            END as status_surat');
    	$this->db->from('ppof_tbl_flow');
    	$this->db->join('ppof_tbl_surat', 'ppof_tbl_flow.id_surat = ppof_tbl_surat.id', 'left');
        $this->db->join('ppof_tbl_group', 'ppof_tbl_flow.group_tracking = ppof_tbl_group.id_group');
        $this->db->join('ppof_tbl_generate', 'ppof_tbl_flow.id_surat = ppof_tbl_generate.id_surat', 'left');
     	$this->db->where('ppof_tbl_flow.id', $id);
    	$this->db->where('ppof_tbl_flow.id_surat', $id_surat);
    	$this->db->where('ppof_tbl_flow.id_status', 3);
    	$this->db->where('ppof_tbl_flow.id_divisi', $id_div);
    	$this->db->where('ppof_tbl_flow.id_pengirim', $id_pengirim);
    	return $this->db->get()->row();
    }

    public function tujuan($id_surat, $flag_tujuan){
    	$query = $this->db->query("select ppof_tbl_div.div_name, CASE WHEN ppof_tbl_tujuan.flag_read = 0 THEN 'UNREAD' ELSE '<b>READ</b>' END as FLAG from ppof_tbl_tujuan left join ppof_tbl_div on ppof_tbl_tujuan.id_div = ppof_tbl_div.id_div where id_surat = '$id_surat' and ppof_tbl_tujuan.flag_tujuan = '$flag_tujuan'");
    	return $query->result();
    }

    public function count_tujuan($id_surat, $flag_tujuan){
    	$query = $this->db->query("select count(*) as nilai from ppof_tbl_tujuan left join ppof_tbl_div on ppof_tbl_tujuan.id_div = ppof_tbl_div.id_div where id_surat = '$id_surat' and ppof_tbl_tujuan.flag_tujuan = '$flag_tujuan'");
    	return $query->row();
    }

    public function attachment($id_surat){
    	$this->db->select('id_surat, nama_file, ukuran_file, ppof_tbl_upload.month');
    	$this->db->from('ppof_tbl_upload');
    	$this->db->where('id_surat', $id_surat);
    	return $this->db->get()->result();
    }

    public function count_attachment($id_surat){
    	$query = $this->db->query("select count(*) as nilai from ppof_tbl_upload where id_surat = '$id_surat'");
    	return $query->row();
    }
}