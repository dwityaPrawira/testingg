<?php 

class M_divisi extends CI_Model
{
    private $tabel = 'ppof_tbl_div';

	public function group_by_id($id_group, $id_div){

		$this->db->select('id_group, group_name, group_suppervisor, group_level');
		$this->db->from('ppof_tbl_group');
		$this->db->where('id_group', $id_group);
		$this->db->where('id_div', $id_div);

		return $this->db->get()->row();
	}

	public function div_detail($id_div) {
		$this->db->select('id_div, div_name, div_identity, active');
		$this->db->from('ppof_tbl_div');
		$this->db->where('id_div', $id_div);
		return $this->db->get()->row();
	}

	public function get_data_divisi(){
        $this->db->select('ppof_tbl_div.id_div, ppof_tbl_div.div_name');
        $this->db->from('ppof_tbl_div');
        $result = $this->db->get()->result_array();

        $ddown_div[''] = "--";
        foreach ($result as $row) {
            $ddown_div[$row['id_div']] = $row['div_name']; 
        }
        return $ddown_div;
    }

    public function insert($data){
    	$this->db->insert($this->tabel, $data);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function update($data_upd, $idx){
        $this->db->set($data_upd);
        $this->db->where('ppof_tbl_div.id_div', $idx);
        $this->db->update($this->tabel);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function get_data($param = array(), $offset = null, $limit = null){
        $id_div = $this->nativesession->get('id_div');
        
        $this->db->select('ppof_tbl_div.id_div, ppof_tbl_div.div_name, ppof_tbl_div.div_identity, ppof_tbl_div.active');
        $this->db->from('ppof_tbl_div');
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'ppof_tbl_div.div_name'){
                    $this->db->like($idx, $data);
                }
            }
        }
        $this->db->order_by("ppof_tbl_div.div_name", "asc");
        if(($offset!=null) && ($limit!=null)) $this->db->limit($limit,$offset);
        $query = $this->db->get();
        return $query->result();
        
    }

    public function count_data($param = array()){
        
        $this->db->select('ppof_tbl_div.id_div');
        $this->db->from('ppof_tbl_div');
        // $this->db->where('id_div', $id_div);
        if (is_array($param)) {
            foreach($param as $idx => $data){
                if($idx == 'ppof_tbl_div.div_name'){
                    $this->db->like($idx, $data);
                }
            }
        }
        return $this->db->count_all_results();
    }

}