<?php 

class M_surat extends CI_Model {
	
	public function __construct() {
        parent::__construct();
    }

    public function tujuan_event($id_div){

        $this->db->select('id_div, div_name');
        $this->db->from('ppof_tbl_div');
        // $this->db->where('id_div !=', $id_div);
        $this->db->where('id_div =', $id_div);
        $result = $this->db->get()->result_array();

        foreach ($result as $row) {
            $tujuan_event[$row['id_div']] = $row['div_name']; 
        }
        return $tujuan_event;

    }

    public function get_tujuan($id_surat){
    	$q = $this->db->query("select id_div from ppof_tbl_tujuan where id_surat = '$id_surat'");
    	return $q->result();
    }

    public function check_level($id_group) {
        $this->db->from('ppof_tbl_group');
        $this->db->where('id_group', $id_group);

        $query = $this->db->get();
        return $query->row();
    }

}