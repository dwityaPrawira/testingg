<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="panel">
			<div class="panel-heading">
                Update Group
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <br />
                <?php
                    echo form_open_multipart('C_group/update', 'class="form-horizontal form-label-left" novalidate');
                ?>

                <?php echo form_hidden('idx', $idx); ?>
                
                <div class="form-group">
                    <label for="group_name" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Satker  <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="group_name" class="form-control col-md-7 col-xs-12" placeholder="group" required="required" value="<?php echo $group_name;?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="id_group" class="control-label col-md-3 col-sm-3 col-xs-12">Tujuan Koordinasi</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php echo form_dropdown('ddown_supervisor', $ddown_supervisor, $group_suppervisor, 'class="form-control"'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="id_group" class="control-label col-md-3 col-sm-3 col-xs-12">Role</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php 
                            $options = array(
                                '1' => 'Admin Divisi',
                                '2' => 'Pelaksana',
                                '3' => 'Viewer',
                            );

                            echo form_dropdown('role', $options, '--', 'class="form-control"');
                        ?>
                    </div>
                </div>
               
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
                <?php
                    echo form_close();
                ?>
            </div>
		</div>
	</div>
</div>



