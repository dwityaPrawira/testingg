<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
                <h2>Tambah Employee</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                    echo form_open_multipart('C_employee/mutasi', 'class="form-horizontal form-label-left" novalidate');
                ?>

                <input type="hidden" name="id" value="<?php echo $id;?>">
                <div class="form-group">
                    <label for="npp" class="control-label col-md-3 col-sm-3 col-xs-12">NPP  <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="npp" class="form-control col-md-7 col-xs-12" placeholder="nama" required="required" value="<?php echo $npp;?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="Nama" class="control-label col-md-3 col-sm-3 col-xs-12">Nama  <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="nama" class="form-control col-md-7 col-xs-12" placeholder="nama" required="required" value="<?php echo $nama;?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="id_group" class="control-label col-md-3 col-sm-3 col-xs-12">Divisi</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select name="kategori" id="kategori" class="form-control">
                            <option value="0">-PILIH-</option>
                            <?php foreach($ddown_div->result() as $row):?>
                                <option value="<?php echo $row->id_div;?>"><?php echo $row->div_name;?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="id_group" class="control-label col-md-3 col-sm-3 col-xs-12">Group</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <select name="id_group" class="subkategori form-control">
                            <option value="0">-PILIH-</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
                <?php
                    echo form_close();
                ?>
            </div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#kategori').change(function(){
            var id=$(this).val();
            $.ajax({
                url : "<?php echo base_url();?>index.php/C_employee/get_group",
                method : "POST",
                data : {id: id},
                async : false,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].id_group+'>'+data[i].group_name+'</option>';
                    }
                    $('.subkategori').html(html);
                }
            });
        });
    });
</script>

