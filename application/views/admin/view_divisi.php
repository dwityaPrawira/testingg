<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
                <h2>Update Divisi</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                    echo form_open_multipart('C_divisi/update', 'class="form-horizontal form-label-left" novalidate');
                ?>

                <?php echo form_hidden('idx', $idx); ?>
                
                <div class="form-group">
                    <label for="group_name" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Divisi  <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="div_name" class="form-control col-md-7 col-xs-12" placeholder="nama divisi" required="required" value="<?php echo $div_name;?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="group_name" class="control-label col-md-3 col-sm-3 col-xs-12">Kode Divisi  <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="div_identity" class="form-control col-md-7 col-xs-12" placeholder="kode divisi" required="required" value="<?php echo $div_identity;?>">
                    </div>
                </div>
               
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
                <?php
                    echo form_close();
                ?>
            </div>
		</div>
	</div>
</div>



