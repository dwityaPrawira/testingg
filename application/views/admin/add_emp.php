<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="panel">
			<div class="panel-heading">
                Tambah Employee
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <br />
                <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                <?php
                    echo form_open_multipart('C_employee/insert', 'class="form-horizontal form-label-left" novalidate');
                ?>
                <div class="form-group">
                    <label for="npp" class="control-label col-md-3 col-sm-3 col-xs-12">NIP  <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="npp" class="form-control col-md-7 col-xs-12" placeholder="nip" required="required" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="Nama" class="control-label col-md-3 col-sm-3 col-xs-12">Nama  <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="nama" class="form-control col-md-7 col-xs-12" placeholder="nama" required="required" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="Nama" class="control-label col-md-3 col-sm-3 col-xs-12">Tipe User  <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <select name="category" id="category" class="form-control">
                        <option value="">-- PILIH --</option>
                        <option value="0">HUMAS KSI BPK</option>
                        <option value="1">BPK</option>
                    </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Satker</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" id="sub_category" name="ddown_group" required>
                            <option>No Selected</option>
                        </select>
                    </div>
                  </div>

                <!-- <div class="form-group">
                    <label for="id_group" class="control-label col-md-3 col-sm-3 col-xs-12">Satker</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php // echo form_dropdown('ddown_group', $ddown_group, '', 'class="form-control"'); ?>
                    </div>
                </div> -->

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
                <?php
                    echo form_close();
                ?>
            </div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#category').change(function(){ 
            var id=$(this).val();
            $.ajax({
                url : "<?php echo site_url('C_employee/get_sub_category');?>",
                method : "POST",
                data : {id: id},
                async : true,
                dataType : 'json',
                success: function(data){
                        
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].id_group+'>'+data[i].group_name+'</option>';
                    }
                    $('#sub_category').html(html);

                }
            });
            return false;
        }); 
    });
</script>