<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="panel">
			<div class="panel-heading">
                Tambah Satker
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <br />
                <?php
                    echo form_open_multipart('C_group/insert', 'class="form-horizontal form-label-left" novalidate');
                ?>

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="hidden" name="id_group" value="<?php echo $id_group; ?>" class="form-control col-md-7 col-xs-12" placeholder="kode" required="required" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="Nama" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Satker<span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="nama" class="form-control col-md-7 col-xs-12" placeholder="nama" required="required" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="id_group" class="control-label col-md-3 col-sm-3 col-xs-12">Role</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php 
                            $options = array(
                                '1' => 'Kepala Biro',
                                '4' => 'Head Admin (IT Admin)',
                                '5' => 'Super Admin',
                                '2' => 'User / Inquirer'
                            );

                            echo form_dropdown('role', $options, '--', 'class="form-control"');
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
                <?php
                    echo form_close();
                ?>
            </div>
		</div>
	</div>
</div>



