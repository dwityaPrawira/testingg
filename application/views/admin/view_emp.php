<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="panel">
			<div class="panel-heading">
                Update Employee
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <br />
                <?php
                    echo form_open_multipart('C_employee/update', 'class="form-horizontal form-label-left" novalidate');
                ?>

                <input type="hidden" name="npp" value="<?php echo $npp;?>">
                <input type="hidden" name="id" value="<?php echo $id;?>">
                
                <div class="form-group">
                    <label for="cc-surat" class="control-label col-md-3 col-sm-3 col-xs-12">Nama  <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="nama" class="form-control col-md-7 col-xs-12" placeholder="nama" required="required" value="<?php echo $nama;?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="id_group" class="control-label col-md-3 col-sm-3 col-xs-12">Satker</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php echo form_dropdown('ddown_group', $ddown_group, '', 'class="form-control"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
                <?php
                    echo form_close();
                ?>
            </div>
		</div>
	</div>
</div>



