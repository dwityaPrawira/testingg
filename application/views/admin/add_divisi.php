<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
                <h2>Tambah Employee</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                    echo form_open_multipart('C_divisi/insert', 'class="form-horizontal form-label-left" novalidate');
                ?>

                <div class="form-group">
                    <label for="npp" class="control-label col-md-3 col-sm-3 col-xs-12">NAMA DIVISI  <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="nama" class="form-control col-md-7 col-xs-12" placeholder="nama" required="required" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="Nama" class="control-label col-md-3 col-sm-3 col-xs-12">KODE DIVISI  <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="kode" class="form-control col-md-7 col-xs-12" placeholder="kode" required="required" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
                <?php
                    echo form_close();
                ?>
            </div>
		</div>
	</div>
</div>



