<link href="<?php echo base_url('assets/css/login/bootstrap.min.css'); ?>" rel="stylesheet">
<script src="<?php echo base_url('assets/css/login/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/css/login/jquery.min.js'); ?>"></script>
<!------ Include the above in your HEAD tag ---------->
<!DOCTYPE html>
<html>
<head>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Halaman Login</title>
	<link href="<?php echo base_url('assets/bucket-admin/bs3/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/login-new.css'); ?>" rel="stylesheet">

    <script type="text/javascript">
    $(document).ready(function(){
        <?php
            $flashmessage = $this->session->flashdata ( 'msg_login' );
            if(!empty($flashmessage)){
        ?>
                new PNotify({
                    title: 'Error!',
                    text: '<?php echo $flashmessage;?>',
                    type: 'error',
                    styling: 'bootstrap3'
                });
        <?php
            }
            $flashmessage_scc = $this->session->flashdata ( 'success_message' );
            if(!empty($flashmessage_scc)){
        ?>
                new PNotify({
                    title: 'Success',
                    text: '<?php echo $flashmessage_scc;?>',
                    type: 'success',
                    styling: 'bootstrap3'
                });
        <?php
            }
        ?>
    })
</script>
</head>
<body>
    <?php $flashmessage = $this->session->flashdata ( 'msg_login' );?>
    <div class="container">
        <div class="d-flex justify-content-center h-100">
            <div class="card">
                <div class="card-header">
                <div class="form-signin-heading" style="text-align: center;">
                    <?php            
                        $image_properties = array(
                            'src'   => 'assets/img/evenmanlogogrey.png',
                            'maintain_ratio' => 'TRUE',
                            'class' => 'img-responsive'
                        );
                    ?>
                    <?php
                        echo img($image_properties);
                    ?>
                </div>
                </div>
                <div class="card-body">
                    <?php
                        $attributes = array("autocomplete" => "off",  "class" => "form-signin",); // prevent save to browser cache
                        echo form_open('login/validate', $attributes);
                    ?>
                        <div class="form-group text-danger" style="text-align: center"><?php echo $flashmessage;?></div>

                        <div class="input-group form-group">
                        <span class="input-group-addon" id="iconn1"> <i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" class="form-control" name="username" required="required" placeholder="Username">
                        </div>
                        <div class="input-group form-group">
                            <span class="input-group-addon" id="iconn1"> <i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" class="form-control" name="password" required="required" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Login" class="btn float-right login_btn">
                        </div>
                    <?php echo form_close(); ?>
                </div>
                <div class="card-footer"></div>
            </div>
        </div>
    </div>
</body>
</html>