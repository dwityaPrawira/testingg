<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo $meta_title; ?></title>

        <!-- Bootstrap -->
        <link href="<?php echo base_url('assets/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo base_url('assets/gentelella/vendors/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
        <!-- NProgress -->
        <link href="<?php echo base_url('assets/gentelella/vendors/nprogress/nprogress.css');?>" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="<?php echo base_url('assets/gentelella/build/css/custom.min.css');?>" rel="stylesheet">
        <!-- PNotify -->
        <link href="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.nonblock.css');?>" rel="stylesheet">
        <!-- Summernote text editor CSS-->
        <link href="<?php echo base_url('assets/plugins/summernote/dist/summernote.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css');?>" rel="stylesheet">
        <!-- DataTables Responsive CSS -->
        <link href="<?php echo base_url('assets/plugins/datatables-responsive/css/dataTables.responsive.css');?>" rel="stylesheet">
        <!-- yadcf Datatables -->
        <link href="<?php echo base_url('assets/css/jquery.dataTables.yadcf.css');?>" rel="stylesheet">
        <!-- jQuery -->
        <script src="<?php echo base_url('assets/gentelella/vendors/jquery/dist/jquery.min.js');?>"></script>
        <!-- Date Picker -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/datepicker/css/bootstrap-datepicker.min.css');?>">
        <script type="text/javascript" src="<?php echo base_url('assets/plugins/datepicker/js/bootstrap-datepicker.min.js');?>"></script>
        <!-- Calendar -->
        <!-- <link rel="stylesheet" type="text/css" href="<?php //echo base_url().'assets/css/bootstrap.min.css'; ?>"> -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/style.css'; ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/font-awesome/css/font-awesome.min.css'; ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/fullcalendar/fullcalendar.css'; ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'; ?>">
        <!-- Time Picker -->
        <link rel="stylesheet" href="<?php echo base_url().'assets/dist/wickedpicker.min.css'; ?>">
        <script src="<?php echo base_url('assets/dist/wickedpicker.min.js');?>"></script>
        <!-- Chart -->
        <script src="<?php echo base_url('assets/grafik/chart.js@2.8.0');?>"></script>
    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <?php
                                $CI = & get_instance();
                                $image_properties = array(
                                    'src'   => 'assets/img/logo-single.png',
                                    'alt' => '...',
                                    'maintain_ratio' => 'TRUE',
                                    'width'         => '35',
                                    'height'       => '35',
                                );
                            ?>
                            <a href="<?php echo site_url('pages/index');?>" class="site_title"><?php
                        echo img($image_properties);
                    ?>&nbsp;<span>Aplikasi Event</span></a>
                        </div>
                        <div class="clearfix"></div>
                        <!-- menu profile quick info -->
                        <?php
                            $CI = & get_instance();
                            $image_properties = array(
                                'src'   => 'assets/photos/'.$CI->nativesession->get('photo'),
                                'alt' => '...',
                                'class' => 'img-circle profile_img'
                            );
                        ?>
                        <div class="profile clearfix">
                            <div class="profile_pic">
                                <?php
                                    echo img($image_properties);
                                ?>
                            </div>
                            <div class="profile_info">
                                <span>Selamat Datang,</span>
                                <h2><?php echo $CI->nativesession->get('nama_user');?></h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info -->
                        <br />
                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                            <div class="menu_section">
                                <h3>Main Menu</h3>
                                <ul class="nav side-menu">
                                    <?php
                                        foreach ($menu_utama as $rows) {
                                            if($rows->parent != 1){ // check if menu has sub-menu

                                                if($rows->nama == 'Approval') {
                                                    echo "<li>";
                                                    echo anchor($rows->url, '<i class="'.$rows->icons.'"></i> '.$rows->nama.' <span class="badge badge-danger">('.$count_approval.')</span>');
                                                    echo "</li>";
                                                } else {
                                                    echo "<li>";
                                                    echo anchor($rows->url, '<i class="'.$rows->icons.'"></i> '.$rows->nama);
                                                    echo "</li>";
                                                }
                                                
                                            } else {
                                                echo '<li>';
                                                echo '<a href="#"><i class="fa fa-bar-chart-o"></i> '.$rows->nama.'<span class="fa arrow"></span></a>';
                                                $sub_menu = $CI->m_main->get_submenu($rows->id_menu); // generate sub-menu
                                                foreach ($sub_menu as $rowz ) {
                                                    echo '<ul class="nav nav-second-level">';
                                                    echo '<li>';
                                                    echo '<a href='.$rowz->url.'>'.$rowz->nama.'</a>';
                                                    echo '</li>';
                                                    echo '</ul>';
                                                }
                                                echo '</li>';
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <?php
                                            $image_properties = array(
                                                'src'   => 'assets/photos/'.$CI->nativesession->get('photo')
                                            );
                                            echo img($image_properties).''.$CI->nativesession->get('nama_user');
                                        ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li>
                                            <a href="javascript:;">
                                                <i class="fa fa-user"></i> <?php echo $CI->session->userdata('username');?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <i class="fa fa-briefcase"></i> <?php echo strtoupper($CI->nativesession->get('group_name'));?>
                                            </a>
                                        </li>
                                        <li>
                                            <?php echo anchor('c_updatepassword', '<i class="fa fa-qrcode"></i> Ubah Profil'); ?>
                                        </li>
                                        <li>
                                            <?php echo anchor('login/logout', '<i class="fa fa-sign-out pull-right"></i> Logout'); ?>
                                            <!--<a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a>-->
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->
                <!-- page content -->
                <div class="right_col" role="main">
                    <?php if (!empty($content)): ?>
                        <?php $this->load->view($content); ?>
                    <?php else: ?>
                        <?php echo 'Halaman tidak ada'; ?>
                    <?php endif; ?>
                </div>
                <!-- /page content -->
                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        KONSOLIDATOR DAN PENGATUR JADWAL ACARA KEGIATAN &copy; 2021 All Rights Reserved.</a>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
        </div>
        <!-- Bootstrap -->
        <script src="<?php echo base_url('assets/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js');?>"></script>
        <!-- NProgress -->
        <script src="<?php echo base_url('assets/gentelella/vendors/nprogress/nprogress.js');?>"></script>
        <!-- Parsley -->
        <script src="<?php echo base_url('assets/gentelella/vendors/parsleyjs/dist/parsley.min.js');?>"></script>
        <!-- validator -->
        <script src="<?php echo base_url('assets/gentelella/vendors/validator/validator.js');?>"></script>
        <!-- jQuery autocomplete -->
        <script src="<?php echo base_url('assets/gentelella/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js');?>"></script>
        <!-- PNotify -->
        <script src="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.js');?>"></script>
        <script src="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js');?>"></script>
        <script src="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.nonblock.js');?>"></script>
        <!-- Custom Theme Scripts -->
        <script src="<?php echo base_url('assets/gentelella/build/js/custom.js');?>"></script>
        <!--Summernote text editor JS -->
        <script src="<?php echo base_url('assets/plugins/summernote/dist/summernote.js');?>"></script>
        <!-- DataTables JavaScript -->
        <script src="<?php echo base_url('assets/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
        <script src="<?php echo base_url('assets/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js');?>"></script>
        <!-- yadcf Javascript -->
        <script src="<?php echo base_url('assets/js/jquery.dataTables.yadcf.js');?>"></script>
        <script>
            var site = "<?php echo site_url();?>";
            var i = 1;
            var k = 1;
            $(document).ready(function(){
                setAutocomplete();
                $("form").on("click", "#generate", tujuan);
                $("form").on("click", "#generatecc", cc);
                $("form").on("click", "#generateupload", upload);

                $("form").on("click", ".hapus-tujuan", function(){
                    $(this).parent('span').parent('div').parent('div').parent('div').remove();
                })
                $("form").on("click", ".hapus-cc", function(){
                    $(this).parent('span').parent('div').parent('div').parent('div').remove();
                })
            });
            function setAutocomplete(){

                $('.autocomplete').autocomplete({
                    serviceUrl: site+'/c_autocomplete/search_employee',
                    onSelect: function (suggestion) {
                        $(this).parent('div').children('.divid').val(suggestion.data);
                    }
                });
                $('.autocomplete2').autocomplete({
                    serviceUrl: site+'/c_autocomplete/search_employee',
                    onSelect: function (suggestion) {
                        $(this).parent('div').children('.dividcc').val(suggestion.data);
                    }
                });
            }

            var tujuan = function(){
                var num = $('input[name="div_name"]').length;
                var clone = '<div class="item form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="tujuan-surat"></label>' +
                '<div class="col-md-6 col-sm-6 col-xs-12"><div class="input-group"><input type="text" name="div_name" class="form-control autocomplete col-md-7 col-xs-12" placeholder="Tujuan Surat"><span class="input-group-btn"><button class="btn btn-default hapus-tujuan" type="button"><i class="fa fa-trash-o"></i></button></span><input type="hidden" name="div_id[]" class="divid" required="required"></div></div>';
                $("#tujuan-div").append(clone);
                setAutocomplete();
            }

            var cc = function(){
                var num = $('input[name="div_name"]').length;
                var clone = '<div class="item form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="cc-surat"></label>' +
                    '<div class="col-md-6 col-sm-6 col-xs-12"><div class="input-group"><input type="text" name="cc_name" class="form-control autocomplete2 col-md-7 col-xs-12" placeholder="CC Surat"><span class="input-group-btn"><button class="btn btn-default hapus-cc" type="button"><i class="fa fa-trash-o"></i></button></span><input type="hidden" name="div_idcc[]" class="dividcc"></div></div>';
                $("#cc-div").append(clone);
                setAutocomplete();
            }

            var upload = function(){
                var clone = '<input type="file" name="userfile[]" size="20" id="file"/>';
                $("#new").append(clone);
            }
        </script>
    </body>
</html>