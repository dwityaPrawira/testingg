<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $meta_title; ?></title>

    <!--Core CSS -->
    <link href="<?php echo base_url('assets/bucket-admin/bs3/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/bucket-admin/js/jquery-ui/jquery-ui-1.10.1.custom.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/bucket-admin/css/bootstrap-reset.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/bucket-admin/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/bucket-admin/js/jvector-map/jquery-jvectormap-1.2.2.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/bucket-admin/css/clndr.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/bucket-admin/css/wickedpicker.min.css'); ?>" rel="stylesheet">

    <!--clock css-->
    <link href="<?php echo base_url('assets/bucket-admin/js/morris-chart/morris.css'); ?>" rel="stylesheet">

    <!-- PNotify -->
    <link href="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.nonblock.css'); ?>" rel="stylesheet">

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bucket-admin/js/css3clock/css/style.css'); ?>">

    <?php if ($useDatatable) : ?>
        <link href="<?php echo base_url('assets/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css'); ?>" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="<?php echo base_url('assets/plugins/datatables-responsive/css/dataTables.responsive.css'); ?>" rel="stylesheet">

        <!-- yadcf Datatables -->
        <link href="<?php echo base_url('assets/css/jquery.dataTables.yadcf.css'); ?>" rel="stylesheet">
    <?php endif; ?>

    <?php if (isset($use_calendar)) : ?>
        <!--calendar css-->
        <link href="<?php echo base_url('assets/bucket-admin/js/fullcalendar/bootstrap-fullcalendar.css'); ?>" rel="stylesheet">
    <?php endif; ?>

    <!-- Summernote text editor CSS-->
    <link href="<?php echo base_url('assets/plugins/summernote-0.8.18/summernote.css'); ?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href=" <?php echo base_url('assets/bucket-admin/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/bucket-admin/css/style-responsive.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/bucket-admin/css/autocomplete-custom.css'); ?>" rel="stylesheet">

    <script src="<?php echo base_url('assets/bucket-admin/js/jquery.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/wickedpicker.min.js'); ?>"></script>

    <!-- Chart -->
    <!-- <script src="<?php echo base_url('assets/grafik/chart.js@2.8.0'); ?>"></script> -->

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>

<body>
    <section id="container">
        <!--header start-->
        <header class="header fixed-top clearfix">
            <!--logo start-->
            <div class="brand">

                <a href="index.html" class="logo" style="margin-left: 20px;">
                    <?php
                    $CI = &get_instance();
                    $image_properties = array(
                        'src'   => 'assets/img/LOGOEmanager.png',
                        // 'src'   => 'assets/img/new_logo_ksi_putih.png',
                        'alt' => '...',
                        'class' => 'img-responsive'
                    );
                    ?>
                    <?php
                    echo img($image_properties);
                    ?>
                </a>
                <div class="sidebar-toggle-box">
                    <div class="fa fa-bars"></div>
                </div>
            </div>
            <!--logo end-->

            <div class="nav notify-row" id="top_menu">
                <!--  check level, admin dan tier 3 tidak ada approval -->
                <?php 
                    $grp_lvl = $this->nativesession->get('group_level');
                    if($grp_lvl == '4'){
                ?>
                    
                <?php } else if($grp_lvl == '3') {?>
                    <ul class="nav top-menu">
                        <li id="header_inbox_bar">
                            <a href="<?php echo base_url('index.php/c_inbox'); ?>">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-important"><?php echo $count_arsip; ?></span>
                            </a>
                        </li>
                    </ul>
                <?php } else if($grp_lvl == '2') {?>
                    <ul class="nav top-menu">
                        <li id="header_inbox_bar">
                            <a href="<?php echo base_url('index.php/c_inbox'); ?>">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-important"><?php echo $count_arsip; ?></span>
                            </a>
                        </li>
                    </ul>
                <?php }
                else { ?>
                    <ul class="nav top-menu">
                        <li class="dropdown">
                            <?php echo anchor(
                                'c_approval',
                                '<i class="fa fa-gavel"></i>
                                <span class="badge bg-success">' . $count_approval . '</span>',
                                array('class' => 'dropdown-toggle', 'data-toggle' => 'data-toggle')
                            ); ?>
                        </li>
                        <li id="header_inbox_bar">
                            <a href="<?php echo base_url('index.php/c_inbox'); ?>">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-important"><?php echo $count_arsip; ?></span>
                            </a>
                        </li>
                    </ul>
                <?php } ?>
            </div>

            <div class="top-nav clearfix">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu">
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <?php
                            $image_properties = array(
                                'src'   => 'assets/photos/' . $CI->nativesession->get('photo')
                            );
                            echo img($image_properties) . '<span class="username">' . $CI->nativesession->get('nama_user') . '</span>';
                            ?>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <li><?php echo anchor('c_updatepassword', '<i class="fa fa-suitcase"></i> Profil'); ?></li>
                            <li><a href="javascript:;">
                                    <i class="fa fa-briefcase"></i> <?php echo ($CI->nativesession->get('group_name').strlen() > 35 ) ? strtoupper($CI->nativesession->get('group_name')) : substr(strtoupper($CI->nativesession->get('group_name')), 0, 35) . '...' ; ?>
                                </a>
                            </li>
                            <li><?php echo anchor('login/logout', '<i class="fa fa-key"></i> Logout'); ?></li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!--search & user info end-->
            </div>
        </header>
        <!--header end-->
        <!--sidebar start-->
        <aside>
            <div id="sidebar" class="nav-collapse">
                <!-- sidebar menu start-->
                <div class="leftside-navigation">
                    <ul class="sidebar-menu" id="nav-accordion">
                        <?php
                        foreach ($menu_utama as $rows) {
                            if ($rows->parent != 1) { // check if menu has sub-menu
                                echo '<li>';
                                echo anchor($rows->url, '<i class="' . $rows->icons . '"></i> <span>' . $rows->nama . '</span>');;
                                echo '</li>';
                            } else {
                                echo '<li class="sub-menu">';
                                echo anchor($rows->url, '<i class="' . $rows->icons . '"></i> ' . $rows->nama . ' <span class="badge badge-danger">(' . $count_approval . ')</span>');
                                echo "</li>";
                            }
                        }
                        ?>
                    </ul>
                </div>
                <!-- sidebar menu end-->
            </div>
        </aside>
        <!--sidebar end-->
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
                <?php if (!empty($content)) : ?>
                    <?php $this->load->view($content); ?>
                <?php else : ?>
                    <?php echo 'Halaman tidak ada'; ?>
                <?php endif; ?>
            </section>
        </section>
        <!--main content end-->
    </section>

    <script src="<?php echo base_url('assets/bucket-admin/js/jquery-ui/jquery-ui-1.10.1.custom.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/bs3/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/jquery.dcjqaccordion.2.7.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/jquery.scrollTo.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/jquery.nicescroll.js'); ?>"></script>

    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
    <script src="<?php echo base_url('assets/bucket-admin/js/skycons/skycons.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/jquery.scrollTo/jquery.scrollTo.js'); ?>"></script>
    <script src="http:////cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/calendar/clndr.js'); ?>"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/calendar/moment-2.2.1.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/evnt.calendar.init.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/jvector-map/jquery-jvectormap-1.2.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/jvector-map/jquery-jvectormap-us-lcc-en.js'); ?>"></script>

    <!--clock init-->
    <script src="<?php echo base_url('assets/bucket-admin/js/css3clock/js/css3clock.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/jquery.customSelect.min.js'); ?>"></script>

    <!--Easy Pie Chart-->
    <script src="<?php echo base_url('assets/bucket-admin/js/easypiechart/jquery.easypiechart.js'); ?>"></script>

    <!--Sparkline Chart-->
    <script src="<?php echo base_url('assets/bucket-admin/js/sparkline/jquery.sparkline.js'); ?>"></script>

    <!--Morris Chart-->
    <script src="<?php echo base_url('assets/bucket-admin/js/morris-chart/morris.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/morris-chart/raphael-min.js'); ?>"></script>

    <!--jQuery Flot Chart-->
    <!-- <script src="<?php echo base_url('assets/bucket-admin/js/flot-chart/jquery.flot.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/flot-chart/jquery.flot.tooltip.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/flot-chart/jquery.flot.resize.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/flot-chart/jquery.flot.pie.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/flot-chart/jquery.flot.animator.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/bucket-admin/js/flot-chart/jquery.flot.growraf.js'); ?>"></script> -->

    <!-- PNotify -->
    <script src="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.js'); ?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js'); ?>"></script>
    <script src="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.nonblock.js'); ?>"></script>

    <script src="<?php echo base_url('assets/gentelella/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js'); ?>"></script>

    <?php if ($useDatatable) : ?>
        <!-- DataTables JavaScript -->
        <script src="<?php echo base_url('assets/plugins/datatables/media/js/jquery.dataTables.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js'); ?>"></script>
        <!-- yadcf Javascript -->
        <script src="<?php echo base_url('assets/js/jquery.dataTables.yadcf.js'); ?>"></script>
    <?php endif; ?>

    <?php if (isset($use_calendar)) : ?>
        <!--calendar js-->
        <script src="<?php echo base_url('assets/bucket-admin/js/fullcalendar/fullcalendar.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/bucket-admin/js/external-dragging-calendar.js'); ?>"></script>
    <?php endif; ?>

    <!--Summernote text editor JS -->
    <script src="<?php echo base_url('assets/plugins/summernote-0.8.18/summernote.js'); ?>"></script>

    <!--common script init for all pages-->
    <script src=" <?php echo base_url('assets/bucket-admin/js/scripts.js'); ?>">
    </script>
    <script>
        var site = "<?php echo site_url(); ?>";
        var i = 1;
        var k = 1;
        $(document).ready(function() {
            setAutocomplete();
            $("form").on("click", "#generate", tujuan);
            $("form").on("click", "#generatecc", cc);
            $("form").on("click", "#generateupload", upload);

            $("form").on("click", ".hapus-tujuan", function() {
                $(this).parent('span').parent('div').parent('div').parent('div').remove();
            })
            $("form").on("click", ".hapus-cc", function() {
                $(this).parent('span').parent('div').parent('div').parent('div').remove();
            })
        });

        function setAutocomplete() {

            $('.autocomplete').autocomplete({
                serviceUrl: site + '/c_autocomplete/search_employee',
                onSelect: function(suggestion) {
                    $(this).parent('div').children('.divid').val(suggestion.data);
                }
            });
            $('.autocomplete2').autocomplete({
                serviceUrl: site + '/c_autocomplete/search_employee',
                onSelect: function(suggestion) {
                    $(this).parent('div').children('.dividcc').val(suggestion.data);
                }
            });
        }

        var tujuan = function() {
            var num = $('input[name="div_name"]').length;
            var clone = '<div class="item form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="tujuan-surat"></label>' +
                '<div class="col-md-6 col-sm-6 col-xs-12"><div class="input-group"><input type="text" name="div_name" class="form-control autocomplete col-md-7 col-xs-12" placeholder="Tujuan Surat"><span class="input-group-btn"><button class="btn btn-default hapus-tujuan" type="button"><i class="fa fa-trash-o"></i></button></span><input type="hidden" name="div_id[]" class="divid" required="required"></div></div>';
            $("#tujuan-div").append(clone);
            setAutocomplete();
        }

        var cc = function() {
            var num = $('input[name="div_name"]').length;
            var clone = '<div class="item form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="cc-surat"></label>' +
                '<div class="col-md-6 col-sm-6 col-xs-12"><div class="input-group"><input type="text" name="cc_name" class="form-control autocomplete2 col-md-7 col-xs-12" placeholder="CC Surat"><span class="input-group-btn"><button class="btn btn-default hapus-cc" type="button"><i class="fa fa-trash-o"></i></button></span><input type="hidden" name="div_idcc[]" class="dividcc"></div></div>';
            $("#cc-div").append(clone);
            setAutocomplete();
        }

        var upload = function() {
            var clone = '<input type="file" name="userfile[]" size="20" id="file"/>';
            $("#new").append(clone);
        }
    </script>

    <?php if (isset($customJs)) : ?>
        <?php if (isset($get_data)) : ?>
            <script type="text/javascript">
                var get_data = '<?php echo $get_data; ?>';
                var backend_url = '<?php echo base_url(); ?>';
            </script>
        <?php endif; ?>

        <script src="<?php echo base_url('assets/bucket-admin/js/custom/' . $customJs); ?>"></script>
    <?php endif; ?>

    <script src="<?php echo base_url('assets/bucket-admin/js/custom/show_done_approve.js'); ?>"></script>
</body>

</html>