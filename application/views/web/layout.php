<div style="margin-bottom: 50px;">
<?php $this->load->view('web/components/header');?></div>
<div class="container">
    <div class="container">
        <div class="row-fluid">
            <div class="span2" >
                <?php $this->load->view('web/components/navigation');?>
            </div>
            <div class="span10">
                <div class="content">
                    <section class="panel panel-info">
                        <div class="panel-heading"><?php echo $meta_title;?></div>
                        <div class="panel-body" style="min-height: 450px;">
                            <?php if (!empty($content)): ?>
                                <?php $this->load->view($content); ?>
                            <?php else: ?>
                                <?php echo 'Halaman tidak ada'; ?>
                            <?php endif; ?>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('web/components/footer');?>
</div>

