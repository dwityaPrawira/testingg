<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title><?php echo $meta_title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="<?php echo base_url('assets/img/bnisyariah.ico'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/metisMenu/metisMenu.min.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/morrisjs/morris.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/metisMenu/metisMenu.min.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/sb-admin-2.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.autocomplete.css'); ?>">

        <script src="<?php echo base_url('assets/js/jquery-1.8.2.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.autocomplete.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/metisMenu/metisMenu.min.js'); ?>" ></script> <!-- Metis Plugin -->
        <script src="<?php echo base_url('assets/js/sb-admin-2.js'); ?>" ></script>  <!-- Custom Theme JavaScript -->
        <script src="<?php echo base_url('assets/js/nicEdit.js'); ?>" ></script>
        
        <script type="text/javascript">
            var site = "<?php echo site_url();?>";
            var i = 1;
            var k = 1;
            $(document).ready(function(){
                $("#autocomplete").focus();
                setAutocomplete();  
                $("#generate").live("click", tujuan);
                $("#generatecc").live("click", cc);
            });
            function setAutocomplete(){
                
                $('.autocomplete').autocomplete({
                    serviceUrl: site+'/c_autocomplete/search',
                    onSelect: function (suggestion) {
                        if(i == '1'){
                            $(".divid").val(suggestion.data);
                        }
                        else{
                            var j = i - 1;
                            $(".divid"+j).val(suggestion.data);   
                        }
                    }
                });
                $('.autocomplete2').autocomplete({
                    serviceUrl: site+'/c_autocomplete/search',
                    onSelect: function (suggestion) {
                        if(k == '1'){
                            $(".dividcc").val(suggestion.data);
                        }
                        else{
                            var xx = k - 1;
                            $(".dividcc"+xx).val(suggestion.data);
                        }
                    }
                });
            }

            var tujuan = function(){
                var num = $("#autocomplete").val();
                $clone = $("#autocomplete").clone();  
                $clonediv = $("#div_id").clone();  
                $clone.val("");
                $clonediv.val("");
                $clone.removeClass("autocomplete").attr("class","form-control autocomplete divform"+i);
                $clonediv.removeClass("divid").attr("class", "divid"+i);
                var r= $('<div id="mg_input"><input class="btn-xs" type="button" value="Hapus" onclick="reset_html2(\'divid'+i+'\'); reset_field(\'divform'+i+'\'); delete_button(\'butid'+i+'\')" /></div>');
                r.attr("class", "btn btn-xs butid"+i);
                ++i;
                $clone.appendTo(".target");
                $clonediv.appendTo(".target");
                $(".target").append(r);
                setAutocomplete();
            }

            var cc = function(){
                var num = $("#autocomplete2").val();
                $clone2 = $("#autocomplete2").clone();  
                $clonediv2 = $("#div_id2").clone();  
                $clone2.val("");
                $clonediv2.val("");
                $clone2.removeClass("autocomplete2").attr("class","form-control autocomplete2 divformcc"+k);
                $clonediv2.removeClass("dividcc").attr("class", "dividcc"+k);
                var r= $('<div id="mg_input"><input type="button" value="Hapus" onclick="reset_html2(\'dividcc'+k+'\'); reset_field(\'divformcc'+k+'\'); delete_button(\'butidcc'+k+'\')" /></div>');
                r.attr("class", "btn btn-xs butidcc"+k);
                ++k;
                $clone2.appendTo(".target2");
                $clonediv2.appendTo(".target2");
                $(".target2").append(r);
                setAutocomplete();
            }

            function reset_html2(id){
                $('.'+id).remove();
            }

            function reset_field(id){
                $('.'+id).remove();
            }

            function delete_button(id){
                $('.'+id).remove(); 
                
            }
        </script>   
        
        <style type="text/css">
        	li.header {
			    color: #4b646f;
			    background: #FFFFFF;
			    padding: 10px;
			}
			
			li.header2 {
				text-align: center;
				color: #4b646f;
			    padding: 10px;
			}
			
			li.headinfo {
				text-align:center;
			}
        </style>
    </head>
    <body>
        <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php $this->load->view('web/components/header'); ?> <!-- /.navigation header -->
            <?php $this->load->view('web/components/navigation');?> <!-- /.left navigation -->
        </nav>
        <div id="page-wrapper">
            <?php if (!empty($content)): ?>
                <?php $this->load->view($content); ?>
            <?php else: ?>
                <?php echo 'Halaman tidak ada'; ?>
            <?php endif; ?>    
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </body>

</html>
