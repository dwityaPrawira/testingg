<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
    $(document).ready(function(){
        <?php
            $flashmessage = $this->session->flashdata ( 'msg_inbox' );
            if(!empty($flashmessage)){
        ?>
            new PNotify({
                title: 'Maaf !',
                text: '<?php echo $flashmessage;?>',
                type: 'warning',
                styling: 'bootstrap3'
            });
        <?php
            }
        ?>
    })
</script>
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><?php echo anchor('pages/index', 'Beranda');?></li>
            <li class="active">Inbox</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <?php
                echo $table;
                echo $pagination;
                ?>
            </div>
        </div>
    </div>
</div>