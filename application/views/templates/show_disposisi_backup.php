<br><br>
<div class="panel panel-default">
  <div class="panel-heading">Monitor Disposisi</div>
  <div class="panel-body"><?php echo $tracking;?></div>  
</div>

<div class="panel panel-default">
  <div class="panel-heading">Pesan Disposisi</div>
  <div class="panel-body"><?php echo $tracking_message;?></div>  
</div>
<table cellpadding="50" border="0" class="table">
<?php $nota_id = $this->uri->segment(3);?>
    <tr>
    	<td>Klasifikasi</td>
       	<td> : <?php echo strtoupper($tipe);?></td>
    </tr>
    <tr>
    	<td>Nomor Surat</td>
        <td>: <?php echo $generate_number;?></td>
    </tr>
	<tr>
    	<td>Tanggal</td>
       	<td> : <?php echo $tgl_create;?></td>
    </tr>

    <tr>
    	<td valign="top">Kepada</td>
       	<td><?php echo $table_get_to;?></td>
    </tr>
    <tr>
    	<td valign="top">Cc</td>
       	<td><?php echo $table_get_cc;?></td>
    </tr>
    <tr>
    	<td>Dari</td>
       	<td> : <?php echo $from;?></td>
    </tr>
    <tr>
    	<td>Hal</td>
       	<td> : <?php echo $subyek_surat;?></td>
    </tr>

    <?php if ($attach != null ) { ?>
    <tr>
    	<td>Lampiran</td>
        <td><?php echo $attach; ?></td>
    </tr>
    <?php } ?> 
</table>
<hr>
<table width="100%" style="overflow: hidden;" border=1>
    <tr>
   		<td>
        <div style="width: 100%; overflow: hidden; padding: 20px;">
			<?php echo $content_surat;?>
        </div>
        </td>
	</tr>
</table>
<table style="padding-top:20px">
	<tr>
    	<td></td>
    </tr>
    <tr>
    	<td><?php //echo $nama_divisi;?></td>
    </tr>
    
    <tr>
    	<td><?php //echo $ttd_memo; ?></td>
    </tr>
    
    <tr>
    	<td><?php //echo "<u>".$nama_pemp."</u>";?></td>
    </tr>
    
    <tr>
    	<td><?php //echo $nama_jabatan;?></td>
    </tr>
</table>
<br />
<p><?php echo anchor('c_disposisi/index', 'back', array('class' => 'back'));?></p>
