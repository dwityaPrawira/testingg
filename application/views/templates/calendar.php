<!-- <section class="panel">
    <header class="panel-heading">
        Kalender Kegiatan
    </header>
    <div class="panel-body">
        <div class="row">
            <aside class="col-lg-12 col-md-12 hidden-sm hidden-xs">
                <div id="calendarIO" class="has-toolbar"></div>
            </aside>

            <aside class="hidden-lg hidden-md col-sm-12 col-xs-12">
                <div id="calendarMobile" class="has-toolbar"></div>
            </aside>
        </div>
    </div>
</section> -->

<div id="calendarIO" class="has-toolbar"></div>

<div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Detil even</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal bucket-form">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Nama Event</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="title" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Tanggal</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="start_date" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Waktu</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="waktuawal" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Biro Humas Pendukung</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="biro" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Dukungan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="dukungan" readonly>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>