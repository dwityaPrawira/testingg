<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<script type="text/javascript">
    $(document).ready(function() {
        // $('.example2').hide().before('<a href="#" id="toggle-example2" class="btn btn-danger"><i class="fa fa-times"></i> Tolak Event</a>');
        $('.example2').hide();
        $('.disposisi').hide().before('<a href="#" id="toggle-example3" class="btn btn-primary"><i class="fa fa-check"></i> Disposisi Event</a>');
        $('.kembali').hide().before('<a href="#" id="toggle-example5" class="btn btn-primary"><i class="fa fa-reply"></i> Kembalikan Event</a>');
        $('.komentar_surat').hide().before('<a href="#" id="toggle-example4" class="btn btn-primary"><i class="fa fa-comments"></i> Komentar Surat</a>');
        $('a#toggle-example2').click(function() {
            $('.example2').slideToggle(1000);
            return false;
        });

        $('a#toggle-example3').click(function() {
            $('.disposisi').slideToggle(1000);
            return false;
        });

        $('a#toggle-example4').click(function() {
            $('.komentar_surat').slideToggle(1000);
            return false;
        });

        $('a#toggle-example5').click(function() {
            $('.kembali').slideToggle(1000);
            return false;
        });

        $("#generate").click(function() {
            $clone = $("#source").clone();
            $clone.appendTo("#target");
        });
        $(document).ready(function() {
            $("#summernote").summernote({
                height: 200,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline']]
                ]
            });
        });
    });
</script>
<style>
.tujuan {
  /* border: 1px solid red;  */
  /* margin-left: -8px; */
}
</style>
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><?php echo anchor('pages/index', 'Beranda'); ?></li>
            <li><?php echo anchor('c_approval', 'Approval'); ?></li>
            <li class="active">Detail Approval Event</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                Detail Approval Event
            </header>
            <div class="panel-body">
                <table cellpadding="50" border="0" class="table borderless table-striped">

                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo format_datetime($tgl_create); ?></td>
                    </tr>

                    <tr>
                        <td valign="top">Tujuan</td>
                        <td>:</td>
                        <td><div class="tujuan"><?php echo $table_get_to; ?></div></td>
                    </tr>
                    <tr>
                        <td>Nama Kegiatan</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo $subyek_surat; ?></td>
                    </tr>

                    <tr>
                        <td>Diteruskan Kepada</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo $biro; ?></td>
                    </tr>

                    <tr>
                        <td>Dukungan</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo $dukungan; ?></td>
                    </tr>

                    <tr>
                        <td>Tanggal Pelaksanaan</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo $tglawal; ?> - <?php echo $tglakhir; ?></td>
                    </tr>

                    <tr>
                        <td>Waktu Pelaksanaan</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo $waktuawal; ?> - <?php echo $waktuakhir; ?></td>
                    </tr>

                    <?php if ($attach != null) { ?>
                        <tr>
                            <td>Lampiran</td>
                            <td>:</td>
                            <td><?php echo $attach; ?></td>
                        </tr>
                    <?php } ?>
                </table>                    
                </section>
                <!--progress bar end-->
                <section class="panel">
                    <div class="panel-heading">
                        Notes
                    </div>
                    <div class="panel-body">
                        <?php echo $content_surat; ?>
                    </div>
                </section>

                <!-- <section class="panel">
                    <div class="panel-heading">
                        Notes Approval
                    </div>
                    <div class="panel-body">
                    <?php // echo $table_notes_approval;?>
                    </div>
                </section> -->
            </div>
        </section>
        
        <section class="panel">
            <div class="panel-heading">
                Pesan Approve / Tolak
            </div>
            <?php echo form_open('c_approval/aksi/' . $this->uri->segment(3) . '/' . $id_surat); ?>

        
        <?php if ($display_dd != "none") { ?>
            
                <div class="panel-body">
                    <?php // echo form_open('c_approval/approve/' . $this->uri->segment(3) . '/' . $id_surat); ?>
                    <div class="item form-group col-md-12 col-sm-12 col-xs-12">
                        <label for="biro" class="control-label col-md-3 col-sm-3 col-xs-12">Diteruskan kepada <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-check-input move-left" type="checkbox" name="check_list[]" alt="Checkbox" value="PI"> PI
                            <input class="form-check-input move-left" type="checkbox" name="check_list[]" alt="Checkbox" value="HAL"> HAL
                            <input class="form-check-input move-left" type="checkbox" name="check_list[]" alt="Checkbox" value="KSI"> KSI
                            <input class="form-check-input move-left" type="checkbox" name="check_list[]" alt="Checkbox" value="MUSEUM"> MUSEUM
                        </div>
                    </div>
                    
                    <?php // echo form_close(); ?>
                </div>
            
        <?php } else { ?>
            
        <?php  } ?>
            
            <section class="panel">
                <div class="panel-body">
                    <div class="item form-group col-md-12 col-sm-12 col-xs-12">
                        <textarea class="form-control" rows="3" name="somenotes" id="summernote"></textarea><br>
                        <button type="submit" name="submit_val" value="1" class="btn btn-primary"><i class="fa fa-check"></i> Approve</button>
                        <button type="submit" name="submit_val" value="2" class="btn btn-danger"><i class="fa fa-times"></i> Tolak</button>
                    </div>
                </div>            
                
            </section>
            <?php echo form_close(); ?>

        </section>
    </div>
</div>