<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<style>
.tujuan {
  /* border: 1px solid red;  */
  margin-left: 1px;
}
</style>

<script type="text/javascript">
    $(document).ready(function() {
        $('.disposisi').hide().before('<a href="#" id="toggle-example3" class="btn btn-primary"><i class="fa fa-level-down"></i> Disposisi Event</a>');
        $('.komentar').hide().before('<a href="#" id="toggle-example4" class="btn btn-primary"><i class="fa fa-level-down"></i> Update Status</a>');
        $('a#toggle-example3').click(function() {
            $('.disposisi').slideToggle(1000);
            return false;
        });
        $('a#toggle-example4').click(function() {
            $('.komentar').slideToggle(1000);
            return false;
        });
        $("#generate").click(function() {
            $clone = $("#source").clone();
            $clone.appendTo("#target");
        });
        $(document).ready(function() {
            $("#summernote").summernote({
                height: 200,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline']]
                ]
            });
        });

        $("form").on("click", "#generate_disposisi", generate_disposisi);
        $("form").on("click", ".hapus-disposisi", function() {
            $(this).parent('span').parent('div').parent('div').parent('div').remove();
        })
    });
    var generate_disposisi = function() {
        var clone = '<div class="form-group"><div class="col-md-4 col-sm-4 col-xs-12">';
        clone += '<div class="input-group"><select name="option_disposisi[]" class="form-control">';
        <?php
        foreach ($option_disposisi as $key => $row) {
        ?>
            clone += '<option value="<?php echo $key; ?>"><?php echo $row; ?></option>';
        <?php
        }
        ?>
        clone += '</select><span class="input-group-btn"><button class="btn btn-default hapus-disposisi" type="button"><i class="fa fa-trash-o"></i></button></span></div></div></div>';
        $("#disposisi_div").append(clone);
    }
</script>

<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><?php echo anchor('pages/index', 'Beranda'); ?></li>
            <li><?php echo anchor('c_inbox', 'Inbox'); ?></li>
            <li class="active">Detail Arsip Event</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                Detail Arsip Event
            </header>
            <div class="panel-body">
                <table cellpadding="50" border="0" class="table borderless table-striped">
                    <?php $nota_id = $this->uri->segment(3); ?>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo format_datetime($tgl_create); ?></td>
                    </tr>
                    <tr>
                        <td valign="top">Tujuan</td>
                        <td>:</td>
                        <td><div class="tujuan"><?php echo $table_get_to; ?></div></td>
                    </tr>
                    <tr>
                        <td>Nama Kegiatan</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo $subyek_surat; ?></td>
                    </tr>
                    <tr>
                        <td>Diteruskan Kepada</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo $biro; ?></td>
                    </tr>
                    <tr>
                        <td>Dukungan</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo $dukungan; ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal Pelaksanaan</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo $tglawal; ?> - <?php echo $tglakhir; ?></td>
                    </tr>
                    <tr>
                        <td>Waktu Pelaksanaan</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo $waktuawal; ?> - <?php echo $waktuakhir; ?></td>
                    </tr>
                    <?php if ($attach != null) { ?>
                        <tr>
                            <td>Lampiran</td>
                            <td>:</td>
                            <td>&nbsp;&nbsp;<?php echo $attach; ?></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </section>

        <section class="panel">
            <div class="panel-heading">
                Notes
            </div>
            <div class="panel-body">
                <?php echo $content_surat; ?>
            </div>
        </section>

        <section class="panel">
            <div class="panel-heading">
                Notes Approval
            </div>
            <div class="panel-body">
                <?php echo $table_notes_approval; ?>
            </div>
        </section>

        <?php //echo $flag_level;?>

        <?php if ($flag_level != 1) { ?>
            <section class="panel">
                <div class="panel-heading">
                    Pesan
                </div>
                <div class="panel-body">
                    <?php echo !empty($pesan_surat) ? $pesan_surat : $pesan_tolak;
                    "<br>" ?>
                </div>
            </section>
        <?php } ?>

        <?php if ($flag_level == 3) { ?>
            <?php if ($id_status_surat != 4) {  // surat yang ditolak tidak bisa update status?>
            
                <?php if (empty($comment)) { ?>
                    <section class="panel">
                        <div class="panel-body">
                            <?php
                            $uri = $this->uri->segment(3);
                            echo form_open('C_inbox/upd_status/' . $uri, 'class="form-horizontal form-label-left" novalidate');
                            ?>
                            <!-- add variable to post -->
                            <input type="hidden" name="id" value="<?php echo $this->uri->segment(3); ?>">
                            <input type="hidden" name="id_surat" value="<?php echo $id_surat; ?>">
                            <!-- add variable to post -->
                            <div class="komentar" style="margin-top: 15px;">
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <textarea class="form-control" name="update_status" id="summernote"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <button type="submit" name="disposisi" class="btn btn-primary"> Update</button>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </section>
                <?php } ?>
            <?php } ?>
        <?php } ?>

        <?php if (empty($pesan_tolak)) { ?>
            <?php if ($flag_level != 3) { ?>
                <?php if($flag_subordinat != 1) { ?>
                <section class="panel">
                    <div class="panel-body">
                        <?php
                        $uri = $this->uri->segment(3);
                        echo form_open('C_inbox/disposisi/' . $uri, 'class="form-horizontal" novalidate');
                        ?>
                        <!-- add variable to post -->
                        <input type="hidden" name="id" value="<?php echo $this->uri->segment(3); ?>">
                        <input type="hidden" name="id_surat" value="<?php echo $id_surat; ?>">
                        <input type="hidden" name="id_divisi" value="<?php echo $id_divisi; ?>">
                        <input type="hidden" name="id_group" value="<?php echo $id_group; ?>">
                        <input type="hidden" name="id_pengirim" value="<?php echo $id_pengirim; ?>">
                        <input type="hidden" name="npp_pengirim" value="<?php echo $npp_pengirim; ?>">
                        <!-- add variable to post -->

                        <div class="disposisi" style="margin-top: 15px;">
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <?php echo form_dropdown('option_disposisi[]', $option_disposisi, '', 'class="form-control" required="required"'); ?>
                                </div>
                            </div>
                            <div id="disposisi_div">

                            </div>
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <a href="javascript:;" class="btn btn-info" id="generate_disposisi">
                                        <i class="fa fa-plus-square"></i> Tambah
                                    </a>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <textarea class="form-control" name="pesan_disposisi" id="summernote"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <button type="submit" name="disposisi" class="btn btn-primary"><i class="fa fa-level-down"></i> Disposisi</button>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </section>
                <?php } ?>
            <?php } ?>
        <?php  } ?>

        <?php if (!empty($comment)) { ?>
            <section class="panel">
                <div class="panel-heading">
                    Status
                </div>
                <div class="panel-body">
                    <?php echo $comment; ?>
                </div>
            </section>
        <?php } ?>

    </div>
</div>