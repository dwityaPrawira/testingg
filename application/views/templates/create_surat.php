<?php

defined('BASEPATH') or exit('No direct script access allowed');
?>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
        $flashmessage = $this->session->flashdata('message');
        if (!empty($flashmessage)) {
        ?>
            new PNotify({
                title: 'Error!',
                text: '<?php echo $flashmessage; ?>',
                type: 'error',
                styling: 'bootstrap3'
            });
        <?php
        }
        $flashmessage_scc = $this->session->flashdata('success_message');
        if (!empty($flashmessage_scc)) {
        ?>
            new PNotify({
                title: 'Success',
                text: '<?php echo $flashmessage_scc; ?>',
                type: 'success',
                styling: 'bootstrap3'
            });
        <?php
        }
        ?>
    })
</script>

<style>
    input[type=time]::-webkit-datetime-edit-ampm-field {
        display: none;
    }
</style>

<script type="text/javascript">
    $(function() {
        $(".datepicker").datepicker({
            dateFormat: 'yy-mm-dd',
            autoclose: true,
            todayHighlight: true,
        });
        $("#tgl_mulai").on('change', function(selected) {
            var startDate = new Date(selected.timeStamp);
            $("#tgl_akhir").datepicker("option", "minDate", startDate);
            console.log($('#tgl_akhir'));
            if ($("#tgl_mulai").val() > $("#tgl_akhir").val()) {
                $("#tgl_akhir").val($("#tgl_mulai").val());
            }
        });
    });
</script>

<style>
    .move-left {
        width: 15px;
        box-shadow: none;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><?php echo anchor('pages/index', 'Beranda'); ?></li>
            <li class="active">Buat Event</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <section class="panel">
                    <header class="panel-heading">
                        Form Buat Event
                    </header>
                    <div class="panel-body">
                        <div>
                            <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                            <?php
                            echo form_open_multipart('C_surat/kirim', 'class="form-horizontal form-label-left" novalidate');
                            ?>
                            <div class="form-group" style="display: none;">
                                <label for="id_group" class="col-lg-2 col-sm-2 control-label"><b>Tujuan Event </b></label>
                                <div class="col-lg-10">
                                    <?php echo form_dropdown('div_id[]', $ddown_tujuan, '', 'class="form-control"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2"><b>Nama Kegiatan </b></label>
                                <div class="col-sm-8">
                                    <input type="text" name="subject" class="form-control" id="subject" placeholder="Nama Kegiatan" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cc-surat" class="col-sm-2"><b>Bentuk Dukungan</b></label>
                                <div class="col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="check_list[]" alt="Checkbox" value="Dokumentasi"> Dokumentasi
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="check_list[]" alt="Checkbox" value="Anggaran"> Anggaran
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="check_list[]" alt="Checkbox" value="Koordinasi Instansi"> Koordinasi Instansi
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="check_list[]" alt="Checkbox" value="Lain lain"> Lain lain
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cc-surat" class="col-sm-2"><b>Tanggal Awal</b> </label>
                                <div class="col-lg-8">
                                    <input id="tgl_mulai" autocomplete="off" placeholder="masukkan tanggal Awal" type="text" class="form-control datepicker" name="tgl_awal">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cc-surat" class="col-sm-2"><b>Tanggal Akhir</b></label>
                                <div class="col-lg-8">
                                    <input id="tgl_akhir" autocomplete="off" placeholder="masukkan tanggal Akhir" type="text" class="form-control datepicker" name="tgl_akhir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cc-surat" class="col-sm-2 "><b>Waktu Pelaksanaan</b></label>
                                <div class="col-lg-8">
                                    <input type="text" id="timepicker-24-hr" name="waktuawal" class="form-control timepicker-24-hr">
                                    &nbsp;<b>Sampai dengan :</b>
                                    <input type="text" id="timepicker-24-hr" name="waktuakhir" class="form-control timepicker-24-hr">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="hal" class="col-sm-2"><b>Notes</b></label>
                                <div class="col-lg-8">
                                    <textarea class="form-control" name="content" id="summernote"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="text-danger">* ekstensi file upload hanya diperbolehkan .doc, .docx, .pdf, .xls, .xlsx, .jpeg, .png, .heic  dan sebesar 5 MB</label>
                                <label for="upload" class="col-sm-2"><b>Upload</b></label>
                                <div class="col-lg-8">
                                    <input type="file" name="userfile[]" size="20" id="file" />
                                    <div id="new"></div>
                                    <div style="padding-top: 5px;">
                                        <a href="javascript:;" class="btn btn-primary" id="generateupload">
                                            <i class="fa fa-plus-square"></i> Tambah
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                            <?php
                            echo form_close();
                            ?>
                        </div>
                    </div>
                </section>
                <!--progress bar end-->
            </div>
        </div>
    </div>
</div>
<!-- <div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Buat Event</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <br />
                <?php
                echo form_open_multipart('C_surat/kirim', 'class="form-horizontal form-label-left" novalidate');
                ?>
                
                <div class="form-group">
                    <label for="id_group" class="control-label col-md-3 col-sm-3 col-xs-12">Tujuan Event</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php echo form_dropdown('div_id[]', $ddown_tujuan, '', 'class="form-control"'); ?>
                    </div>
                </div>
                <div class="item form-group">
                    <label for="cc-surat" class="control-label col-md-3 col-sm-3 col-xs-12">Nama Kegiatan  <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="subject" class="form-control col-md-7 col-xs-12" placeholder="Nama Kegiatan" required="required">
                    </div>
                </div>
                <div class="item form-group">
                    <label for="cc-surat" class="control-label col-md-3 col-sm-3 col-xs-12">Bentuk Dukungan  <span class="required">*</span></label>
                    <div class="col-sm-3">
                        <div class="form-check">
                            <input class="form-check-input move-left" type="checkbox" name="check_list[]" alt="Checkbox" value="Dokumentasi"> Dokumentasi
                            <br>
                            <input class="form-check-input move-left" type="checkbox" name="check_list[]" alt="Checkbox" value="Anggaran"> Anggaran
                            <br>
                            <input class="form-check-input move-left" type="checkbox" name="check_list[]" alt="Checkbox" value="Koordinasi Instansi"> Koordinasi Instansi
                            <br>
                            <input class="form-check-input move-left" type="checkbox" name="check_list[]" alt="Checkbox" value="Lain lain"> Lain lain
                        </div>
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Tgl Awal</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="tgl_mulai" autocomplete="off" placeholder="masukkan tanggal Awal" type="text" class="form-control datepicker" name="tgl_awal">
                    </div>
                </div>
                <div class="item form-group">
                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Tgl Akhir</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="tgl_akhir" autocomplete="off" placeholder="masukkan tanggal Akhir" type="text" class="form-control datepicker" name="tgl_akhir">
                    </div>
                </div>
                <div class="item form-group">
                    <label for="appt" class="control-label col-md-3 col-sm-3 col-xs-12">Waktu Pelaksanaan</label>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <input type="text" id="timepicker-24-hr" name="waktuawal" class="form-control timepicker-24-hr">
                        &nbsp;<b>Sampai dengan :</b>
                        <input type="text" id="timepicker-24-hr" name="waktuakhir" class="form-control timepicker-24-hr">
                    </div>
                </div>
                <div class="form-group">
                    <label for="hal" class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <textarea class="form-control" name="content" ></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="upload" class="control-label col-md-3 col-sm-3 col-xs-12">Upload</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="file" name="userfile[]" size="20" id="file"/>
                        <div id="new"></div>
                        <div style="padding-top: 5px;">
                            <a href="javascript:;" class="btn btn-primary" id="generateupload">
                                <i class="fa fa-plus-square"></i> Tambah
                            </a>
                        </div>
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="reset" class="btn btn-danger">Reset</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
                <?php
                echo form_close();
                ?>
            </div>
        </div>
    </div>
</div>  -->
<script>
    var twelveHour = $('.timepicker-12-hr').wickedpicker();
    $('.time').text('//JS Console: ' + twelveHour.wickedpicker('time'));
    $('.timepicker-24-hr').wickedpicker({
        twentyFour: true
    });
    $('.timepicker-12-hr-clearable').wickedpicker({
        clearable: true
    });
</script>