<?php
/**
 * Created by PhpStorm.
 * User: danympradana
 * Date: 8/1/17
 * Time: 9:21 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="page-title">
    <div class="title_left">
        <h3><?php echo anchor('pages/index', 'Beranda');?> / Sudah Approve</h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Detail Surat</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped">
                    <tr>
                        <td>Klasifikasi</td>
                        <td> : <?php echo strtoupper($tipe);?></td>
                    </tr>
                    <tr>
                        <td>Nomor Surat</td>
                        <td>: <?php echo $no_surat;?></td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td> : <?php echo format_datetime($tgl_create);?></td>
                    </tr>

                    <tr>
                        <td valign="top">Kepada</td>
                        <td><?php echo $table_get_to;?></td>
                    </tr>
                    <tr>
                        <td valign="top">Cc</td>
                        <td><?php echo $table_get_cc;?></td>
                    </tr>
                    <tr>
                        <td>Dari</td>
                        <td> : <?php echo $from;?></td>
                    </tr>
                    <tr>
                        <td>Hal</td>
                        <td> : <?php echo $subyek_surat;?></td>
                    </tr>

                    <?php if ($attach != null ) { ?>
                        <tr>
                            <td>Lampiran</td>
                            <td><?php echo $attach; ?></td>
                        </tr>
                    <?php } ?>
                </table>
                <hr />
                <div>
                    <?php echo $content_surat;?>
                </div>
            </div>
        </div>
    </div>
</div>