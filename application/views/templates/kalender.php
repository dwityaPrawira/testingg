<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style>
    .without_ampm::-webkit-datetime-edit-ampm-field {
       display: none;
     }
     input[type=time]::-webkit-clear-button {
       -webkit-appearance: none;
       -moz-appearance: none;
       -o-appearance: none;
       -ms-appearance:none;
       appearance: none;
       margin: -10px; 
     }
</style>

<script type="text/javascript">
    $(function(){
         $(".datepicker").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
        });
        $("#tgl_mulai").on('changeDate', function(selected) {
            var startDate = new Date(selected.date.valueOf());
            $("#tgl_akhir").datepicker('setStartDate', startDate);
            if($("#tgl_mulai").val() > $("#tgl_akhir").val()){
              $("#tgl_akhir").val($("#tgl_mulai").val());
            }
        });
     });
</script>
<div class="page-title">
    <div class="title_left">
        <h3><?php echo anchor('pages/index', 'Beranda');?> / Kalender Kegiatan</h3>
    </div>
</div>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Filter Event</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <?php echo form_open_multipart('C_kalender/filter', 'class="form-horizontal form-label-left" novalidate');?>

                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Tgl Awal</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="tglawal" autocomplete="off" placeholder="masukkan tanggal Awal" type="text" class="form-control datepicker" name="tglawal">
                    </div>
                </div>
                <div class="item form-group">
                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Tgl Akhir</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="tglakhir" autocomplete="off" placeholder="masukkan tanggal Akhir" type="text" class="form-control datepicker" name="tglakhir">
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Filter</button>
                    </div>
                </div>
                <?php echo form_close();?>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <?php
                echo $table;
                echo $pagination;
                ?>
            </div>
        </div>
    </div>
</div>