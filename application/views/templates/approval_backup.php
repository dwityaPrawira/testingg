<div class="row">
<div style="margin-top: 10px;">
    <ol class="breadcrumb">
      <li><?php echo anchor('pages/index', 'Beranda');?></li>
      <li class="active">Approval</li>
    </ol>
    </div>
    <hr>
<div >
<?php 
$flashmessage = $this->session->flashdata ( 'msg_approve' );
echo ! empty ( $flashmessage ) ? '<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert">&times;</button><p class="message">' . $flashmessage . '</p></div>' : '';
echo $table; ?>
</div>
</div>