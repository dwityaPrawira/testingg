<script type="text/javascript">
    $(document).ready(function(){
        <?php
            $flashmessage = $this->session->flashdata ( 'message' );
            if(!empty($flashmessage)){
        ?>
        new PNotify({
            title: 'Error!',
            text: '<?php echo $flashmessage;?>',
            type: 'error',
            styling: 'bootstrap3'
        });
        <?php
            }
            $flashmessage_scc = $this->session->flashdata ( 'success_message' );
            if(!empty($flashmessage_scc)){
        ?>
        new PNotify({
            title: 'Success',
            text: '<?php echo $flashmessage_scc;?>',
            type: 'success',
            styling: 'bootstrap3'
        });
        <?php
            }
        ?>
    })
</script>
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><?php echo anchor('pages/index', 'Beranda');?></li>
            <li class="active">Ubah Password</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="row">            
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Form Ubah Foto Profile
                    </header>
                    <div class="panel-body">
                        <div class="position-center">
                        <?php echo form_open_multipart('C_updatepassword/update_profile', 'class="form-horizontal form-label-left" novalidate');?>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Foto <span class="required"></span></label>
                            <?php
                                $CI = & get_instance();
                                $image_properties = array(
                                    'src'   => 'assets/photos/'.$CI->nativesession->get('photo'),
                                    'alt' => '...',
                                    'class' => 'img-circle profile_img'
                                );
                            ?>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <?php echo img($image_properties);?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="upload" class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="file" name="userfile[]" size="20" id="file"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <input type="hidden" name="id_employee" value="<?php echo $id_employee;?>" />
                                <input type="hidden" name="npp" value="<?php echo $npp;?>" />
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama <span class="required"></span></label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <label class="form-control col-md-7 col-xs-12"><?php echo $nama_user;?><span class="required"></span></label>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Group <span class="required"></span></label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <label class="form-control col-md-7 col-xs-12"><?php echo $group_name;?><span class="required"></span></label>
                            </div>
                        </div>
                        <div class="ln_solid"></div>

                        <?php echo form_close();?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">            
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        Form Ubah Password
                    </header>
                    <div class="panel-body">
                        <div class="position-center">
                        <?php
                            echo form_open('C_updatepassword/save', 'class="form-horizontal form-label-left" novalidate');
                        ?>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Password Lama <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input type="password" name="password_lama" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Password Baru <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input type="password" name="password_baru" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Konfirmasi Password <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input type="password" name="konfirmasi_password" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <input type="hidden" name="user_email" value="<?php echo $info['user']?>" />
                                <button type="reset" class="btn btn-danger">Batal</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                        <?php
                            echo form_close();
                        ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>