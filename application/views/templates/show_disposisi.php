<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><?php echo anchor('pages/index', 'Beranda'); ?></li>
            <li><?php echo anchor('c_disposisi', 'Disposisi'); ?></li>
            <li class="active">Detail Disposisi Event</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                Detail Arsip
            </header>
            <div class="panel-body">
                <table class="table table-striped borderless">
                    <?php $nota_id = $this->uri->segment(3); ?>
                    <tr>
                        <td style="width: 15%;">Klasifikasi</td>
                        <td style="width: 5%;">:</td>
                        <td>&nbsp;&nbsp;<?php echo strtoupper($tipe); ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo $tgl_create; ?></td>
                    </tr>
                    <tr>
                        <td valign="top">Kepada</td>
                        <td>:</td>
                        <td><?php echo $table_get_to; ?></td>
                    </tr>
                    <tr>
                        <td valign="top">Diteruskan Kepada</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo $biro; ?></td>
                    </tr>
                    <tr>
                        <td valign="top">Dukungan</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo $dukungan; ?></td>
                    </tr>
                    <tr>
                        <td>Hal</td>
                        <td>:</td>
                        <td>&nbsp;&nbsp;<?php echo $subyek_surat; ?></td>
                    </tr>

                    <?php if ($attach != null) { ?>
                        <tr>
                            <td>Lampiran</td>
                            <td>:</td>
                            <td>&nbsp;&nbsp;<?php echo $attach; ?></td>
                        </tr>
                    <?php } ?>
                </table>
                <hr />
                <div>
                    <?php echo $content_surat; ?>
                </div>
            </div>
        </section>

        <section class="panel">
            <div class="panel-heading">
                Notes Approval
            </div>
            <div class="panel-body">
                <?php echo $table_notes_approval; ?>
            </div>
        </section>

        <section class="panel">
            <div class="panel-heading">
                Monitor Disposisi
            </div>
            <div class="panel-body">
                <?php echo $tracking; ?>
            </div>
        </section>

        <section class="panel">
            <div class="panel-heading">
                Pesan Disposisi
            </div>
            <div class="panel-body">
                <ul class="messages">
                    <?php foreach ($m_tracking as $mt) : ?>
                        <li>
                            <div class="message_wrapper">
                                <h5 class="heading"><?php echo $mt->nama; ?></h5>
                                <p><i class="fa fa-calendar"></i> <?php echo format_datetime($mt->tgl_disposisi); ?></p>
                                <blockquote class="message" style="font-size: inherit;"><?php echo $mt->pesan; ?></blockquote>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </section>
    </div>
</div>