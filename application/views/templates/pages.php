<?php
function generate_panel($col, $id)
{
	$template = '<div class="' . $col . '">
						<section class="panel">
							<div class="panel-body">
								<div class="chartJS">
									<canvas id="' . $id . '" height="300" width:"800"></canvas>
								</div>
							</div>
						</section>
					</div>';
	return $template;
}
?>

<div class="row">
	<?php
	switch ($group_level) {
		case '2':
			echo generate_panel('col-sm-12', 'myChartUser');
			break;

		case '3':
			echo generate_panel('col-sm-12', 'myChartUser');
			break;

		case '4':
			echo generate_panel('col-sm-6', 'loginSuccess');
			echo generate_panel('col-sm-6', 'loginFail');
			break;

		default:
			echo generate_panel('col-md-8 col-sm-6', 'myChart');
			echo generate_panel('col-md-4 col-sm-6', 'myChart2');
			break;
	}
	?>
</div>

<div class="row">
	<div class="col-md-12">
		<section class="panel">
			<header class="panel-heading">
				Event
			</header>
			<div class="panel-body" style="overflow-x: scroll;">
				<?php echo $table; ?>
			</div>
		</section>
	</div>
</div>

<script type="text/javascript">
	var arrColor = [
		'#D1EED3', '#D8C3E0', '#F6D7C5', '#F7A8B2', '#BCE5E2',
		'#F1705F', '#F3BD6A', '#7DDBD3', '#CF92E1', '#D0B2F1'
 		]; 
	var myChart = document.getElementById('myChart');

	if (myChart != null) {
		var ctx = myChart.getContext('2d');
		var graph = <?php echo json_encode($graph); ?>;
		var dataGrap = [];

		if (graph.length > 0) {
			$.each(graph, function(index, value) {
				var hexDataset = arrColor[index];
				

				var item = {
					label: value.group_pengirim,
					data: [value.total],
					backgroundColor: hexDataset 
				};

				dataGrap.push(item)
			});
		}

		var chart = new Chart(ctx, {
			type: 'bar',
			options: {
				plugins: {
					legend: {
						position: 'bottom',
						align: 'start'
					},
				},
				maintainAspectRatio: false,
			},
			data: {
				labels: [
					"Jumlah Event"
				],
				datasets: dataGrap
			},
		});
	}
</script>

<script type="text/javascript">
	var myChart2 = document.getElementById('myChart2');

	if (myChart2 != null) {
		var ctx = document.getElementById('myChart2').getContext('2d');
		var chart = new Chart(ctx, {
			type: 'pie',
			data: {
				labels: [
					'Diajukan',
					'Ditolak',
					'Disetujui',
					'Sedang Berjalan'
				],
				datasets: [{
					label: 'My First Dataset',
					data: [<?php echo $diajukan; ?>, <?php echo $ditolak; ?>, <?php echo $disetujui; ?>, <?php echo $berjalan; ?>],
					backgroundColor: [
						'rgb(255, 205, 86)',
						'rgb(255, 99, 132)',
						'rgb(107,142,35)',
						'rgb(54, 162, 235)'
					],
					hoverOffset: 4
				}]
			},
			options: {
				maintainAspectRatio: false
			}
		});
	}
</script>

<script type="text/javascript">
	var loginSuccess = document.getElementById('loginSuccess');

	if (loginSuccess != null) {
		var ctx = loginSuccess.getContext('2d');
		var chart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: [
					<?php
					if (count($graph_success) > 0) {
						foreach ($graph_success as $data) {
							echo "'" . $data->user_email . "',";
						}
					}
					?>
				],
				datasets: [{
					label: 'Login Berhasil',
					backgroundColor: [
						'rgb(255, 99, 132)',
						'rgb(54, 162, 235)',
						'rgb(255, 205, 86)'
					],
					borderColor: '##93C3D2',
					data: [
						<?php
						if (count($graph_success) > 0) {
							foreach ($graph_success as $data) {
								echo $data->success . ", ";
							}
						}
						?>
					]
				}]
			},
			options: {
				maintainAspectRatio: false
			}
		});
	}
</script>

<script type="text/javascript">
	var loginFail = document.getElementById('loginFail');

	if (loginFail != null) {
		var ctx = document.getElementById('loginFail').getContext('2d');
		var chart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: [
					<?php
					if (count($graph_fail) > 0) {
						foreach ($graph_fail as $data) {
							echo "'" . $data->user_email . "',";
						}
					}
					?>
				],
				datasets: [{
					label: 'Login Gagal',
					backgroundColor: [
						'rgb(255, 99, 132)',
						'rgb(54, 162, 235)',
						'rgb(255, 205, 86)'
					],
					borderColor: '##93C3D2',
					data: [
						<?php
						if (count($graph_fail) > 0) {
							foreach ($graph_fail as $data) {
								echo $data->fail . ", ";
							}
						}
						?>
					]
				}]
			},
			options: {
				maintainAspectRatio: false
			}
		});
	}
</script>

<script type="text/javascript">
	var myChartUser = document.getElementById('myChartUser');

	if (myChartUser != null) {
		var ctx = document.getElementById('myChartUser').getContext('2d');
		var chart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: [
					'Diajukan',
					'Ditolak',
					'Disetujui',
					'Sedang Berjalan'
				],
				datasets: [{
					label: 'Event',
					data: [<?php echo $diajukan; ?>, <?php echo $ditolak; ?>, <?php echo $disetujui; ?>, <?php echo $berjalan; ?>],
					backgroundColor: [
						'rgb(255, 99, 132)',
						'rgb(54, 162, 235)',
						'rgb(255, 205, 86)',
						'rgb(107,142,35)'
					],
					hoverOffset: 4
				}]
			},
			options: {
				maintainAspectRatio: false
			}
		});
	}
</script>