<?php
/**
 * Created by PhpStorm.
 * User: danympradana
 * Date: 8/1/17
 * Time: 7:38 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
    $(document).ready(function(){
        <?php
            $flashmessage = $this->session->flashdata ( 'msg_blm_approve' );
            if(!empty($flashmessage)){
        ?>
        new PNotify({
            title: 'Maaf !',
            text: '<?php echo $flashmessage;?>',
            type: 'warning',
            styling: 'bootstrap3'
        });
        <?php
            }
        ?>
    })
</script>
<div class="page-title">
    <div class="title_left">
        <h3><?php echo anchor('pages/index', 'Beranda');?> / Sudah Approve</h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <?php
                echo $table;
                echo $pagination;
                ?>
            </div>
        </div>
    </div>
</div>