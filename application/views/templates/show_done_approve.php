<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<style>
.tujuan {
  /* border: 1px solid red;  */
  margin-left: -9px;
}
</style>

<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><?php echo anchor('pages/index', 'Beranda'); ?></li>
            <li><?php echo anchor('c_approval/done', 'Sudah Approve'); ?></li>
            <li class="active">Detail Sudah Approve</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                Detail Sudah Approve
            </header>
            <div class="panel-body">
                <table cellpadding="50" border="0" class="table borderless table-striped">
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td><?php echo format_datetime($tgl_create); ?></td>
                    </tr>
                    <tr>
                        <td valign="top">Tujuan</td>
                        <td>:</td>
                        <td><div class="tujuan"><?php echo $table_get_to; ?></div></td>
                    </tr>
                    <tr>
                        <td>Nama Kegiatan</td>
                        <td>:</td>
                        <td><?php echo $subyek_surat; ?></td>
                    </tr>
                    <tr>
                        <td>Diteruskan Kepada</td>
                        <td>:</td>
                        <td><?php echo $biro; ?></td>
                    </tr>
                    <tr>
                        <td>Dukungan</td>
                        <td>:</td>
                        <td><?php echo $dukungan; ?></td>
                    </tr>

                    <tr>
                        <td>Tanggal Pelaksanaan</td>
                        <td>:</td>
                        <td><?php echo $tglawal; ?> - <?php echo $tglakhir; ?></td>
                    </tr>

                    <tr>
                        <td>Waktu Pelaksanaan</td>
                        <td>:</td>
                        <td><?php echo $waktuawal; ?> - <?php echo $waktuakhir; ?></td>
                    </tr>

                    <?php if ($attach != null) { ?>
                        <tr>
                            <td>Lampiran</td>
                            <td>:</td>
                            <td><?php echo $attach; ?></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </section>

        <section class="panel">
            <div class="panel-heading">
                Notes
            </div>
            <div class="panel-body">
                <?php echo $content_surat; ?>
            </div>
        </section>

        <section class="panel">
            <div class="panel-heading">
                Notes Approval
            </div>
            <div class="panel-body">
                <?php echo $table_notes_approval;?>
            </div>
        </section>
    </div>
</div>