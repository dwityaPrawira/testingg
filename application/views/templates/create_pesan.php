<script type="text/javascript">
    $(document).ready(function() {
        <?php
        $flashmessage = $this->session->flashdata('message');
        if (!empty($flashmessage)) {
        ?>
            new PNotify({
                title: 'Error!',
                text: '<?php echo $flashmessage; ?>',
                type: 'error',
                styling: 'bootstrap3'
            });
        <?php
        }
        $flashmessage_scc = $this->session->flashdata('success_message');
        if (!empty($flashmessage_scc)) {
        ?>
            new PNotify({
                title: 'Success',
                text: '<?php echo $flashmessage_scc; ?>',
                type: 'success',
                styling: 'bootstrap3'
            });
        <?php
        }
        ?>
    })
</script>
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><?php echo anchor('pages/index', 'Beranda'); ?></li>
            <li class="active">Kirim Pesan</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                Form Kirim Pesan
            </header>
            <div class="panel-body">
                <div class="position-center">
                    <?php
                    echo form_open_multipart('C_pesan/kirim', 'class="form-horizontal form-label-left" novalidate');
                    ?>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tujuan-surat"><b>Tujuan Surat <span class="required">*</span></b></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="div_name" class="form-control autocomplete col-md-7 col-xs-12" placeholder="Tujuan Surat">
                            <input type="hidden" name="div_id[]" id="div_id" class="divid" required="required">
                        </div>
                    </div>
                    <div id="tujuan-div"></div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <a href="javascript:;" class="btn btn-primary" id="generate">
                                <i class="fa fa-plus-square"></i> Tambah
                            </a>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label for="cc-surat" class="control-label col-md-3 col-sm-3 col-xs-12"><b>Subyek <span class="required">*</span></b></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="subject" class="form-control col-md-7 col-xs-12" placeholder="Nama Kegiatan" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="hal" class="control-label col-md-3 col-sm-3 col-xs-12"><b>Isi *</b></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <textarea class="form-control" name="content" id="summernote"></textarea>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="reset" class="btn btn-danger">Batal</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                    <?php
                    echo form_close();
                    ?>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#summernote").summernote({
            height: 200,
            toolbar: [
                ['style', ['bold', 'italic', 'underline']]
            ]
        });
    });
</script>