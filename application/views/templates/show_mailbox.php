<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.disposisi').hide().before('<a href="#" id="toggle-example3" class="btn btn-primary" style="margin-left: 10px;"><i class="fa fa-level-down"></i> Disposisi Surat</a>');
        $('a#toggle-example3').click(function() {
            $('.disposisi').slideToggle(1000);
            return false;
        });
        $("#generate").click(function() {
            $clone = $("#source").clone();
            $clone.appendTo("#target");
        });
        $(document).ready(function() {
            $("#summernote").summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline']]
                ],
                height: 200,
                placeholder: 'Tulis pesan disposisi...'
            });
        });

        $("form").on("click", "#generate_disposisi", generate_disposisi);
        $("form").on("click", ".hapus-disposisi", function() {
            $(this).parent('span').parent('div').parent('div').parent('div').remove();
        })
    });
    var generate_disposisi = function() {
        var clone = '<div class="form-group"><div class="col-md-4 col-sm-4 col-xs-12">';
        clone += '<div class="input-group"><select name="option_disposisi[]" class="form-control">';
        <?php
        foreach ($option_disposisi as $key => $row) {
        ?>
            clone += '<option value="<?php echo $key; ?>"><?php echo $row; ?></option>';
        <?php
        }
        ?>
        clone += '</select><span class="input-group-btn"><button class="btn btn-default hapus-disposisi" type="button"><i class="fa fa-trash-o"></i></button></span></div></div></div>';
        $("#disposisi_div").append(clone);
    }
</script>
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><?php echo anchor('pages/index', 'Beranda'); ?></li>
            <li><?php echo anchor('c_inbox', 'Inbox'); ?></li>
            <li class="active">Detail Mailbox</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <section class="panel">
            <header class="panel-heading">
                Detail Arsip
            </header>
            <div class="panel-body">
                <table cellpadding="50" border="0" class="table borderless table-striped">
                    <?php $nota_id = $this->uri->segment(3); ?>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td><?php echo format_datetime($tgl_create); ?></td>
                    </tr>
                    <tr>
                        <td>Dari</td>
                        <td>:</td>
                        <td><?php echo $from; ?></td>
                    </tr>
                    <tr>
                        <td>Subyek Pesan</td>
                        <td>:</td>
                        <td><?php echo $subyek_surat; ?></td>
                    </tr>
                </table>
            </div>
        </section>
        <section class="panel">
            <div class="panel-heading">
                Notes
            </div>
            <div class="panel-body">
                <?php echo $content_surat; ?>
            </div>
        </section>
    </div>
</div>