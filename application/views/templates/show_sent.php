<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<style>
.tujuan {
  /* border: 1px solid red;  */
  margin-left: -10px;
}
</style>

<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><?php echo anchor('c_sentitem', 'Sent Item');?></li>
            <li class="active">Detail Event</li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                    Detail Event
                    </header>
                    <div class="panel-body">
                        
                    <table class="table borderless table-striped">
                            <?php $nota_id = $this->uri->segment(3);?>
                            <tr>
                                <td>Tanggal</td>
                                <td>:</td>
                                <td><?php echo format_datetime($tgl_create);?></td>
                            </tr>

                            <tr>
                                <td valign="top">Tujuan</td>
                                <td>:</td>
                                <td><div class="tujuan"><?php echo $table_get_to;?></div></td>
                            </tr>
                            <tr>
                                <td>Nama Kegiatan</td>
                                <td>:</td>
                                <td><?php echo $subyek_surat;?></td>
                            </tr>

                            <tr>
                                <td>Diteruskan Kepada</td>
                                <td>:</td>
                                <td><?php echo $biro;?></td>
                            </tr>

                            <tr>
                                <td>Dukungan</td>
                                <td>:</td>
                                <td><?php echo $dukungan;?></td>
                            </tr>

                            <tr>
                                <td>Tanggal Pelaksanaan</td>
                                <td>:</td>
                                <td><?php echo $tglawal;?> - <?php echo $tglakhir;?></td>
                            </tr>

                            <tr>
                                <td>Waktu Pelaksanaan</td>
                                <td>:</td>
                                <td><?php echo $waktuawal;?> - <?php echo $waktuakhir;?></td>
                            </tr>

                            <?php if ($attach != null ) { ?>
                                <tr>
                                    <td>Lampiran</td>
                                    <td>:</td>
                                    <td><?php echo $attach; ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td>Status</td>
                                <td>:</td>
                                <td><?php echo $status_surat;?></td>
                            </tr>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                    Notes
                    </header>
                    <div class="panel-body">
                        <?php echo $content_surat;?>
                    </div>
                </section>

                <section class="panel">
                    <div class="panel-heading">
                        Notes Approval
                    </div>
                    <div class="panel-body">
                        <?php echo $table_notes_approval; ?>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
