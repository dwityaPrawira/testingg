<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<style>
    .dataTables_wrapper {
        width: <?php echo ($table_width != '') ? $table_width : '100%'; ?>;
    }

    .modal {
        /* new custom width */
        width: 750px;
        /* must be half of the width, minus scrollbar on the left (30px) */
        margin-left: -375px;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><?php echo anchor('pages/index', 'Beranda'); ?></li>
            <li class="active"><?php echo $title; ?></li>
        </ul>
        <!--breadcrumbs end -->
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        List <?php echo $title; ?>
                    </header>
                    <div class="panel-body" style="overflow-x: scroll;">
                        <?php
                        if (isset($add_link) && $add_link != '') {
                            echo "<div class='x_title'><ul class='nav navbar-right panel_toolbox'>";
                            echo "<li>" . anchor($add_link, '<i class="fa fa-plus"></i> Tambah Data', 'style="color: #73879C;" title="Tambah Data"') . "</li>";
                            echo "</ul><div class='clearfix'></div></div>";
                        }
                        ?>
                        <table class="table table-striped table-bordered table-hover" id="table-datatable">
                            <thead>
                                <tr>
                                    <?php
                                    foreach ($table_header as $idx => $data) {
                                        echo "<th>$data</th>";
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <?php
                            if (count($filter) > 0) {
                            ?>
                                <tfoot>
                                    <tr>
                                        <?php
                                        foreach ($table_header as $idx => $data) {
                                            echo "<th></th>";
                                        }
                                        ?>
                                    </tr>
                                </tfoot>
                            <?php
                            }
                            ?>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
        $flashmessage = $this->session->flashdata('msg_inbox');
        $flashmessage_success = $this->session->flashdata('msg_success');
        if (!empty($flashmessage)) {
        ?>
            new PNotify({
                title: 'Maaf !',
                text: '<?php echo $flashmessage; ?>',
                type: 'warning',
                styling: 'bootstrap3'
            });
        <?php
        }
        ?>

        <?php
        if (!empty($flashmessage_success)) {
        ?>
            new PNotify({
                title: 'Success',
                text: '<?php echo $flashmessage_success; ?>',
                type: 'success',
                styling: 'bootstrap3'
            });
        <?php
        }
        ?>

        var table = $('#table-datatable').DataTable({
            'dom': '<l<t>ip>',
            'bSort': false,
            'bProcessing': true,
            'bServerSide': true,
            'bInfo' : false,
            'lengthChange' : false,
            'ajax': "<?php echo base_url() . 'index.php/' . $ajax_target; ?>"
        });
        <?php
        if (count($filter) > 0) {
        ?>
            yadcf.init(table, [
                <?php
                $string = '';
                foreach ($filter as $fil) {
                    $string .= '{';
                    foreach ($fil as $idx => $f) {
                        $string .= $idx . ': ' . $f . ',';
                    }
                    $string .= '},';
                }
                echo $string;
                ?>
            ], 'footer');
        <?php
        }
        ?>
    })
</script>