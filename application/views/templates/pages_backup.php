<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Pelaksanaan Event'],
  ['Diajukan', <?php echo $diajukan;?>],
  ['Ditolak', <?php echo $ditolak;?>],
  ['Disetujui', <?php echo $disetujui;?>],
  ['Sedang Berjalan', <?php echo $berjalan;?>]  
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Informasi Event', 'width':550, 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}
</script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Biro Humas Permintaan Terbanyak'],
  ['Biro Keuangan', <?php echo $keuangan;?>],
  ['Biro Sumber Daya Manusia', <?php echo $sdm;?>],
  ['Biro Sekretariat Pimpinan', <?php echo $sekre;?>],
  ['Biro Umum', <?php echo $umum;?>]  
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Biro Humas Permintaan Terbanyak', 'width':550, 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart2'));
  chart.draw(data, options);
}
</script>

<div class="page-title">
    <div class="title_left">
        <h3>Aplikasi Pengatur Jadwal Acara Kegiatan</h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row top_tiles">
    <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="animated flipInY" id="piechart"></div>
        </div>
    </div>
    <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="animated flipInY" id="piechart2"></div>
        </div>
    </div>
</div>

<div class="row top_tiles">
    <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="tile-stats">
            <div class="x_title">
                <h2>Event</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div><?php echo $table;?></div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<?php if($group_level == 0 || $group_level == '2') {?>
<?php } else{ ?>

<div class="row top_tiles">
    <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-inbox"></i></div>
            <div class="count"><?php echo $inbox_count;?></div>
            <h3>Arsip Event</h3>
            <p><?php echo anchor('c_inbox/index', '<i class="fa fa-folder-open-o"></i> Lihat Detail', 'class="btn btn-default"');?></p>
        </div>
    </div>
    <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-pencil-square-o"></i></div>
            <div class="count"><?php echo $approval_count?></div>
            <h3>Approval Event</h3>
            <p><?php echo anchor('c_approval/index', '<i class="fa fa-folder-open-o"></i> Lihat Detail', 'class="btn btn-default"');?></p>
        </div>
    </div>
</div>

<?php }; ?>