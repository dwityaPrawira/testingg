<script type="text/javascript">
$(document).ready(function(){   
    $('.disposisi').hide().before('<a href="#" id="toggle-example3" class="btn btn-xs btn-danger">Disposisi Surat</a>');
    $('a#toggle-example3').click(function() {
        $('.disposisi').slideToggle(1000);
        return false;
    });
    $("#generate").click(function(){
        $clone = $("#source").clone();
        $clone.appendTo("#target");
    });
});
</script>
<br><br>
<?php if($flag_level != 1) {?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Pesan Disposisi</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <ul class="messages">
                    <li>       
                        <div >
                            <div style="font-size: inherit;"><?php echo !empty($pesan_surat) ? $pesan_surat : $pesan_tolak;"<br>"?></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<table cellpadding="50" border="0" class="table">

<?php $nota_id = $this->uri->segment(3);?>
    <tr>
    	<td>Klasifikasi</td>
       	<td> : <?php echo strtoupper($tipe);?></td>
    </tr>
    <tr>
    	<td>Nomor Surat</td>
        <td>: <?php echo $nomor_surat;?></td>
    </tr>
	<tr>
    	<td>Tanggal</td>
       	<td> : <?php echo $tgl_create;?></td>
    </tr>

    <tr>
    	<td valign="top">Kepada</td>
       	<td><?php echo $table_get_to;?></td>
    </tr>
    <tr>
    	<td valign="top">Cc</td>
       	<td><?php echo $table_get_cc;?></td>
    </tr>
    <tr>
    	<td>Dari</td>
       	<td> : <?php echo $from;?></td>
    </tr>
    <tr>
    	<td>Hal</td>
       	<td> : <?php echo $subyek_surat;?></td>
    </tr>

    <?php if ($attach != null ) { ?>
    <tr>
    	<td>Lampiran</td>
        <td><?php echo $attach; ?></td>
    </tr>
    <?php } ?> 
</table>
<hr>
<table width="100%" style="overflow: hidden;" border=1>
    <tr>
   		<td>
        <div style="width: 100%; overflow: hidden; padding: 20px;">
			<?php echo $content_surat;?>
        </div>
        </td>
	</tr>
</table>
<br>
<?php echo $tracking_surat;?>
<table style="padding-top:20px">
	<tr>
    	<td></td>
    </tr>
    <tr>
    	<td><?php //echo $nama_divisi;?></td>
    </tr>
    
    <tr>
    	<td><?php //echo $ttd_memo; ?></td>
    </tr>
    
    <tr>
    	<td><?php //echo "<u>".$nama_pemp."</u>";?></td>
    </tr>
    
    <tr>
    	<td><?php //echo $nama_jabatan;?></td>
    </tr>
</table>
<br />
<?php if(empty($pesan_tolak)){ ?>
	<?php if($flag_level != 3){ ?>
	<?php 
        $uri = $this->encrypt->decode(base64_decode($this->uri->segment(3)), 'Test@123');
        echo form_open('C_inbox/disposisi/'. $uri );
    ?>
	<!-- add variable to post -->
	<input type="hidden" name="id" value="<?php echo $this->uri->segment(3);?>">
	<input type="hidden" name="id_surat" value="<?php echo $id_surat;?>">
	<input type="hidden" name="id_divisi" value="<?php echo $id_divisi;?>">
	<input type="hidden" name="id_group" value="<?php echo $id_group;?>">
	<input type="hidden" name="id_pengirim" value="<?php echo $id_pengirim;?>">
	<input type="hidden" name="npp_pengirim" value="<?php echo $npp_pengirim;?>">
	<!-- add variable to post -->
	<div class="disposisi">
	    <br>
	    <div id="source">
	        <?php echo form_dropdown('option_disposisi[]', $option_disposisi);?>
	    </div>
	    <div id="target"></div>
	    <br><input name="button" type="button" class="btn btn-default btn-xs" id="generate" value="Add" />
	    <br>
	    <br>
	        <textarea class="form-control" rows="3" name="pesan_disposisi"></textarea>
	    <br>
	    <input type="submit" name="disposisi" class="btn btn-xs btn-danger" value="Disposisi">
	</div>
	<?php echo form_close();?>
	<?php } ?>
<?php  } ?>