<script type="text/javascript">
$(document).ready(function(){   
    $('.example2').hide().before('<a href="#" id="toggle-example2" class="btn btn-xs btn-danger">Tolak Surat</a>');
    $('.disposisi').hide().before('<a href="#" id="toggle-example3" class="btn btn-xs btn-primary">Disposisi Surat</a>');
    $('.kembali').hide().before('<a href="#" id="toggle-example5" class="btn btn-xs btn-primary">Kembalikan Surat</a>');
    $('.komentar_surat').hide().before('<a href="#" id="toggle-example4" class="btn btn-xs btn-primary">Komentar Surat</a>');
    $('a#toggle-example2').click(function() {
        $('.example2').slideToggle(1000);
        return false;
    });

    $('a#toggle-example3').click(function() {
        $('.disposisi').slideToggle(1000);
        return false;
    });

    $('a#toggle-example4').click(function() {
        $('.komentar_surat').slideToggle(1000);
        return false;
    });

    $('a#toggle-example5').click(function() {
        $('.kembali').slideToggle(1000);
        return false;
    });

    $("#generate").click(function(){
        $clone = $("#source").clone();
        $clone.appendTo("#target");
    });
    
});
</script>

<br><br>
<table cellpadding="50" border="0" class="table">
    <tr>
    	<td>Klasifikasi</td>
       	<td> : <?php echo strtoupper($tipe);?></td>
    </tr>
    <tr>
    	<td>Nomor Surat</td>
        <td>: <?php echo $nomor_surat;?></td>
    </tr>
	<tr>
    	<td>Tanggal</td>
       	<td> : <?php echo $tgl_create;?></td>
    </tr>

    <tr>
    	<td valign="top">Kepada</td>
       	<td><?php echo $table_get_to;?></td>
    </tr>
    <tr>
    	<td valign="top">Cc</td>
       	<td><?php echo $table_get_cc;?></td>
    </tr>
    <tr>
    	<td>Dari</td>
       	<td> : <?php echo $from;?></td>
    </tr>
    <tr>
    	<td>Hal</td>
       	<td> : <?php echo $subyek_surat;?></td>
    </tr>

    <?php if ($attach != null ) { ?>
    <tr>
    	<td>Lampiran</td>
        <td><?php echo $attach; ?></td>
    </tr>
    <?php } ?> 
</table>
<hr>
<table width="100%" style="overflow: hidden;" border=1>
    <tr>
   		<td>
        <div style="width: 100%; overflow: hidden; padding: 20px;">
			<?php echo $content_surat;?>
        </div>
        </td>
	</tr>
</table>
<table style="padding-top:20px">
	<tr>
    	<td></td>
    </tr>
    <tr>
    	<td><?php //echo $nama_divisi;?></td>
    </tr>
    
    <tr>
    	<td><?php //echo $ttd_memo; ?></td>
    </tr>
    
    <tr>
    	<td><?php //echo "<u>".$nama_pemp."</u>";?></td>
    </tr>
    
    <tr>
    	<td><?php //echo $nama_jabatan;?></td>
    </tr>
</table>
<br />
<?php 
echo form_open('c_approval/approve/'.$this->uri->segment(3).'/'.$id_surat);?>
<input type="hidden" name="id_surat" value="<?php echo $id_surat;?>">
<input type="submit" name="submit_val" class="btn btn-xs btn-primary" value="Kirim">
<?php echo form_close();?>

<?php echo form_open('c_approval/tolak/'. $this->uri->segment(3).'/'.$id_surat);?>
<div class="example2"><br>
<textarea class="form-control" rows="3" name="pesan_tolak"></textarea><br>
<input type="submit" name="submit_val" class="btn btn-xs btn-danger" value="Tolak">
</div>
<?php echo form_close();?>
