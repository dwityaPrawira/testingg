<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Aplikasi Event | Login Page</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url('assets/gentelella/vendors/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url('assets/gentelella/vendors/nprogress/nprogress.css');?>" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo base_url('assets/gentelella/vendors/animate.css/animate.min.css');?>" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url('assets/gentelella/build/css/custom.min.css');?>" rel="stylesheet">
    <!-- PNotify -->
    <link href="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.buttons.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.nonblock.css');?>" rel="stylesheet">
</head>

<body class="login">
<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <?php
                    $attributes = array("autocomplete" => "off"); // prevent save to browser cache
                    echo form_open('login/validate', $attributes);
                ?>

                <?php
                    $CI = & get_instance();
                    $image_properties = array(
                        'src'   => 'assets/img/logo.png',
                        'alt' => '...',
                        'maintain_ratio' => 'TRUE',
                        'width'         => '320',
                        'height'       => '80',
                    );
                ?>
                    <h1>Login Form</h1>
                    <div>
                    <?php
                        echo img($image_properties);
                    ?>
                    </div>
                    <div>
                        <input type="text" name="username" id="username" class="form-control" placeholder="Username" required="" />
                    </div>
                    <div>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="" />
                    </div>
                    <div>
                        <input type="submit" class="btn btn-default submit" value="Login" style="margin: 10px 15px 0 0; float: none;"/>
                        <!--<a class="reset_pass" href="#">Lost your password?</a>-->
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <!--<p class="change_link">New to site?
                            <a href="#signup" class="to_register"> Create Account </a>
                        </p>-->

                        <div class="clearfix"></div>
                        <br />

                        <div>
                            <h1><i class="fa fa-recycle"></i> <span>Event Management</span></a></h1>
                            <p>&copy; 2021 All Rights Reserved.</p>
                        </div>
                    </div>
                <?php echo form_close(); ?>
            </section>
        </div>

        <div id="register" class="animate form registration_form">
            <section class="login_content">
                <form>
                    <h1>Create Account</h1>
                    <div>
                        <input type="text" class="form-control" placeholder="Username" required="" />
                    </div>
                    <div>
                        <input type="email" class="form-control" placeholder="Email" required="" />
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Password" required="" />
                    </div>
                    <div>
                        <a class="btn btn-default submit" href="index.html">Submit</a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <p class="change_link">Already a member ?
                            <a href="#signin" class="to_register"> Log in </a>
                        </p>

                        <div class="clearfix"></div>
                        <br />

                        <div>
                            <h1><i class="fa fa-paw"></i> Gentelella Alela!</h1>
                            <p>©2021 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
<!-- jQuery -->
<script src="<?php echo base_url('assets/gentelella/vendors/jquery/dist/jquery.min.js');?>"></script>
<!-- PNotify -->
<script src="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.js');?>"></script>
<script src="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.buttons.js');?>"></script>
<script src="<?php echo base_url('assets/gentelella/vendors/pnotify/dist/pnotify.nonblock.js');?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        <?php
            $flashmessage = $this->session->flashdata ( 'msg_login' );
            if(!empty($flashmessage)){
        ?>
        new PNotify({
            title: 'Maaf !',
            text: '<?php echo $flashmessage;?>',
            type: 'error',
            styling: 'bootstrap3'
        });
        <?php
            }
        ?>
    })
</script>
</body>
</html>
