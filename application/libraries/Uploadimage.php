<?php
class Uploadimage {	
	
	var $config;
	public function __construct(){
		// get month
		$bulan_attach = date('m');
		$this->ci =& get_instance();
		$this->config = array(
			'upload_path'     => dirname($_SERVER["SCRIPT_FILENAME"])."/photos/",
			'upload_url'      => base_url()."files/",
			// 'allowed_types'   => "gif|jpg|png|jpeg|pdf|doc|xml|docx|xls|xlsx|ppt|pptx|ods|odt|txt|zip|rar|txt",
			'allowed_types'   => "gif|jpg|png|jpeg",
			'overwrite'       => TRUE
		);
	}
	
	public function do_upload($key){
		$this->ci->load->library('upload', $this->config);
		$this->ci->load->library('session');
		$this->ci->load->helper('array');

		$cpt = count($key);
		$files = $_FILES;
		$filesize = NULL;
		$ukuran_file = array();
		$nama_file = array();
		$stack = array();
		for($i=0; $i<$cpt; $i++){
			if($files['userfile']['name'][$i] != ''){
				$_FILES['userfile']['name'] 		= $files['userfile']['name'][$i];
				$_FILES['userfile']['type'] 		= $files['userfile']['type'][$i];
				$_FILES['userfile']['tmp_name'] 	= $files['userfile']['tmp_name'][$i];
				$_FILES['userfile']['error']		= $files['userfile']['error'][$i];
				$_FILES['userfile']['size'] 		= $files['userfile']['size'][$i];
				if(!$this->ci->upload->do_upload('userfile')){
					$this->ci->upload->display_errors("<span class='error'>", "</span>");
					$status = 0;
					array_push($stack, $status);
					$this->ci->session->set_userdata('status_upload', $stack);
				}
				else if($this->ci->upload->do_upload('userfile')){
					$data = $this->ci->upload->data();
					$filename = element('file_name', $data);
					$filesize += element('file_size', $data);
					$namanya = substr(strrchr($filename,'.'),1);
					$this->ci->session->set_userdata('nama_file'.$i, $filename);
					$status = 1;
					array_push($stack, $status);
					$this->ci->session->set_userdata('status_upload', $stack);
					
					array_push($nama_file, $data['file_name']);
					array_push($ukuran_file, $data['file_size']);
				}
			}
		}
		
		$this->ci->session->set_userdata('nama_file', $nama_file);
		$this->ci->session->set_userdata('ukuran_file', $ukuran_file);
	}
}