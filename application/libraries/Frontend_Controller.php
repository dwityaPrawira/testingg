<?php

class Frontend_Controller extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->data['meta_title'] = 'Aplikasi Event';
        
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('nativesession');
        $this->load->library('table');
        $this->load->library('encrypt');
        
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('ppof_helper');
        $this->load->helper('html');

        $this->check_login(); // prevent bypass login form

        $this->load->model('M_main');
        $group_level 	= $this->nativesession->get('group_level');


        // generate count approval
        $this->load->model('M_approval');
        $id_div = $this->nativesession->get('id_div');
    	$id_group = $this->nativesession->get('id_group');
        $this->data['count_approval'] = $this->M_approval->count_data($id_group, $id_div);

        // generate count arsip surat
        $this->load->model('M_inbox');
        $group_level = $this->nativesession->get('group_level');
        $group_suppervisor = $this->nativesession->get('group_suppervisor');
        $id_employee = $this->nativesession->get('id_employee');
        if($group_level == 1){
            $count_query_arsip = $this->M_inbox->count_data_gm($id_div, '5');
        } else if($group_level == 2){
            $param_search['id_tujuan'] = $id_group;
            $param_search['div_tracking'] = $id_div;
            $count_query_arsip = $this->M_inbox->count_data_two_new($param_search);
        } else {
            $param_search['id_tujuan'] = $id_employee;
            $param_search['div_tracking'] = $id_div;
            $count_query_arsip = $this->M_inbox->count_data_two_new($param_search);
        }
        $this->data['count_arsip'] = $count_query_arsip;


        //get menu
        $this->data['menu_utama'] = $this->M_main->generate_menu($group_level);
        /* This Application Must Be Used With BootStrap 3 *  */
        $this->config_pagination['full_tag_open'] = "<ul class='pagination'>";
        $this->config_pagination['full_tag_close'] ="</ul>";
        $this->config_pagination['num_tag_open'] = '<li>';
        $this->config_pagination['num_tag_close'] = '</li>';
        $this->config_pagination['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $this->config_pagination['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $this->config_pagination['next_tag_open'] = "<li>";
        $this->config_pagination['next_tagl_close'] = "</li>";
        $this->config_pagination['prev_tag_open'] = "<li>";
        $this->config_pagination['prev_tagl_close'] = "</li>";
        $this->config_pagination['first_tag_open'] = "<li>";
        $this->config_pagination['first_tagl_close'] = "</li>";
        $this->config_pagination['last_tag_open'] = "<li>";
        $this->config_pagination['last_tagl_close'] = "</li>";
        // end of file Pagination.php
        
    }

    function check_login(){
    	$username  = $this->nativesession->get('username');
    	if(empty($username) || !isset($username)){
    		redirect('login/index');
    	}
    }

}