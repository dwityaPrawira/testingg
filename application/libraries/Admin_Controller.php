<?php

class Admin_Controller extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->data['meta_title'] = 'Admin Page';

        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('ppof_helper');
        $this->load->helper('html');

        $this->load->library('form_validation');
        $this->load->library('nativesession');



        //get menu
        $this->data['menu_utama'] = $this->M_main->generate_menu($group_level);
        /* This Application Must Be Used With BootStrap 3 *  */
        $this->config_pagination['full_tag_open'] = "<ul class='pagination'>";
        $this->config_pagination['full_tag_close'] ="</ul>";
        $this->config_pagination['num_tag_open'] = '<li>';
        $this->config_pagination['num_tag_close'] = '</li>';
        $this->config_pagination['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $this->config_pagination['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $this->config_pagination['next_tag_open'] = "<li>";
        $this->config_pagination['next_tagl_close'] = "</li>";
        $this->config_pagination['prev_tag_open'] = "<li>";
        $this->config_pagination['prev_tagl_close'] = "</li>";
        $this->config_pagination['first_tag_open'] = "<li>";
        $this->config_pagination['first_tagl_close'] = "</li>";
        $this->config_pagination['last_tag_open'] = "<li>";
        $this->config_pagination['last_tagl_close'] = "</li>";
        // end of file Pagination.php


        function check_login(){
        $username  = $this->nativesession->get('username');
        $level     = $this->nativesession->get("group_level");

        if(empty($username) || !isset($username)){
            redirect('login/index');
        }

        //  tambahan variable level untuk validasi level khusus administrator yaitu = '0'
        /*if(empty($username) || !isset($username) || $level != 0){
            redirect('login/index');
        }*/
    
    }

}