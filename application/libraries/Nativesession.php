<?php 
if ( ! defined('BASEPATH') )
    exit( 'No direct script access allowed' );

//load our Nativesession library
// $this->load->library( 'nativesession' );        
// Read the username from session
// $username = $this->nativesession->get( 'username' );
// Update shopping cart session data
// $this->nativesession->set( 'cart', $cart );

class Nativesession
{
    public function __construct()
    {
        // session_start();
    }

    public function set( $key, $value )
    {
        $_SESSION[$key] = $value;
    }

    public function get( $key )
    {
        return isset( $_SESSION[$key] ) ? $_SESSION[$key] : null;
    }

    public function regenerateId( $delOld = false )
    {
        session_regenerate_id( $delOld );
    }

    public function delete( $key )
    {
        unset( $_SESSION[$key] );
    }
}
?>