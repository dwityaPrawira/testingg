<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Group extends Frontend_Controller {

	var $template = 'web/template';
    var $limit = 10;

	public function __construct() {
        parent::__construct();   

        $this->load->model("M_group");
        $this->load->library("nativesession");     
    }

    public function index() {
    	$this->data['title'] = 'Satker';
        $this->data['content'] = 'templates/list';
        $this->data['table_width'] = '';
        $this->data['table_header'] = array('No','SATKER', '');
        $this->data['ajax_target'] = 'C_group/ajax_target';
        $this->data['add_link'] = 'C_group/add';
        $this->data['useDatatable'] = TRUE;
        $this->data['filter'] = array(
            array('column_number' => 1,'filter_type' => '\'text\'','filter_delay' => 500),
        );
        $this->load->view($this->template,$this->data);
    }

    public function ajax_target() {
        $group_level        = $this->nativesession->get('group_level'); // check level
        $id_group           = $this->nativesession->get('id_group');
        $group_suppervisor  = $this->nativesession->get('group_suppervisor'); // get supervisor id
        $id_div             = $this->nativesession->get('id_div');

        $draw = $_GET['draw'];
        $offset = $_GET['start'];
        $length = $_GET['length'];
        $param_search = array();

        if(($_GET['columns'][1]['search']['value']!='')) $param_search['ppof_tbl_group.group_name'] = $_GET['columns'][1]['search']['value'];
        
        $dataReal           = $this->M_group->get_data($param_search, $offset, $this->limit);
        $recordsFiltered    = $this->M_group->count_data($param_search);
        $recordsTotal       = $this->M_group->count_data();

        $data = array(
            'draw'              => $draw,
            'recordsTotal'      => $recordsTotal,
            'recordsFiltered'   => $recordsFiltered,
            'data'              => array()
        );
        $i = 1 + $offset;

        foreach($dataReal as $row){
            $id_group  = base64_encode($this->encrypt->encode($row->id_group, 'Test@123'));
            $data['data'][] = array(
                $i, $row->group_name, anchor('C_group/get_data/'.$id_group.'/'.$id_div,'<i class="glyphicon glyphicon-pencil"></i>
    <span class="hidden-tablet">Edit</span>')
            );
            $i++;
        }
        die(json_encode($data));
    }


    public function add() {
        $id_group = "";
        $group_name = "";
        $id_div = $this->nativesession->get('id_div');
        $this->data['ddown_group'] = $this->M_group->get_data_divisi();

        $last_gid = $this->M_group->get_last_group();

        $addval = substr($last_gid->id_group, 1);
        $addval = $addval + 1;        
        $mergeval = "G";
        $mergeval .= $addval;

        $this->data['id_group'] = $mergeval;

        $this->data['content'] = "admin/add_group";
        $this->load->view($this->template, $this->data);
    }

    public function insert() {
        $nama = $this->input->post('npp');
        $data = array(
            'id_group' => $this->input->post('id_group'),
            'group_name' => $this->input->post('nama'),
            'group_level' => $this->input->post('role'),
            'id_div' => '1'
        );

        $this->M_group->insert($data);
        redirect('C_group/index');    
    }

    public function get_data($id_group, $id_div) {

        $id_group  = $this->encrypt->decode(base64_decode($id_group), 'Test@123');
        // $groups = $this->M_group->group_by_id($id_group, $id_div);
        $groups = $this->M_group->group_detail($id_group);

        $this->data['idx'] = $groups->idx;
        $this->data['group_name'] = $groups->group_name;
        $this->data['id_group'] = $groups->id_group;
        $this->data['group_suppervisor'] = $groups->group_suppervisor;
        $this->data['group_level'] = $groups->group_level;
        $this->data['id_div'] = $groups->id_div;
        $this->data['ddown_div'] = $this->M_group->get_data_divisi();
        $this->data['ddown_supervisor'] = $this->M_group->group_list_all($id_group);
        $this->data['content'] = "admin/view_group";

        $this->load->view($this->template, $this->data);
    }

    public function update() {
    	$idx = $this->input->post('idx');

        $data = array(
            'group_name' => $this->input->post('group_name'),
            'group_suppervisor' => $this->input->post('ddown_supervisor'),
            'id_div' => '1',
            'group_level' => $this->input->post('role')
        );

        $this->M_group->update($data, $idx);
        redirect('C_group/index');
    }

}