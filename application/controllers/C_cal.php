<?php

class C_Cal extends Frontend_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_inbox');
		$this->load->model('M_main');
		$this->load->model('M_surat');
		$this->load->model('M_kalender');
		$this->load->model('M_group');

		$this->load->library('table');
	}

	var $template = "web/template";

	public function index(){

		$group_level = $this->nativesession->get('group_level');
		$id_group = $this->nativesession->get('id_group');
		$id_div = $this->nativesession->get('id_div');

		// $data_calendar = $this->M_kalender->get_list_cal();
		if($group_level == '1') {
			$data_calendar = $this->M_kalender->get_list_cal();
		} else {
			$scope = $this->M_group->group_scope($id_group);
			$scope_group = $scope->scope;
			$flag_group = $scope->flag;
			if($flag_group == '0') {
				if($scope_group == '0') {
					$data_calendar = $this->M_kalender->get_list_cal_word('KSI');
				} else if ($scope_group == '1'){
					$data_calendar = $this->M_kalender->get_list_cal_word('HAL');
				} else if ($scope_group == '2'){
					$data_calendar = $this->M_kalender->get_list_cal_word('PI');
				} else if ($scope_group == '3'){
					$data_calendar = $this->M_kalender->get_list_cal_word('MUSEUM');
				}
			} else {
				$data_calendar = $this->M_kalender->get_cal_scope($scope_group, $id_div);
			}
		}
		
		$calendar = array();
		foreach ($data_calendar as $key => $val) {
			$calendar[] = array(
				'id' 	=> intval($val->id),
				'title' => $val->title,
				'description' => $val->description,
				'start' => date_format(date_create($val->start_date), "Y-m-d H:i:s"),
				'end' 	=> date_format(date_create($val->end_date), "Y-m-d H:i:s"),
				'waktuawal' => $val->waktuawal,
				'waktuakhir' => $val->waktuakhir,
				'biro' => $val->biro,
				'dukungan' => $val->dukungan,
				'color' => $val->color,
			);
		}

		$this->data['get_data'] = json_encode($calendar);
		$this->data['content'] = "templates/calendar";
		$this->data['use_calendar'] = TRUE;
		$this->data['customJs'] = 'calendar.js';
		$this->data['useDatatable'] = TRUE;
		$this->load->view($this->template, $this->data);
	}
}
