<?php 

class C_Surat extends Frontend_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('m_main');
		$this->load->model('M_surat');
		$this->load->helper('array');
	}

	var $template = 'web/template';

	public function index(){

		$id_div = $this->nativesession->get('id_div');
		$this->data['ddown_tujuan'] = $this->M_surat->tujuan_event($id_div);

		$group_suppervisor 	= $this->nativesession->get('group_suppervisor');
		$check_grp_level = $this->M_surat->check_level($group_suppervisor);
        $supervisor_level = $check_grp_level->group_level;

		$this->data['supervisor_level'] = $supervisor_level;
		$this->data['content'] = "templates/create_surat";
    	$this->load->view($this->template, $this->data);
	}

	public function kirim(){

		$this->load->library('form_validation');

        $this->form_validation->set_rules('subject', 'Nama Kegiatan', 'required');
		$this->form_validation->set_rules('tgl_awal', 'Tanggal Awal', 'required');
		$this->form_validation->set_rules('tgl_akhir', 'Tanggal Akhir', 'required');
		$this->form_validation->set_rules('waktuawal', 'Waktu Awal', 'required');
		$this->form_validation->set_rules('waktuakhir', 'Waktu Akhir', 'required');

		if ($this->form_validation->run() == FALSE) {
		
			$id_div = $this->nativesession->get('id_div');
			$this->data['ddown_tujuan'] = $this->M_surat->tujuan_event($id_div);

			$group_suppervisor 	= $this->nativesession->get('group_suppervisor');
			$check_grp_level = $this->M_surat->check_level($group_suppervisor);
			$supervisor_level = $check_grp_level->group_level;

			$this->data['supervisor_level'] = $supervisor_level;
			$this->data['content'] = "templates/create_surat";
			$this->load->view($this->template, $this->data);

		} else {

			$id_group 			= $this->nativesession->get('id_group');
			$group_name 		= $this->nativesession->get('group_name');
			$group_suppervisor 	= $this->nativesession->get('group_suppervisor');
			$id_div 			= $this->nativesession->get('id_div');
			$username			= $this->nativesession->get('username');
			$id_employee		= $this->nativesession->get('id_employee');
			$tanggal 			= date('Y-m-d H:i:s');
			$type 				= "event";

			// upload file
			$check_upload 	= $_FILES['userfile']['name'];
			$upload = array_filter($check_upload);
			if(!empty($upload)){
				$this->cek_upload($check_upload, $type);
			}
			
			$tujuan 	= $this->input->post('div_id');
			$tujuan 	= array_filter($tujuan);

			if(empty($tujuan)){ // if empty redirect to index
				redirect('c_surat/index');
			}

			// insert checkbox
			$checkboxes = $this->input->post('check_list');
			if(empty($checkboxes)) {
				redirect('c_surat/index');
			}
			$dukungan = implode(", ", $checkboxes);

			// set parameter to database
			$data_surat = array(
				'tipe_surat' 	=> $type,
				'subyek_surat' 	=> $this->input->post('subject'),
				'content' 		=> $this->input->post('content'),
				'tgl_create' 	=> date('Y-m-d H:i:s'),
				'biro'			=> $this->input->post('biro'),
				'dukungan'		=> $dukungan,
				'tglawal'		=> $this->input->post('tgl_awal'),
				'tglakhir'		=> $this->input->post('tgl_akhir'),
				'waktuawal'		=> $this->input->post('waktuawal'),
				'waktuakhir'	=> $this->input->post('waktuakhir')
			);

			$this->m_main->insert_ar(TBL_SURAT, $data_surat); // insert into database content surat
			$lastid = $this->db->insert_id();

			// insert into database
			insert_tujuan($tujuan, 1, $lastid); // insert tujuan

			if($this->session->userdata('nama_file')){
				$nama_file 		= $this->session->userdata('nama_file');
				$ukuran_file 	= $this->session->userdata('ukuran_file');
				$month 			= date('m');
				$attachmentdata = array_combine($nama_file, $ukuran_file);
				insert_attach($attachmentdata, $month, $lastid);
			}

			$sent_status = "";
			// cek punya supervisor atau tidak
			if(!empty($group_suppervisor)){
				$data_flow_kirim = array(
					'id_surat' 		=> $lastid,
					'id_status' 	=> 2,
					'id_divisi' 	=> $id_div,
					'id_group' 		=> $group_suppervisor,
					'id_pengirim' 	=> $id_employee,
					'npp_pengirim'	=> $username,
					'id_approval'	=> $id_group,
				);
				$this->m_main->insert_ar(TBL_FLOW, $data_flow_kirim);
				$sent_status = '2';
			} else {
				$approval_flag = 5; // langsung dikirim suratnya
				// loop berdasarkan table ppof_tbl_tujuan
				$tujuan_surat = $this->M_surat->get_tujuan($lastid);
				$flow_terkirim = array(
					'id_surat' 		=> $lastid,
					'id_status' 	=> 5,
					'id_divisi' 	=> $id_div,
					'id_group' 		=> $id_group,
					'id_pengirim' 	=> $id_employee,
					'tgl_approve'	=> $tanggal,
					'npp_pengirim'	=> $username,
					'id_approval'	=> $id_group,
					'sent_status' => '0'
				);
				tujuan_terkirim_event($tujuan_surat, $flow_terkirim);
				// insert nomor surat
				$id_identity = $this->nativesession->get('div_identity');
				$data_number = array(
					'id_surat' => $lastid,
					'generate_number' => $id_identity.'/'.$lastid,
				);
				$this->m_main->insert_ar(TBL_GENERATE, $data_number);
				$sent_status = '5';

				$group_suppervisor = $id_group;

				$tglakhir = $this->input->post('tgl_akhir');
				$tglakhir = $tglakhir.' 23:59:59';

				$data_calendar = array(
					'title' => $this->input->post('subject'),
					'description' => $this->input->post('content'),
					'start_date' => $this->input->post('tgl_awal'),
					'end_date' => $tglakhir,
					'waktuawal' => $this->input->post('waktuawal'),
					'waktuakhir' => $this->input->post('waktuakhir'),
					'biro' => $this->input->post('biro'),
					'dukungan' => $dukungan,
					'id_surat' => $lastid
				);
				$this->m_main->insert_ar('calendar', $data_calendar);
			}

			// untuk sent item
			$data_flow_sent = array(
				'id_surat' 		=> $lastid,
				'id_status' 	=> 3,
				'id_divisi' 	=> $id_div,
				'id_group' 		=> $id_group,
				'id_pengirim' 	=> $id_employee,
				'npp_pengirim'	=> $username,
				'tgl_approve'	=> $tanggal,
				'group_tracking'=> $group_suppervisor,
				'sent_status'	=> $sent_status,
				'group_pengirim' => $group_name
			);
			$this->m_main->insert_ar(TBL_FLOW, $data_flow_sent);

			$this->session->set_flashdata('success_message', 'Event berhasil dikirim');
			redirect('c_surat/index');

		}		
	}

	public function cek_upload($check_upload, $type){
		
		if(!empty($check_upload)){ 
			$this->load->library('uploader');
			$this->uploader->do_upload($check_upload);
			$status = $this->session->userdata('status_upload');
			if(in_array(0, $status)){
				$this->session->unset_userdata('status_upload');
				$this->session->set_flashdata('message', 'Maaf, Extensi / total ukuran file attachment anda tidak sesuai dengan yang telah ditentukan');
				redirect('c_surat/index');
			}
			else{
				$this->session->unset_userdata('status_upload');
				$this->nativesession->delete('subject');
				$this->nativesession->delete('content');
			}
		}
	}

}