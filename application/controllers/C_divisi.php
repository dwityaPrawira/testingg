<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Divisi extends Frontend_Controller {

	var $template = 'web/template';
    var $limit = 10;

	public function __construct() {
        parent::__construct();   

        $this->load->model("M_divisi");
        $this->load->library("nativesession");     
    }

    public function index() {
    	$this->data['title'] = 'Divisi';
        $this->data['content'] = 'templates/list';
        $this->data['table_width'] = '';
        $this->data['table_header'] = array('No','NAMA DIVISI', 'KODE DIVISI', '');
        $this->data['ajax_target'] = 'C_divisi/ajax_target';
        $this->data['add_link'] = 'C_divisi/add';
        $this->data['filter'] = array(
            array('column_number' => 1,'filter_type' => '\'text\'','filter_delay' => 500),
        );
        $this->load->view($this->template,$this->data);
    }

    public function ajax_target() {
        $group_level        = $this->nativesession->get('group_level'); // check level
        $id_group           = $this->nativesession->get('id_group');
        $group_suppervisor  = $this->nativesession->get('group_suppervisor'); // get supervisor id
        $id_div             = $this->nativesession->get('id_div');

        $draw = $_GET['draw'];
        $offset = $_GET['start'];
        $length = $_GET['length'];
        $param_search = array();

        if(($_GET['columns'][1]['search']['value']!='')) $param_search['ppof_tbl_div.div_name'] = $_GET['columns'][1]['search']['value'];
        
        $dataReal           = $this->M_divisi->get_data($param_search, $offset, $this->limit);
        $recordsFiltered    = $this->M_divisi->count_data($param_search);
        $recordsTotal       = $this->M_divisi->count_data();

        $data = array(
            'draw'              => $draw,
            'recordsTotal'      => $recordsTotal,
            'recordsFiltered'   => $recordsFiltered,
            'data'              => array()
        );
        $i = 1 + $offset;

        foreach($dataReal as $row){
            $id_div  = base64_encode($this->encrypt->encode($row->id_div, 'Test@123'));
            $data['data'][] = array(
                $i, $row->div_name, $row->div_identity, anchor('C_divisi/get_data/'.$id_div,'<i class="glyphicon glyphicon-pencil"></i>
    <span class="hidden-tablet">Edit</span>')
            );
            $i++;
        }
        die(json_encode($data));
    }


    public function add() {
        $this->data['content'] = "admin/add_divisi";
        $this->load->view($this->template, $this->data);
    }

    public function insert() {
        $nama = $this->input->post('npp');
        $data = array(
            'div_name' => $this->input->post('nama'),
            'div_identity' => $this->input->post('kode'),
            'active' => '1'
        );

        $this->M_divisi->insert($data);
        redirect('C_divisi/index');    
    }

    public function get_data($id_div) {

        $id_div = $this->encrypt->decode(base64_decode($id_div), 'Test@123');
        $divisi = $this->M_divisi->div_detail($id_div);     

        $this->data['idx'] = $id_div;
        $this->data['div_name'] = $divisi->div_name;
        $this->data['div_identity'] = $divisi->div_identity;
        $this->data['active'] = $divisi->active;
        $this->data['content'] = "admin/view_divisi";

        $this->load->view($this->template, $this->data);
    }

    public function update() {
    	$idx = $this->input->post('idx');

        $data = array(
            'div_name' => $this->input->post('div_name'),
            'div_identity' => $this->input->post('div_identity'),
            'active' => '1'
        );

        $this->M_divisi->update($data, $idx);
        redirect('C_divisi/index');
    }

}