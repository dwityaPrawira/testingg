<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();
class Pages extends Frontend_Controller {

	var $template = 'web/template';

	public function __construct() {
        parent::__construct();

        $this->load->model('M_inbox');
        $this->load->model('m_approval');
		$this->load->model('M_group');
        
    }

	var $limit = 10;

    public function index(){

		$this->load->model('M_employee');
		$this->load->library('session');

    	$id_div = $this->nativesession->get('id_div');
    	$id_group = $this->nativesession->get('id_group');
    	$id_tujuan = $this->nativesession->get('id_employee');
		$id_pengirim = $this->nativesession->get('id_employee');
    	$group_suppervisor = $this->nativesession->get('group_suppervisor');
		$group_level = $this->nativesession->get('group_level');
    	$npp = $this->nativesession->get('npp');		

		$get_data_emp = $this->M_employee->get_employee($npp); // get flag employee
		$flag = $get_data_emp->flag;

		if($flag == '1') {
			session_destroy(); // clear session
			redirect('Login/user_deactive');
		}

		if($group_level == '1') { // monitoring admin

			$diajukan = $this->M_inbox->kegiatan_diajukan();
			$disetujui = $this->M_inbox->kegiatan_disetujui();
			$ditolak = $this->M_inbox->kegiatan_ditolak();
			$berjalan = $this->M_inbox->kegiatan_berjalan();

			$query = $this->M_inbox->get_data_event();
			$count_query = $this->M_inbox->count_data_event();

		} else if($group_level == '2') {

			// get table scope
			$scope = $this->M_group->group_scope($id_group);
			$scope_group = $scope->scope;

			// $diajukan = $this->M_inbox->kegiatan_diajukan_user($id_pengirim, $id_div);
			$diajukan = $this->M_inbox->kegiatan_diajukan_group($id_div, $scope_group);
			// $disetujui = $this->M_inbox->kegiatan_disetujui_user($id_pengirim, $id_div);
			$disetujui = $this->M_inbox->kegiatan_disetujui_group($id_div, $scope_group);
			// $berjalan = $this->M_inbox->kegiatan_berjalan_user($id_pengirim, $id_div);
			$berjalan = $this->M_inbox->kegiatan_berjalan_group($id_div, $scope_group);
			// $ditolak = $this->M_inbox->kegiatan_ditolak_user($id_pengirim, $id_div);
			$ditolak = $this->M_inbox->kegiatan_ditolak_group($id_div, $scope_group);
			
			$query = $this->M_inbox->event_group_scope($scope_group, $id_div);
			$count_query = $this->M_inbox->count_event_group_scope($scope_group, $id_div);

			// $query = $this->M_inbox->get_data_event_user($id_pengirim, $id_div, $id_group);
			// $count_query = $this->M_inbox->count_data_event_user($id_pengirim, $id_div, $id_group);

		} else if($group_level == '3') {

			// $diajukan = $this->M_inbox->kegiatan_diajukan();
			// $disetujui = $this->M_inbox->kegiatan_disetujui();
			// $ditolak = $this->M_inbox->kegiatan_ditolak();
			// $berjalan = $this->M_inbox->kegiatan_berjalan();

			// $query = $this->M_inbox->get_data_event();
			// $count_query = $this->M_inbox->count_data_event();

			// get table scope
			$scope = $this->M_group->group_scope($id_group);
			$scope_group = $scope->scope;

			// $diajukan = $this->M_inbox->kegiatan_diajukan_user($id_pengirim, $id_div);
			$diajukan = $this->M_inbox->kegiatan_diajukan_group($id_div, $scope_group);
			// $disetujui = $this->M_inbox->kegiatan_disetujui_user($id_pengirim, $id_div);
			$disetujui = $this->M_inbox->kegiatan_disetujui_group($id_div, $scope_group);
			// $berjalan = $this->M_inbox->kegiatan_berjalan_user($id_pengirim, $id_div);
			$berjalan = $this->M_inbox->kegiatan_berjalan_group($id_div, $scope_group);
			// $ditolak = $this->M_inbox->kegiatan_ditolak_user($id_pengirim, $id_div);
			$ditolak = $this->M_inbox->kegiatan_ditolak_group($id_div, $scope_group);

			$query = $this->M_inbox->get_data_event_user($id_pengirim, $id_div, $id_group);
			$count_query = $this->M_inbox->count_data_event_user($id_pengirim, $id_div, $id_group);

		}

    	$this->data['diajukan'] = $diajukan; // count data kegiatan diajukan
    	$this->data['disetujui'] = $disetujui; // count data kegiatan disetujui
    	$this->data['ditolak'] = $ditolak; // count data kegiatan ditolak
    	$this->data['berjalan'] = $berjalan; // count data kegiatan berjalan

    	
    	$this->data['keuangan'] = $this->M_inbox->count_biro_keu(); // count biro keuangan
    	$this->data['sdm'] = $this->M_inbox->count_biro_sdm(); // count biro sdm
    	$this->data['sekre'] = $this->M_inbox->count_biro_sekre(); // count biro sekre
    	$this->data['umum'] = $this->M_inbox->count_biro_umum(); // count biro umum

    	// ================================================================================
		$data_inbox = $query;
		$num_rows = $count_query;

		if($num_rows > 0){
            $this->config_pagination['base_url'] 	= site_url('c_kalender/index/');
            $this->config_pagination['total_rows'] 	= $num_rows;
            $this->config_pagination['per_page'] 	= $this->limit;
            
			$this->pagination->initialize($this->config_pagination);
			$this->data['pagination'] 	= $this->pagination->create_links();
				
			$tmpl = array ( 'table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="table table-striped table-condensed">', 'row_alt_start' => '<tr class="zebra">', 'row_alt_end' => '</tr>' );
			$this->table->set_template($tmpl);
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'TANGGAL EVENT', 'SUBJECT', 'DUKUNGAN','STATUS');
			$i = 0;
			
			foreach($data_inbox as $row){

				$date_now = date("Y-m-d");

	            if($row->id_status == '5') {
	            	if($date_now < $row->tglawal) {
						$colors = "<span class='label label-success'>DISETUJUI</span>";
		            } else {
		            	$colors = "<span class='label label-primary'>BERJALAN</span>"; // sudah berjalan
		            }	            	
	            } else if($row->id_status == '7') {
	            	if($date_now < $row->tglawal) {
						$colors = "<span class='label label-success'>DISETUJUI</span>";
		            } else {
		            	$colors = "<span class='label label-primary'>BERJALAN</span>"; // sudah berjalan
		            }	            	
	            } else if($row->id_status == '2') {
	            	$colors = "<span class='label label-info'>MENUNGGU PERSETUJUAN</span>";	
	            } else if($row->id_status == '8') {
	            	if($date_now < $row->tglawal) {
						$colors = "<span class='label label-success'>DISETUJUI</span>";
		            } else {
		            	$colors = "<span class='label label-primary'>BERJALAN</span>"; // sudah berjalan
		            }	    
	            }else {
	            	$colors = "<span class='label label-danger'>DITOLAK</span>";	
	            }

				$id  = base64_encode($this->encrypt->encode($row->id, 'Test@123'));
				$id_surat = base64_encode($this->encrypt->encode($row->id_surat, 'Test@123'));
				$this->table->add_row(++$i,  $row->tglawal.' s/d '.$row->tglakhir, '<b>'.limit_to_numwords($row->subyek_surat, 5).'</b>', $row->dukungan,$colors);
			}
			$this->data['table'] = $this->table->generate();
			$this->table->clear();

		} else { // empty data
			$this->data['table'] = "Tidak Ada Data.";
			// $this->session->set_flashdata("msg_inbox", "Tidak ada data");
		}
    	// ================================================================================
    	
    	$group_level = $this->nativesession->get('group_level');
    	if($group_level == 1){ // div head. id_tujuan, id_status
    		$id_status = '5';
			$count_query = $this->M_inbox->count_data_gm($id_div, $id_status);
			$count_approval = $this->m_approval->count_data($id_group, $id_div);
			$this->data['visible'] = "";
		} else if ($group_level == 2){ // tier 2 user biasa
            $id_tujuan = $id_group;
			$count_query = $this->M_inbox->count_data_two($id_tujuan, $id_div, $group_suppervisor, $id_group);
			$count_approval = $this->m_approval->count_data($id_group, $id_div);
			$this->data['visible'] = "";
		} else {
			$count_query = $this->M_inbox->count_data_two($id_tujuan, $id_div, $group_suppervisor, $id_group);
			$count_approval = $this->m_approval->count_data($id_group, $id_div);
			$this->data['visible'] = "none";
		}

		// $graph = $this->M_inbox->get_group_pengirim();
		$this->data['graph'] = $this->M_inbox->get_group_pengirim();

		$this->data['graph_success'] = $this->M_inbox->graph_success_login();
		$this->data['graph_fail'] = $this->M_inbox->graph_fail_login();

		$this->data['group_level'] = $group_level;
		$this->data['approval_count'] = $count_approval;
		$this->data['inbox_count'] = $count_query;
    	$this->data['content'] = "templates/pages";
    	$this->load->view($this->template, $this->data);
    }

}