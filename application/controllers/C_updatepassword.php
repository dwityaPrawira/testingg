<?php
class C_updatepassword extends Frontend_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library('SimpleLoginSecure');
        $this->load->library("nativesession"); 
        $this->load->helper('array');
        $this->load->model('M_employee');
    }
    var $template = "web/template";

    function index(){
        $this->data['id_employee'] = $this->nativesession->get('id_employee');
        $this->data['group_name'] = $this->nativesession->get('group_name');
        $this->data['photo'] = $this->nativesession->get('photo');
        $this->data['nama_user'] = $this->nativesession->get('nama_user');
        $this->data['npp'] = $this->nativesession->get('npp');
        
        $this->data['info'] = $_SESSION;
        $this->data['content'] = 'templates/update_password';
        $this->load->view($this->template,$this->data);
    }

    function save(){
        // print_r($this->input->post());
        $user_email = $this->input->post('user_email');
        $password_lama = $this->input->post('password_lama');
        $password_baru = $this->input->post('password_baru');
        $konfirmasi_password = $this->input->post('konfirmasi_password');
        if($password_baru != $konfirmasi_password){
            $this->session->set_flashdata("message", "Ubah password gagal, password baru dan konfirmasi password tidak sama.");
        }else{
            $update = $this->simpleloginsecure->edit_password($user_email, $password_lama, $password_baru);
            if(!$update){
                $this->session->set_flashdata("message", "Ubah password gagal, password lama yang Anda masukkan salah.");
            }else{
                $this->session->set_flashdata("success_message", "Ubah password berhasil, silahkan coba login kembali.");
            }
        }
        redirect('c_updatepassword');
    }

    function update_profile(){

        $id_employee = $this->input->post('id_employee');
        $npp = $this->input->post('npp');
        $type = "event";

        // upload file
        $check_upload = $_FILES['userfile']['name'];
        $tipe = $_FILES['userfile']['type'];
        $size = $_FILES['userfile']['size'];

        $upload = array_filter($check_upload);

        if(empty($upload)){
            $this->session->set_flashdata('message', 'Maaf, Tidak ada file yang di upload');
                redirect('C_updatepassword/index');
        } 

        $this->cek_upload($check_upload, $type);

        // update file
        $namaphoto = $check_upload['0'];
        $this->M_employee->update_foto($namaphoto, $npp, $id_employee);

        // redirect user
        $this->session->set_flashdata('success_message', 'Update foto berhasil, silahkan logout dan login kembali untuk melihat perubahan');
                redirect('C_updatepassword/index');
    }

    public function cek_upload($check_upload, $type){
        
        if(!empty($check_upload)){ 
            $this->load->library('uploadimage');
            $this->uploadimage->do_upload($check_upload);
            $status = $this->session->userdata('status_upload');
            if(in_array(0, $status)){
                $this->session->unset_userdata('status_upload');
                $this->session->set_flashdata('message', 'Maaf, Extensi / total ukuran file attachment anda tidak sesuai dengan yang telah ditentukan');
                redirect('C_updatepassword/index');
            }
            else{
                $this->session->unset_userdata('status_upload');
                $this->nativesession->delete('subject');
                $this->nativesession->delete('content');
            }
        }
    }
}