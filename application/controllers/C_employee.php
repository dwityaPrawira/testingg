<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class C_employee extends Frontend_Controller // Admin_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model("M_employee");
        $this->load->library("nativesession");
        $this->load->library('SimpleLoginSecure');
	}

	var $template = "web/template";
	var $limit = 10;

	public function index() {
		$this->data['title'] = 'Employee';
        $this->data['content'] = 'templates/list';
        $this->data['table_width'] = '';
        $this->data['table_header'] = array('No', 'NIP', 'NAMA','SATKER', '', '', '');
        $this->data['ajax_target'] = 'C_employee/ajax_target';
        $this->data['add_link'] = 'C_employee/add';
        $this->data['useDatatable'] = TRUE;
        $this->data['filter'] = array(
            array('column_number' => 1,'filter_type' => '\'text\'','filter_delay' => 500),
            array('column_number' => 2,'filter_type' => '\'text\'','filter_delay' => 500)
        );
        $this->load->view($this->template,$this->data);
	}

	public function ajax_target() {
        $group_level		= $this->nativesession->get('group_level'); 
        $id_group 			= $this->nativesession->get('id_group');
        $group_suppervisor 	= $this->nativesession->get('group_suppervisor'); 

        $draw = $_GET['draw'];
        $offset = $_GET['start'];
        $length = $_GET['length'];
        $param_search = array();

        if(($_GET['columns'][1]['search']['value']!='')) $param_search['npp'] = $_GET['columns'][1]['search']['value'];
        if(($_GET['columns'][2]['search']['value']!='')) $param_search['nama'] = strtolower($_GET['columns'][2]['search']['value']);

        
        $dataReal           = $this->M_employee->get_data($param_search, $offset, $this->limit);
        $recordsFiltered    = $this->M_employee->count_data($param_search);
        $recordsTotal       = $this->M_employee->count_data();
       
        $data = array(
            'draw'              => $draw,
            'recordsTotal'      => $recordsTotal,
            'recordsFiltered'   => $recordsFiltered,
            'data'              => array()
        );
        $i = 1 + $offset;

        foreach($dataReal as $row){
            $id  = base64_encode($this->encrypt->encode($row->id, 'Test@123'));
            $npp = base64_encode($this->encrypt->encode($row->npp, 'Test@123'));

            if($row->flag == '0') {
                $status = "deactive";
            } else {
                $status = "activate";
            }

            $data['data'][] = array(
                $i, $row->npp, $row->nama, $row->group_name, anchor('C_employee/get_data/'.$id.'/'.$npp,'<i class="glyphicon glyphicon-user"></i>
    <span class="hidden-tablet">Edit</span>') , anchor('C_employee/reset_password/'.$npp,'<i class="glyphicon glyphicon-globe"></i>
    <span class="hidden-tablet">Reset</span>') , anchor('C_employee/update_status/'.$npp,'<span class="hidden-tablet">'.$status.'</span>')
            );

            $i++;
        }
        die(json_encode($data));
	}

    public function get_data($id, $npp) {
        $id  = $this->encrypt->decode(base64_decode($id), 'Test@123');
        $npp = $this->encrypt->decode(base64_decode($npp), 'Test@123');
        $id_div = $this->nativesession->get('id_div');

        $get_data = $this->M_employee->get_data_emp($id_div, $npp, $id);

        $this->data['id'] = $get_data->id;
        $this->data['npp'] = $get_data->npp;
        $this->data['nama'] = $get_data->nama;
        $this->data['group_id'] = $get_data->id_group;
        $this->data['group_name'] = $get_data->group_name;
        $this->data['id_div'] = $get_data->id_div;
        $this->data['ddown_group'] = $this->M_employee->get_data_group($get_data->id_group, $get_data->group_name, $id_div);

        $this->data['content'] = "admin/view_emp";
        $this->load->view($this->template, $this->data);        
        
    }

    public function update() {
        $id = $this->input->post('id');
        $npp = $this->input->post('npp');

        $data_upd = array(
            'nama' => $this->input->post('nama'),
            'id_group' => $this->input->post('ddown_group'),
        );    

        if($this->M_employee->update($data_upd, $id, $npp) == TRUE) {
            redirect('C_employee/index');
        } else {
            $msg = $this->db->_error_message();
            $num = $this->db->_error_number();
            echo "Error(".$num.") ".$msg;
        }
    }

    public function add(){
        $id_group = "";
        $group_name = "";
        $id_div = $this->nativesession->get('id_div');

        $options = [
            '1' => 'BPK',
            '0' => 'HUMAS KSI BPK'
        ];
        $this->data['ddown_tipe'] = form_dropdown('tipe', $options, '', 'class="form-control"');
        $this->data['ddown_group'] = $this->M_employee->get_data_group($id_group, $group_name, $id_div);

        $this->data['content'] = "admin/add_emp";
        $this->load->view($this->template, $this->data);
    }

    function get_sub_category(){
        $category_id = $this->input->post('id',TRUE);
        $data = $this->M_employee->get_sub_category($category_id)->result();
        echo json_encode($data);
    }

    public function insert() {
        
        $npp = $this->input->post('npp');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('npp', 'Nip', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('ddown_group', 'Satker', 'required');

        if ($this->form_validation->run() == FALSE) {

            $id_group = "";
            $group_name = "";
            $id_div = $this->nativesession->get('id_div');
            $options = ['1' => 'BPK','0' => 'HUMAS KSI BPK'];
            $this->data['ddown_tipe'] = form_dropdown('tipe', $options, '', 'class="form-control"');
            $this->data['ddown_group'] = $this->M_employee->get_data_group($id_group, $group_name, $id_div);
            $this->data['content'] = "admin/add_emp";
            $this->load->view($this->template, $this->data);

        } else {
            if($this->check_npp($npp) == TRUE){ // sudah terdaftar
                // error message npp sudah ada
                redirect('C_employee/index', 'refresh');
            } else {
                // insert
                $data = array(
                    'npp' => $this->input->post('npp'),
                    'nama' => $this->input->post('nama'),
                    'id_group' => $this->input->post('ddown_group'),
                    'photo' => 'user-default.png'
                );
    
                $this->M_employee->insert($data, $npp);
                redirect('C_employee/index');    
            }
        }
    }

    public function check_npp($npp){
        $check_npp = $this->M_employee->check_npp($npp);
        return $check_npp;
    }

    public function get_mutasi($id, $npp) {
        $id  = $this->encrypt->decode(base64_decode($id), 'Test@123');
        $npp = $this->encrypt->decode(base64_decode($npp), 'Test@123');
        $id_div = $this->nativesession->get('id_div');

        $get_emp = $this->M_employee->get_data_emp($id_div, $npp, $id);

        $this->data['id'] = $get_emp->id;
        $this->data['npp'] = $get_emp->npp;
        $this->data['nama'] = $get_emp->nama;
        $this->data['ddown_div'] = $this->M_employee->div_list();
        $this->data['content'] = "admin/mutasi_emp";
        $this->load->view($this->template, $this->data);
    }

    public function get_group(){
        $id = $this->input->post('id');
        $data = $this->M_employee->group_list_by_div($id);
        echo json_encode($data);
    }

    public function mutasi() {
        $id = $this->input->post('id');
        $npp = $this->input->post('npp');

        $data = array(
            'id_group' => $this->input->post('id_group'),
        );

        $this->M_employee->update($data, $id, $npp);
        redirect('C_employee/index');
    }

    public function reset_password($npp) {
        $new_pass = "123456";
        $npp = $this->encrypt->decode(base64_decode($npp), 'Test@123');

        $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE); 
        $user_pass_hashed = $hasher->HashPassword($new_pass);

        $reset = $this->M_employee->reset_password($npp, $user_pass_hashed);
        if(!$reset){
            $this->session->set_flashdata("message", "Reset password gagal.");
        }else{
            $this->session->set_flashdata("success_message", "Reset password berhasil.");
        }
        redirect('C_employee/index');
    }

    public function delete($npp) {
        redirect('C_employee/index');
    }

    public function update_status($npp) {
        $npp_decrypt = $this->encrypt->decode(base64_decode($npp), 'Test@123');
        $get_data = $this->M_employee->get_data_employee($npp_decrypt); // get flag
        $flag = $get_data->flag;
        
        if($flag == '0') { // if flag == 0, update jadi 1
            $this->M_employee->upd_flag($npp_decrypt, "1");
        } else { // else if flag == 1, update jadi 0
            $this->M_employee->upd_flag($npp_decrypt, "0");
        }    
        redirect('C_employee/index');
    }

}