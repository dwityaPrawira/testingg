<?php
class C_autocomplete extends Frontend_Controller{
	
	public function __construct(){
		parent::__construct();
        $this->load->model('m_main');
	}
	
	public function search()
	{
		$arr = array();
        //get URL query
        $url = parse_url($_SERVER['REQUEST_URI']);
        parse_str($url['query'], $params);

        $keyword = $params['query'];
		$keyword = strtoupper($keyword);
		$data = $this->m_main->search_div($keyword);
		foreach($data as $row){
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
					'value'	=>$row->div_name,
					'data'	=>$row->id_div
			);
		}
		echo json_encode($arr);
	}

	public function search_employee() {

		$arr = array();
        //get URL query
        $url = parse_url($_SERVER['REQUEST_URI']);
        parse_str($url['query'], $params);

        $keyword = $params['query'];
		$keyword = strtoupper($keyword);
		$data = $this->m_main->search_employee($keyword);
		foreach($data as $row){
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
					'value'	=>$row->nama,
					'data'	=>$row->npp
			);
		}
		echo json_encode($arr);
		
	}
}