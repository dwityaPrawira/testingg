<?php

class C_Disposisi extends Frontend_Controller
{

	var $template = "web/template";
	var $limit = 15;

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_inbox');
		$this->load->model('M_main');
		$this->load->model('M_surat');
		$this->load->model('M_disposisi');
		$this->load->library('table');
	}

	public function index()
	{
		$this->data['title'] = 'Disposisi';
		$this->data['content'] = 'templates/list';
		$this->data['table_width'] = '';
		$this->data['table_header'] = array('No', 'TANGGAL', 'SUBJECT');
		$this->data['ajax_target'] = 'c_disposisi/ajax_target';
		$this->data['useDatatable'] = TRUE;
		$this->data['filter'] = array(
			array('column_number' => 2, 'filter_type' => '\'text\'', 'filter_delay' => 500)
		);
		$this->load->view($this->template, $this->data);
	}

	public function ajax_target()
	{
		$group_level 	= $this->nativesession->get('group_level');
		$id_group		= $this->nativesession->get('id_group');
		$id_employee	= $this->nativesession->get('id_employee');
		$id_div			= $this->nativesession->get('id_div');

		if ($group_level == 1) {
			$tujuan = $id_div;
		} else {
			$tujuan = $id_group;
		}

		$draw = $_GET['draw'];
		$offset = $_GET['start'];
		$length = $_GET['length'];
		$param_search = array(
			'id_tujuan' => $tujuan
		);

		if (($_GET['columns'][2]['search']['value'] != '')) $param_search['subyek_surat'] = $_GET['columns'][2]['search']['value'];

		$recordsTotal = $this->M_disposisi->count_data_new();
		$recordsFiltered = $this->M_disposisi->count_data_new($param_search);
		$dataReal = $this->M_disposisi->get_data_new($param_search, $offset, $this->limit);

		$data = array(
			'draw' => $draw,
			'recordsTotal' => $recordsTotal,
			'recordsFiltered' => $recordsFiltered,
			'data' => array()
		);
		$i = 1 + $offset;
		foreach ($dataReal as $row) {
			// $id  = base64_encode($this->encrypt->encode($row->id, 'Test@123'));
			// $id_surat = base64_encode($this->encrypt->encode($row->id_surat, 'Test@123'));

			$id = $row->id;
			$id_surat = $row->id_surat;

			$data['data'][] = array(
				$i, format_datetime($row->tgl_disposisi), anchor('c_disposisi/show/' . $id . '/' . $id_surat, limit_to_numwords($row->subyek_surat, 5), 'style="font-weight: bold; text-decoration: underline;"')
			);
			$i++;
		}
		die(json_encode($data));
	}

	public function index_old()
	{
		$this->data['pagination'] = '';
		$group_level 	= $this->nativesession->get('group_level');
		$id_group		= $this->nativesession->get('id_group');
		$id_employee	= $this->nativesession->get('id_employee');

		$uri_segment = 3;
//		if($this->uri->segment($uri_segment) == NULL){
//			$val_segment = 1;
//		} else {
//			$val_segment = $this->uri->segment($uri_segment);
//		}
		$offset = $this->uri->segment($uri_segment);

		$get_data 	= $this->M_disposisi->get_data($this->limit, $offset, $id_group);
		$count_data = $this->M_disposisi->count_data($id_group);

		if ($count_data > 0) {
			$config['base_url'] 	= site_url('c_inbox/index/');
			$config['total_rows'] 	= $count_data;
			$config['per_page'] 	= $this->limit;
			$config['uri_segment'] 	= $uri_segment;

			$this->pagination->initialize($config);
			$this->data['pagination'] 	= $this->pagination->create_links();

			$tmpl = array('table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="table table-striped table-condensed">', 'row_alt_start' => '<tr class="zebra">', 'row_alt_end' => '</tr>');
			$this->table->set_template($tmpl);
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'TANGGAL', 'SUBJECT');
			$i = 0 + $offset;

			foreach ($get_data as $row) {
				$id  = base64_encode($this->encrypt->encode($row->id, 'Test@123'));
				$id_surat = base64_encode($this->encrypt->encode($row->id_surat, 'Test@123'));
				$this->table->add_row(++$i, format_datetime($row->tgl_disposisi), anchor('c_disposisi/show/' . $id . '/' . $id_surat, limit_to_numwords($row->subyek_surat, 5)));
			}
			$this->data['table'] = $this->table->generate();
			$this->table->clear();
		} else {
			$this->data['table'] = "Tidak Ada Data.";
			$this->session->set_flashdata("msg_disposisi", "Tidak terdapat data disposisi");
		}

		$this->data['content'] = "templates/disposisi";
		$this->load->view($this->template, $this->data);
	}

	public function show($id, $id_surat)
	{
		$id_group 	= $this->nativesession->get('id_group');
		$div_name 	= $this->nativesession->get('div_name');
		$id_divisi 	= $this->nativesession->get('id_div');
		$group_level = $this->nativesession->get('group_level');

		// $id = $this->encrypt->decode(base64_decode($id), 'Test@123');
		// $id_surat = $this->encrypt->decode(base64_decode($id_surat), 'Test@123');

		$notes_approval = $this->M_inbox->get_comment_approval($id_surat);

		if(isset($notes_approval)) {
			$tmpl2 = array ( 'table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">' );
			$this->table->set_template($tmpl2); 

			foreach($notes_approval as $notesapp) {
				$this->table->add_row("<blockquote class='message' style='font-size: inherit;'>".$notesapp->notes_approval." </blockquote><i class='fa fa-calendar'></i> ". $notesapp->tgl_approve." &nbsp;<i class='fa fa-user'></i> ".$notesapp->group_name);				
			}
			$this->data['table_notes_approval'] = $this->table->generate();	
			$this->table->clear();
		} else {
			$this->data['table_notes_approval'] = "-";
		}

		$body_surat = $this->M_disposisi->show_data($id, $id_surat, $id_group);

		// display body surat
		$this->data['tipe'] = $body_surat->tipe_surat;
		$this->data['subyek_surat'] = $body_surat->subyek_surat;
		$this->data['content_surat'] = $body_surat->content;
		$this->data['tgl_create'] = $body_surat->tgl_create;
		$this->data['tgl_disposisi'] = $body_surat->tgl_disposisi;
		$this->data['pesan_disposisi'] = $body_surat->pesan;
		$this->data['generate_number'] = $body_surat->generate_number;
		$this->data['npp_disposisi'] = $body_surat->npp_disposisi;
		$this->data['nama_disposisi'] = $body_surat->nama;
		$this->data['biro']	= $body_surat->biro;
		$this->data['dukungan']	= $body_surat->dukungan;

		$tujuan_surat = $this->M_inbox->tujuan($id_surat, 1);
		$count_tujuan = $this->M_inbox->count_tujuan($id_surat, 1);

		if ($count_tujuan->nilai > 0) {
			$tmpl2 = array('table_open'  => '<table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">');
			$this->table->set_template($tmpl2);

			foreach ($tujuan_surat as $row_to) {
				$this->table->add_row($row_to->div_name);
			}
			$this->data['table_get_to'] = $this->table->generate();
			$this->table->clear();
		} else {
			$this->data['table_get_to'] = "-";
		}


		$tujuan_surat = $this->M_inbox->tujuan($id_surat, 2);


		// display attachment surat
		$attachment_surat = $this->M_inbox->attachment($id_surat);
		$count_attachment = $this->M_inbox->count_attachment($id_surat);
		if ($count_attachment->nilai > 0) {
			$tmpl2 = array('table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">');
			$this->table->set_template($tmpl2);
			foreach ($attachment_surat as $row) {
				$atts = array('target' => '_blank');
				$this->table->add_row(anchor(base_url() . 'uploads/' . $row->nama_file, $row->nama_file, $atts));
			}
			$this->data['attach'] = $this->table->generate();
			$this->table->clear();
		} else {
			$this->data['attach'] = "-";
		}
		$this->data['from'] = $div_name;

		// tampilkan tracking disposisi
		$tmpl2 = array('table_open'  => '<table class="table table-striped borderless">');
		$this->table->set_template($tmpl2);
		$tracking_users = $this->M_disposisi->tracking_disposisi($id_surat, $id_divisi, $id_group);

		foreach ($tracking_users as $track) {
			// query untuk ambil komentar
			$komentar = null;
			if (!empty($track->id)) {
				$comment = $this->M_disposisi->get_comment_user($track->id, $id_surat);
				$komentar = $comment->comment;
			}

			$this->table->add_row($track->TUJUAN_DISPOSISI . '<p><mark>'.$komentar.'</mark></p>');
		}
		$this->data['tracking'] = $this->table->generate();
		$this->table->clear();

		// tracking pesan disposisi dengan flag 8
		$parents = get_parents($id_group);
		$new_arr = explode(',', $parents, -1);
		$unique = array_unique($new_arr);
		$ass = array_diff_key($new_arr, $unique);

		$asse = "'" . implode("','", array_unique($ass)) . "'";
		$asseton = str_replace(' ', '', $asse);

		if ($group_level == 1) {
			$tracking_message = $this->M_disposisi->tracking_pesan_disposisi_gm($id_surat, $id_divisi);
			$comment = $this->M_disposisi->get_upd_status1();
		} else { // group level = 2
			$tracking_message = $this->M_disposisi->tracking_pesan_disposisi($id_surat, $id_divisi);
			$comment = $this->M_disposisi->get_upd_status1();
		}
		$this->data['m_tracking'] =  $tracking_message;
		foreach ($tracking_message as $track_message) {
			$this->table->add_row($track_message->pesan . "<p>" . $track_message->tgl_disposisi . " - " . $track_message->nama . "</p>");
		}
		$this->data['tracking_message'] = $this->table->generate();
		$this->table->clear();

		if (!empty($comment)) {
			foreach ($comment as $comment_msg) {
				$this->table->add_row($comment_msg->comment . "<p>" . $comment_msg->npp . " - " . $comment_msg->nama . "</p>");
			}
			$this->data['comment_msg'] = $this->table->generate();
			$this->table->clear();
		} else {
			$this->data['comment_msg'] = " - ";
			$this->table->clear();
		}

		$this->data['content'] = "templates/show_disposisi";
		$this->load->view($this->template, $this->data);
	}
}
