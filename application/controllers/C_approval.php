<?php 

class C_Approval extends Frontend_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('M_approval');
		$this->load->model('M_main');
		$this->load->model('M_surat');
	}

	var $template = "web/template";
	var $limit = 15;
    public function index(){
        $this->data['title'] = 'Approval';
        $this->data['content'] = 'templates/list';
        $this->data['table_width'] = '';
        $this->data['table_header'] = array('No', 'TANGGAL', 'SUBJECT', 'PENGIRIM');
        $this->data['ajax_target'] = 'c_approval/ajax_target';
		$this->data['useDatatable'] = TRUE;
        $this->data['filter'] = array();
        $this->load->view($this->template,$this->data);
    }
    public function ajax_target(){
        $id_group = $this->nativesession->get('id_group');
        $id_div   = $this->nativesession->get('id_div');

        $draw = $_GET['draw'];
        $offset = $_GET['start'];
        $length = $_GET['length'];

        $param_search = array(
            'ppof_tbl_flow.id_group' => $id_group,
            'ppof_tbl_flow.id_divisi' => $id_div
        );
        $recordsTotal = $this->M_approval->count_data_new($param_search);
        $recordsFiltered = $this->M_approval->count_data_new($param_search);
        $dataReal = $this->M_approval->data_approve_new($param_search, $offset, $this->limit);

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => array()
        );
        $i = 1 + $offset;
        foreach($dataReal as $row){
            $id  = base64_encode($this->encrypt->encode($row->id, 'Test@123'));
            $id_surat = base64_encode($this->encrypt->encode($row->id_surat, 'Test@123'));

            $data['data'][] = array(
                $i, format_datetime($row->tgl_create), anchor('c_approval/show/'.$id.'/'.$id_surat,limit_to_numwords($row->subyek_surat, 5),'style="font-weight: bold; text-decoration: underline;"'), $row->group_name
            );
            $i++;
        }
        die(json_encode($data));
    }

	public function show($id, $id_surat){
		$id_group = $this->nativesession->get('id_group');
		$id_div = $this->nativesession->get('id_div');
		$div_name = $this->nativesession->get('div_name');
		$group_suppervisor = $this->nativesession->get('group_suppervisor');
		$id = $this->encrypt->decode(base64_decode($id), 'Test@123');
		$id_surat = $this->encrypt->decode(base64_decode($id_surat), 'Test@123');
		$body_surat = $this->M_approval->get_data($id_group, $id_div, $id);

		if($body_surat->subyek_surat == null){
			redirect('c_approval/index'); // doesnt have permission
		}
		
		$check_grp_level = $this->M_surat->check_level($group_suppervisor);
		$supervisor_level = $check_grp_level->group_level;
		
		$hide_dd = "inline";
		if($supervisor_level == '1') { // hide dropdown
		    $hide_dd = "none";
		}
		
		$this->data['display_dd'] = $hide_dd;

		$notes_approval = $this->M_inbox->get_comment_approval($id_surat);

		if(!empty($notes_approval)) {
			$tmpl2 = array ( 'table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">' );
			$this->table->set_template($tmpl2); 

			foreach($notes_approval as $notesapp) {
				$this->table->add_row("<blockquote class='message' style='font-size: inherit;'>".$notesapp->notes_approval." </blockquote><i class='fa fa-calendar'></i> ". $notesapp->tgl_approve." &nbsp;<i class='fa fa-user'></i> ".$notesapp->group_name);				
			}
			$this->data['table_notes_approval'] = $this->table->generate();	
			$this->table->clear();
		} else {
			$this->data['table_notes_approval'] = "-";
		}
		
		// display body surat
		$this->data['tipe'] = $body_surat->tipe_surat;
		$this->data['subyek_surat'] = $body_surat->subyek_surat;
		$this->data['content_surat'] = $body_surat->content;
		$this->data['tgl_create'] = $body_surat->tgl_create;
		$this->data['biro'] = isset($body_surat->biro) ? $body_surat->biro : ' - ';
		$this->data['dukungan'] = $body_surat->dukungan;
		$this->data['tglawal'] = $body_surat->tglawal;
		$this->data['tglakhir'] = $body_surat->tglakhir;
		$this->data['waktuawal'] = $body_surat->waktuawal;
		$this->data['waktuakhir'] = $body_surat->waktuakhir;

		$this->data['id_surat']			= $id_surat;
		$this->data['nomor_surat']		= $body_surat->generate_number;
		
		// display tujuan surat dan cc
		$tujuan_surat = $this->M_main->tujuan($id_surat, 1);
		$count_tujuan = $this->M_main->count_tujuan($id_surat, 1);

		if($count_tujuan->nilai > 0){
			$tmpl2 = array ( 'table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">' );
			$this->table->set_template($tmpl2); 
			
			foreach($tujuan_surat as $row_to) {
				$this->table->add_row($row_to->div_name);
			}
			$this->data['table_get_to'] = $this->table->generate();	
			$this->table->clear();

		} else {
			$this->data['table_get_to'] = "-";
		}

		
		$tujuan_surat = $this->M_main->tujuan($id_surat, 2);

		// display attachment surat
		$attachment_surat = $this->M_main->attachment($id_surat);
		$count_attachment = $this->M_main->count_attachment($id_surat);

		if($count_attachment->nilai > 0){

			$tmpl2 = array ( 'table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">' );
			$this->table->set_template($tmpl2); 
			
			foreach($attachment_surat as $row) {
				$atts = array('target' => '_blank');
				$this->table->add_row(anchor(base_url().'uploads/'.$row->nama_file, $row->nama_file, $atts));
			}
			$this->data['attach'] = $this->table->generate();	
			$this->table->clear();

		} else {
			$this->data['attach'] = "-";
		}

		$this->data['from'] = $div_name;
		$this->data['nomor_surat'] = " -";

		$this->data['content'] = "templates/show_approval";
		$this->load->view($this->template, $this->data);
	}

	public function aksi($id, $id_surat){
		$id = $this->encrypt->decode(base64_decode($id), 'Test@123');
		$a = $_POST['submit_val'];
		$somenotes = $this->input->post('somenotes');
		if($a == '1') { // approve
			$this->approve($id, $id_surat, $somenotes);
		} else { // tolak
			$this->tolak($id, $id_surat, $somenotes);
		}
	}
	
	public function approve($id, $id_surat, $somenotes){
		// $id = $this->encrypt->decode(base64_decode($id), 'Test@123');
		$id_group = $this->nativesession->get('id_group');
		$id_div  = $this->nativesession->get('id_div');
		$id_employee = $this->nativesession->get('id_employee');
		$group_suppervisor 	= $this->nativesession->get('group_suppervisor');
		$tanggal = date('Y-m-d H:i:s');

		$get_details = $this->M_approval->get_details($id, $id_surat);
		$get_track = $get_details->id_approval;

		$this->M_approval->update_approve($id_group, $id_div, $id_surat, $tanggal, $id, $get_track, $somenotes); // update dengan status 7 menandakan sudah approve. id_group, id_surat param update

		$biro = $this->input->post('check_list');
        if(empty($biro)) {
			// add error belum isi checkbox
        	// redirect('c_approval/index');
        }
		
		$biro = implode(", ", $biro);
		// get detail
		$detail_surat = $this->M_approval->get_detail_surat($id_surat);
		$subyek_surat = $detail_surat->subyek_surat;
		$content = $detail_surat->content;
		$dukungan = $detail_surat->dukungan;
		$waktuawal = $detail_surat->waktuawal;
		$waktuakhir = $detail_surat->waktuakhir;
		$tglawal = $detail_surat->tglawal;
		$tglakhir = $detail_surat->tglakhir;
		
		if($group_suppervisor != null){
			// insert ulang untuk approval selanjutnya. get and insert
			$sent_status = '2';
			$group_tracking = $group_suppervisor;

			$get_appr = $this->M_approval->get_flow_approve($id_group, $id_div, $id_surat);
			$data_reinsert = array(
				'id_surat' 		=> $id_surat,
				'id_status' 	=> 2,
				'id_divisi' 	=> $id_div,
				'id_group' 		=> $group_suppervisor,
				'id_pengirim' 	=> $get_appr->id_pengirim,
				'npp_pengirim'	=> $get_appr->npp_pengirim,
				'id_approval'	=> $id_group,
				'tgl_approve'	=> $tanggal,
			);
			$this->M_main->insert_ar(TBL_FLOW, $data_reinsert);
        			
		} 
		else { // get tujuan, reinsert based on tujuan surat as id_tujuan

			$this->M_approval->update_biro($id_surat, $biro); // update biro

			$sent_status = '5';
			$group_tracking = $id_group;

			$get_appr = $this->M_approval->get_flow_approve($id_group, $id_div, $id_surat);
			$tujuan_surat = $this->M_main->get_tujuan($id_surat); // loop berdasarkan table ppof_tbl_tujuan
			$flow_terkirim = array(
				'id_surat' 		=> $id_surat,
	        	'id_status' 	=> 5,
	        	'id_divisi' 	=> $id_div,
	        	'id_group' 		=> $id_group,
	        	'id_pengirim' 	=> $id_employee,
	        	'tgl_approve'	=> $tanggal,
				'npp_approval'	=> $id_group,
				'npp_pengirim'  => $get_appr->npp_pengirim,
				'id_approval'	=> $id_group,
			    'sent_status'   => $sent_status
			);
			tujuan_terkirim($tujuan_surat, $flow_terkirim);
			
			$tglakhir_cal = $tglakhir.' 23:59:59';
			$data_calendar = array(
				'title' => $subyek_surat,
				'description' => $content,
				'start_date' => $tglawal,
				'end_date' => $tglakhir_cal,
				'waktuawal' => $waktuawal,
				'waktuakhir' => $waktuakhir,
				'biro' => $biro,
				'dukungan' => $dukungan,
				'id_surat' => $id_surat
			);
			$this->m_main->insert_ar('calendar', $data_calendar);

		}
		// update sent item 
		$upd_sent_record = array(
			'sent_status' => $sent_status,
			'group_tracking' => $group_tracking,
		);
		$this->M_approval->upd_sent_record($id_surat, $id_div, $upd_sent_record); // update record sent item
		$this->session->set_flashdata('msg_success', 'Event berhasil disetujui');
		redirect('c_approval/index'); // redirect to main page
	}

	public function tolak($id, $id_surat, $comment) {
		// $id = $this->encrypt->decode(base64_decode($id), 'Test@123'); // decode
		$id_group 		= $this->nativesession->get('id_group');
		$id_div			= $this->nativesession->get('id_div');
		$tanggal 		= date('Y-m-d H:i:s');
		// $comment		= $this->input->post('pesan_tolak');
		// get id_pengirim
		$get_data = $this->M_approval->get_id_pengirim($id_surat, $id_group, $id_div, 2);
		$id_pengirim = $get_data->id_pengirim;
		// get pengirim awal 
		$get_data = $this->M_approval->get_sent_pengirim($id_surat, $id_div, 3);
		$id_group_pengirim = $get_data->id_group;
		$group_level = $get_data->group_level;

		if($group_level == 2){
			$tujuan = $id_group_pengirim;
		} else {
			$tujuan = $id_pengirim;
		}

		$this->M_approval->upd_sent_tolak($id, $id_surat, $id_group);
		$tolak_surat = $this->M_approval->tolak($id_surat, $comment, $tanggal, $id_group, $id_pengirim, $id_group_pengirim, $tujuan, $id_div);

		$this->session->set_flashdata('msg_success', 'Event berhasil ditolak');
		redirect('c_approval/index');
	}
    public function done(){
        $this->data['title'] = 'Sudah Approve';
        $this->data['content'] = 'templates/list';
        $this->data['table_width'] = '';
        $this->data['table_header'] = array('No', 'TANGGAL', 'SUBJECT', 'PENGIRIM');
        $this->data['ajax_target'] = 'c_approval/ajax_target_done';
		$this->data['useDatatable'] = TRUE;
        $this->data['filter'] = array(
            array('column_number' => 2,'filter_type' => '\'text\'','filter_delay' => 500)
        );
        $this->load->view($this->template,$this->data);
    }
    public function ajax_target_done(){
        $id_group = $this->nativesession->get('id_group');
        $id_div   = $this->nativesession->get('id_div');

        $draw = $_GET['draw'];
        $offset = $_GET['start'];
        $length = $_GET['length'];

        $param_search = array(
            'ppof_tbl_flow.id_group' => $id_group,
            'ppof_tbl_flow.id_divisi' => $id_div
        );
        if(($_GET['columns'][2]['search']['value']!='')) $param_search['subyek_surat'] = $_GET['columns'][2]['search']['value'];
        $recordsTotal = $this->M_approval->done_count_data_new();
        $recordsFiltered = $this->M_approval->done_count_data_new($param_search);
        $dataReal = $this->M_approval->done_approve_new($param_search, $offset, $this->limit);

        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => array()
        );
        $i = 1 + $offset;
        foreach($dataReal as $row){
            $url_val  = base64_encode($this->encrypt->encode($row->id, 'Test@123'));
            $url_val2 = base64_encode($this->encrypt->encode($row->id_surat, 'Test@123'));

            $data['data'][] = array(
                $i, format_datetime($row->tgl_approve), anchor('c_approval/show_done/'.$url_val.'/'.$url_val2, limit_to_numwords($row->subyek_surat, 5),'style="font-weight: bold; text-decoration: underline;"'), $row->group_name
            );
            $i++;
        }
        die(json_encode($data));
    }
	// public function done_old(){
    //     $this->data['pagination'] = '';
	// 	$id_group = $this->nativesession->get('id_group');
	// 	$id_div   = $this->nativesession->get('id_div');

	// 	$uri_segment = 3;

	// 	if($this->uri->segment($uri_segment) == NULL){
	// 		$val_segment = 1;
	// 	} else {
	// 		$val_segment = $this->uri->segment($uri_segment);
	// 	}

	// 	$offset = $this->uri->segment($uri_segment);

	// 	/*echo $id_group;
	// 	echo "<br>";
	// 	echo $id_div;*/

	// 	$data_approve 	= $this->M_approval->done_approve($this->limit, $offset, $id_group, $id_div);
	// 	$num_rows 		= $this->M_approval->done_count_data($id_group, $id_div);

	// 	if($num_rows > 0){
	// 		$config['base_url'] 	= site_url('c_approval/done/');
	// 		$config['total_rows'] 	= $num_rows;
	// 		$config['per_page'] 	= $this->limit;
	// 		$config['uri_segment'] 	= $uri_segment;
	// 		$this->pagination->initialize($config);
	// 		$this->data['pagination'] 	= $this->pagination->create_links();
	// 		$tmpl = array ( 'table_open'    => '<table border="0" cellpadding="0" cellspacing="0" class="table table-striped table-condensed">',
	// 				  'row_alt_start'  => '<tr class="zebra">', 'row_alt_end'    => '</tr>' );
	// 		$this->table->set_template($tmpl);
	// 		$this->table->set_empty("&nbsp;");
	// 		$this->table->set_heading('NO', 'TANGGAL', 'SUBJECT', 'PENGIRIM');
	// 		$i = 0 + $offset;
			
	// 		foreach($data_approve as $row){

	// 			$url_val  = base64_encode($this->encrypt->encode($row->id, 'Test@123'));
	// 			$url_val2 = base64_encode($this->encrypt->encode($row->id_surat, 'Test@123'));

	// 			$this->table->add_row(++$i, format_datetime($row->tgl_approve), anchor('c_approval/show_done/'.$url_val.'/'.$url_val2, $row->subyek_surat), $row->group_name);
	// 		}
	// 		$this->data['table'] = $this->table->generate();
	// 		$this->table->clear();

	// 	} else {
	// 		$this->data['table'] = "Tidak Ada Data.";
	// 		$this->session->set_flashdata("msg_blm_approve", "Tidak Ada Data Approval");
	// 	}
		
	// 	$this->data['content'] = "templates/sudah_approve";
	// 	$this->load->view($this->template, $this->data);

	// }
	
	public function show_done($id, $id_surat){
		$id_group 	= $this->nativesession->get('id_group');
		$id_div 	= $this->nativesession->get('id_div');
		$div_name	= $this->nativesession->get('div_name');

		$id 		= $this->encrypt->decode(base64_decode($id), 'Test@123');
		$id_surat 	= $this->encrypt->decode(base64_decode($id_surat), 'Test@123');
		
		$body_surat 	= $this->M_approval->get_data_done($id, $id_surat, $id_group, $id_div);

		$notes_approval = $this->M_inbox->get_comment_approval($id_surat);
		if(isset($notes_approval)) {
			$tmpl2 = array ( 'table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">' );
			$this->table->set_template($tmpl2); 

			foreach($notes_approval as $notesapp) {
				$this->table->add_row("<blockquote class='message' style='font-size: inherit;'>".$notesapp->notes_approval." </blockquote><i class='fa fa-calendar'></i> ". $notesapp->tgl_approve." &nbsp;<i class='fa fa-user'></i> ".$notesapp->group_name);
			}
			$this->data['table_notes_approval'] = $this->table->generate();	
			$this->table->clear();
		} else {
			$this->data['table_notes_approval'] = "-";
		}

		if($body_surat->subyek_surat == null){
			redirect('c_approval/index'); // doesnt have permission
		}

		// display body surat
		$this->data['tipe'] = $body_surat->tipe_surat;
		$this->data['subyek_surat'] = $body_surat->subyek_surat;
		$this->data['content_surat'] = $body_surat->content;
		$this->data['tgl_create'] = $body_surat->tgl_create;
		$this->data['no_surat']	= $body_surat->generate_number;
		$this->data['biro'] = $body_surat->biro;
		$this->data['dukungan']	= $body_surat->dukungan;
		$this->data['id_surat']	= $id_surat;
		$this->data['tglawal'] = $body_surat->tglawal;
		$this->data['tglakhir'] = $body_surat->tglakhir;
		$this->data['waktuawal'] = $body_surat->waktuawal;
		$this->data['waktuakhir'] = $body_surat->waktuakhir;
		
		// display tujuan surat dan cc
		$tujuan_surat = $this->M_main->tujuan($id_surat, 1);
		$count_tujuan = $this->M_main->count_tujuan($id_surat, 1);

		if($count_tujuan->nilai > 0){
			$tmpl2 = array ( 'table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">' );
			$this->table->set_template($tmpl2); 
			
			foreach($tujuan_surat as $row_to) {
				$this->table->add_row($row_to->div_name);
			}
			$this->data['table_get_to'] = $this->table->generate();	
			$this->table->clear();

		} else {
			$this->data['table_get_to'] = "-";
		}

		
		$tujuan_surat = $this->M_main->tujuan($id_surat, 2);

		// display attachment surat
		$attachment_surat = $this->M_main->attachment($id_surat);
		$count_attachment = $this->M_main->count_attachment($id_surat);

		if($count_attachment->nilai > 0){

			$tmpl2 = array ( 'table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">' );
			$this->table->set_template($tmpl2); 
			
			foreach($attachment_surat as $row) {
				$atts = array('target' => '_blank');
				$this->table->add_row(anchor(base_url().'uploads/'.$row->nama_file, $row->nama_file, $atts));
			}
			$this->data['attach'] = $this->table->generate();	
			$this->table->clear();

		} else {
			$this->data['attach'] = "-";
		}

		////
		$this->data['from'] = $div_name;
		$this->data['nomor_surat'] = " -";

		$this->data['content'] = "templates/show_done_approve";
		$this->load->view($this->template, $this->data);
	}

}