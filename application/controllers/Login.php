<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function construct__(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('nativesession');
	}

	public function index(){
		// $this->load->library('SimpleLoginSecure'); // load login library
		// $this->simpleloginsecure->create('dummy', '123456'); // example to create new user
		
		$this->load->library('nativesession');
		$uid_session 	= $this->nativesession->get('username');
		if($uid_session != null){
			redirect('pages/index');
		}

		$this->load->view('login');
	}

	function validate(){

		$this->load->library('SimpleLoginSecure');
		$this->load->helper('ppof_helper');
		$this->load->model('M_inbox');
		$this->load->model('M_employee');
		
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if($this->simpleloginsecure->login($username, $password) == TRUE){
			
			set_session($username); // set all session
			
			$get_successval = $this->M_inbox->get_users($username);
			$success_login = $get_successval->success;
			$success_login += 1;

			$this->M_inbox->login_count($success_login, $username); // update

			redirect('Pages/index');	
		} else {

			// $get_fail = $this->M_inbox->get_users($username);
			// $fail_login = $get_fail->fail;
			// $fail_login += 1;
			// $this->M_inbox->login_fail_count($fail_login, $username); // update

            $this->session->set_flashdata("msg_login", "Login gagal, periksa username dan password Anda.");
			redirect('Login/index');
		}		
	}

	function logout(){
		session_destroy();
		redirect('login/index');
	}

	function user_deactive() {
		$this->session->set_flashdata("msg_login", "User Tidak Aktif");
			redirect('Login/index');
	}
}