<?php 

class C_Pesan extends Frontend_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('m_main');
		$this->load->model('m_surat');
		$this->load->model('M_pesan');
	}

	var $template = 'web/template';

	public function index(){
		$this->data['content'] = "templates/create_pesan";
    	$this->load->view($this->template, $this->data);
	}

	// public function kirim() {
	// 	$tujuan 	= $this->input->post('div_id');
    //     $tujuan 	= array_filter($tujuan);

	// 	print_r($tujuan);
	// }
	
	public function kirim(){
		$id_group = $this->nativesession->get('id_group');
		$group_name = $this->nativesession->get('group_name');
		$group_suppervisor 	= $this->nativesession->get('group_suppervisor');
		$id_div = $this->nativesession->get('id_div');
		$div_name = $this->nativesession->get('div_name');
		$div_identity = $this->nativesession->get('div_identity');
		$group_level = $this->nativesession->get('group_level');
		$username = $this->nativesession->get('username');
		$id_employee = $this->nativesession->get('id_employee');
		$tanggal = date('Y-m-d H:i:s');
		$type = "pesan";

        $tujuan 	= $this->input->post('div_id');
        $tujuan 	= array_filter($tujuan);

        if(empty($tujuan)){ // if empty redirect to index
        	redirect('c_surat/index');
        }

        $data_surat = array(
        	'tipe_surat' 	=> $type,
        	'subyek_surat' 	=> $this->input->post('subject'),
        	'content' 		=> $this->input->post('content'),
        	'tgl_create' 	=> date('Y-m-d H:i:s')	
        );

        $this->m_main->insert_ar(TBL_SURAT, $data_surat); // insert into database content surat
        $lastid = $this->db->insert_id();

        // insert into database
		$this->insert_pesan($tujuan, 1, $lastid);
    	
		$tujuan_surat = $this->m_surat->get_tujuan($lastid);
		$flow_terkirim = array(
			'id_surat' 		=> $lastid,
        	'id_status' 	=> 9,
        	'id_divisi' 	=> $id_div,
        	'id_group' 		=> $id_group,
        	'id_pengirim' 	=> $id_employee,
        	'tgl_approve'	=> $tanggal,
        	'npp_pengirim'	=> $username,
        	'id_approval'	=> $id_group,
		    'sent_status'   => '0'
		);
		tujuan_terkirim_pesan($tujuan_surat, $flow_terkirim);
		
		// $sent_status = '9';
		// $group_suppervisor = $id_group;
  //       $data_flow_sent = array(
  //       	'id_surat' 		=> $lastid,
  //       	'id_status' 	=> 3,
  //       	'id_divisi' 	=> $id_div,
  //       	'id_group' 		=> $id_group,
  //       	'id_pengirim' 	=> $id_employee,
  //       	'npp_pengirim'	=> $username,
  //       	'tgl_approve'	=> $tanggal,
  //       	'group_tracking'=> $group_suppervisor,
  //       	'sent_status'	=> $sent_status
  //       );
  //       $this->m_main->insert_ar(TBL_FLOW, $data_flow_sent);
        $this->session->set_flashdata('success_message', 'Pesan berhasil dikirim');
        redirect('c_pesan/index');        
	}

	public function insert_pesan($values, $flag, $lastid) {

		$arrValues = array();
	    foreach ($values as $row) {
	        $arrValues[] = '("'.$row.'", '.$flag.' ,'.$lastid.')';
	    }
	    $sql = 'INSERT INTO '.TBL_TUJUAN.' (id_div, flag_tujuan, id_surat) VALUES '.implode(',',$arrValues).';';
	    $this->m_main->insert($sql);

	}
}