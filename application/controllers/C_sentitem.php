<?php

class C_Sentitem extends Frontend_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_sentitem');
		$this->load->model('M_main');
		$this->load->library('table');
	}

	var $template = "web/template";
	var $limit = 15;

	public function index()
	{
		$this->data['title'] = 'Sent Item';
		$this->data['content'] = 'templates/list';
		$this->data['table_width'] = '';
		$this->data['table_header'] = array('No', 'TANGGAL', 'SUBJECT', 'STATUS');
		$this->data['ajax_target'] = 'c_sentitem/ajax_target';
		$this->data['filter'] = array(
			array('column_number' => 2, 'filter_type' => '\'text\'', 'filter_delay' => 500)
		);
		$this->data['customJs'] = 'sentitem.js';
		$this->data['useDatatable'] = TRUE;
		$this->load->view($this->template, $this->data);
	}

	public function ajax_target()
	{
		$id_pengirim  	= $this->nativesession->get('id_employee');
		$id_div			= $this->nativesession->get('id_div');

		$draw = $_GET['draw'];
		$offset = $_GET['start'];
		$length = $_GET['length'];
		$param_search = array('id_pengirim' => $id_pengirim, 'id_divisi' => $id_div);

		if (($_GET['columns'][2]['search']['value'] != '')) $param_search['subyek_surat'] = $_GET['columns'][2]['search']['value'];

		$recordsTotal = $this->M_sentitem->count_data_new();
		$recordsFiltered = $this->M_sentitem->count_data_new($param_search);
		$dataReal = $this->M_sentitem->get_data_new($param_search, $offset, $this->limit);

		$data = array(
			'draw' => $draw,
			'recordsTotal' => $recordsTotal,
			'recordsFiltered' => $recordsFiltered,
			'data' => array()
		);
		$i = 1 + $offset;
		foreach ($dataReal as $row) {
			$url_val  = base64_encode($this->encrypt->encode($row->id, 'Test@123'));
			$url_val2 = base64_encode($this->encrypt->encode($row->id_surat, 'Test@123'));

			$data['data'][] = array(
				$i, format_datetime($row->tgl_create), 
				anchor('c_sentitem/show/' . $url_val . '/' . $url_val2, limit_to_numwords($row->subyek_surat, 5), 'style="font-weight: bold; text-decoration: underline;"'), 
				$row->STATUS_SURAT
			);
			$i++;
		}
		die(json_encode($data));
	}


	public function show($id, $id_surat)
	{
		$username		= $this->nativesession->get('username');
		$id_div			= $this->nativesession->get('id_div');
		$id_pengirim	= $this->nativesession->get('id_employee');
		$div_name 		= $this->nativesession->get('div_name');

		$id 		= $this->encrypt->decode(base64_decode($id), 'Test@123');
		$id_surat 	= $this->encrypt->decode(base64_decode($id_surat), 'Test@123');

		$body_surat	= $this->M_sentitem->show($id, $id_surat, $id_pengirim, $id_div);
		$notes_approval = $this->M_inbox->get_comment_approval($id_surat);


		if ($body_surat->subyek_surat == null) {
			redirect('c_sentitem/index'); // doesnt have permission
		}

		if(!empty($notes_approval)) {
			$tmpl2 = array ( 'table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">' );
			$this->table->set_template($tmpl2); 

			foreach($notes_approval as $notesapp) {
				$this->table->add_row("<blockquote class='message' style='font-size: inherit;'>".$notesapp->notes_approval." </blockquote><i class='fa fa-calendar'></i> ". $notesapp->tgl_approve." &nbsp;<i class='fa fa-user'></i> ".$notesapp->group_name);				
			}
			$this->data['table_notes_approval'] = $this->table->generate();	
			$this->table->clear();
		} else {
			$this->data['table_notes_approval'] = "-";
		}

		// display body surat
		$this->data['tipe'] = $body_surat->tipe_surat;
		$this->data['subyek_surat'] = $body_surat->subyek_surat;
		$this->data['content_surat'] = $body_surat->content;
		$this->data['tgl_create'] = $body_surat->tgl_create;
		$this->data['biro'] = isset($body_surat->biro) ? $body_surat->biro : ' - ';
		$this->data['dukungan'] = $body_surat->dukungan;
		$this->data['nomor_surat'] = isset($body_surat->generate_number) ? $body_surat->generate_number : ' - ';
		$this->data['status_surat'] = $body_surat->status_surat;
		$this->data['waktuawal'] = $body_surat->waktuawal;
		$this->data['waktuakhir'] = $body_surat->waktuakhir;
		$this->data['tglawal'] = $body_surat->tglawal;
		$this->data['tglakhir']	= $body_surat->tglakhir;

		// display tujuan surat dan cc
		$tujuan_surat = $this->M_sentitem->tujuan($id_surat, 1);
		$count_tujuan = $this->M_sentitem->count_tujuan($id_surat, 1);

		if ($count_tujuan->nilai > 0) {
			$tmpl2 = array('table_open'  => '<table border="0" cellpadding="0" cellspacing="0">');
			$this->table->set_template($tmpl2);

			foreach ($tujuan_surat as $row_to) {
				$this->table->add_row($row_to->div_name);
			}
			$this->data['table_get_to'] = $this->table->generate();
			$this->table->clear();
		} else {
			$this->data['table_get_to'] = " -";
		}

		$tujuan_surat = $this->M_sentitem->tujuan($id_surat, 2);

		// display attachment surat
		$attachment_surat = $this->M_sentitem->attachment($id_surat);
		$count_attachment = $this->M_sentitem->count_attachment($id_surat);

		if ($count_attachment->nilai > 0) {

			$tmpl2 = array('table_open'  => '<table border="0" cellpadding="0" cellspacing="0" class="mytable">');
			$this->table->set_template($tmpl2);

			foreach ($attachment_surat as $row) {
				$atts = array('target' => '_blank');
				$this->table->add_row(anchor(base_url() . 'uploads/' . $row->nama_file, $row->nama_file, $atts));
			}
			$this->data['attach'] = $this->table->generate();
			$this->table->clear();
		} else {
			$this->data['attach'] = "-";
		}

		$this->data['from'] = $div_name;

		$this->data['content'] = "templates/show_sent";
		$this->load->view($this->template, $this->data);
	}
}
