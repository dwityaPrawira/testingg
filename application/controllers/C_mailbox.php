<?php

class C_Mailbox extends Frontend_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_mailbox');
		$this->load->model('M_main');
		$this->load->model('M_surat');

		$this->load->library('table');
	}

	var $template = "web/template";
	var $limit = 15;

	public function index()
	{
		$this->data['title'] = 'Mailbox';
		$this->data['content'] = 'templates/list';
		$this->data['table_width'] = '';
		$this->data['table_header'] = array('No', 'TANGGAL', 'SUBJECT');
		$this->data['ajax_target'] = 'c_mailbox/ajax_target';
		$this->data['useDatatable'] = TRUE;
		$this->data['filter'] = array(
			// array('column_number' => 2,'filter_type' => '\'text\'','filter_delay' => 500),
			// array('column_number' => 3,'filter_type' => '\'text\'','filter_delay' => 500)
		);

		$this->load->view($this->template, $this->data);
	}

	public function ajax_target()
	{
		// $id_tujuan  		= $this->nativesession->get('id_employee');
		$id_div				= $this->nativesession->get('id_div');
		$group_level		= $this->nativesession->get('group_level'); // check level
		$id_group 			= $this->nativesession->get('id_group');
		$group_suppervisor 	= $this->nativesession->get('group_suppervisor'); // get supervisor id
		$npp = $this->nativesession->get('npp');

		$draw = $_GET['draw'];
		$offset = $_GET['start'];
		$length = $_GET['length'];
		$param_search = array();

		// if(($_GET['columns'][2]['search']['value']!='')) $param_search['generate_number'] = $_GET['columns'][2]['search']['value'];
		if (($_GET['columns'][2]['search']['value'] != '')) $param_search['subyek_surat'] = strtolower($_GET['columns'][3]['search']['value']);


		$param_search['id_tujuan'] = $npp;
		$param_search['id_status'] = 9;

		$dataReal = $this->M_mailbox->get_data_pesan($param_search, $offset, $this->limit);
		$recordsFiltered = $this->M_mailbox->count_data_pesan($param_search);
		$recordsTotal = $this->M_mailbox->count_data_pesan($param_search);

		$data = array(
			'draw' => $draw,
			'recordsTotal' => $recordsTotal,
			'recordsFiltered' => $recordsFiltered,
			'data' => array()
		);
		$i = 1 + $offset;

		foreach ($dataReal as $row) {
			$id  = base64_encode($this->encrypt->encode($row->id, 'Test@123'));
			$id_surat = base64_encode($this->encrypt->encode($row->id_surat, 'Test@123'));
			$generate_number = '';

			$data['data'][] = array(
				$i, format_datetime($row->tgl_create), anchor('c_mailbox/show/' . $id . '/' . $id_surat, limit_to_numwords($row->subyek_surat, 5), 'style="font-weight: bold; text-decoration: underline;"')
			);
			$i++;
		}
		die(json_encode($data));
	}


	public function show($id, $id_surat)
	{
		$username 		= $this->nativesession->get('username');
		$id_div			= $this->nativesession->get('id_div');
		$id_pengirim 	= $this->nativesession->get('id_employee');
		$group_level	= $this->nativesession->get('group_level'); // check level 
		$id_employee	= $this->nativesession->get('id_employee');
		$div_name 		= $this->nativesession->get('div_name');
		$id_group 		= $this->nativesession->get('id_group');
		$group_suppervisor = $this->nativesession->get('group_suppervisor');

		// update status flow.
		$id 		= $this->encrypt->decode(base64_decode($id), 'Test@123');
		$id_surat 	= $this->encrypt->decode(base64_decode($id_surat), 'Test@123');
		$this->M_mailbox->update_flow_status($id, $id_surat);

		if ($group_level == 3) { // tidak bisa disposisi
			$this->data['flag_level'] = "3";
		} else if ($group_level == 1) {
			$this->data['flag_level'] = "1";
		} else {
			$this->data['flag_level'] = "2";
		}

		// $this->data['id_surat'] = $id_surat;
		// if($group_level == 1){ // untuk inbox gm
		// 	$id_status = 5;
		// 	$query = $this->M_mailbox->show_gm($id, $id_surat, $id_div, $id_status);
		// } else  {
		// 	$query = $this->M_mailbox->show($id, $id_surat, $id_employee, $id_div, $group_level, $id_group);
		// } 

		$query = $this->M_mailbox->show($id, $id_surat, $id_employee, $id_div, $group_level, $id_group);

		$body_surat = $query;

		if ($body_surat->subyek_surat == null) {
			redirect('c_inbox/index'); // doesnt have permission
		}

		// get id_status surat
		$id_status_surat = !empty($body_surat->id_status) ? $body_surat->id_status : "";

		if ($id_status_surat == '6') {
			$arrCihuy = array();

			$parents = get_parents($id_group);

			$new_arr = explode(',', $parents, -1);
			$unique = array_unique($new_arr);
			$dupes = array_diff_key($new_arr, $unique);
			$test = "'" . implode("','", $dupes) . "'";
			$test = str_replace(' ', '', $test);

			$id_status_disposisi = 8;
			$get_msg_disposisi = $this->M_inbox->get_disposisi($test, $id_status_disposisi, $id_surat, $id_div);

			$arrPesan = array();
			$tmpl = array('table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="table borderless">', 'row_alt_start' => '<tr class="zebra">', 'row_alt_end' => '</tr>');
			$this->table->set_template($tmpl);
			$this->table->set_empty("&nbsp;");

			foreach ($get_msg_disposisi as $row) {
				$this->table->add_row("<blockquote class='message' style='font-size: inherit;'>" . $row->pesan . "</blockquote><i class='fa fa-calendar'></i> " . $row->tgl_disposisi . " &nbsp;<i class='fa fa-user'></i> " . $row->group_name . " - " . $row->nama . "<br>");
			}

			$this->data['pesan_surat'] = $this->table->generate();
			$this->table->clear();
			$this->data['pesan_tolak'] = "";
			//////////////////////// menampilkan tracking surat
			$get_div_kirim = $this->M_main->get_div_send($id_surat);
			$div_kirim = $get_div_kirim->id_divisi;

			$get_tracking = $this->M_main->tracking_surat($div_kirim, $id_surat);
			$tmpl = array('table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="table table-striped borderless">', 'row_alt_start' => '<tr class="zebra">', 'row_alt_end' => '</tr>');
			$this->table->set_template($tmpl);
			$this->table->set_empty("&nbsp;");

			foreach ($get_tracking as $row) {
				$this->table->add_row("<i class='fa fa-user'></i> " . $row->group_name . " : " . $row->JENJANG);
			}

			$this->data['tracking_surat'] = $this->table->generate();
			$this->table->clear();
		} else if ($id_status_surat == '4') {
			$this->data['pesan_tolak'] = $body_surat->pesan;
			$this->data['tracking_surat'] = "";
		} else {
			$this->data['tracking_surat'] = "";
		}

		// update surat sudah terbaca
		$this->M_mailbox->update_read($id_div, $id_surat);

		// display body surat
		$this->data['tipe'] 			= $body_surat->tipe_surat;
		$this->data['subyek_surat'] 	= $body_surat->subyek_surat;
		$this->data['content_surat'] 	= $body_surat->content;
		$this->data['tgl_create'] 		= $body_surat->tgl_create;
		$this->data['id_pengirim']		= $body_surat->id_pengirim;
		$this->data['id_divisi']		= $body_surat->id_divisi;
		$this->data['id_group']			= $body_surat->id_group;
		$this->data['npp_pengirim']		= $body_surat->npp_pengirim;
		$this->data['nomor_surat']		= $body_surat->generate_number;

		$this->data['biro']		= $body_surat->biro;
		$this->data['dukungan']		= $body_surat->dukungan;
		$this->data['tglawal']		= $body_surat->tglawal;
		$this->data['tglakhir']		= $body_surat->tglakhir;
		$this->data['waktuawal']		= $body_surat->waktuawal;
		$this->data['waktuakhir']		= $body_surat->waktuakhir;

		// display tujuan surat dan cc
		$tujuan_surat = $this->M_mailbox->tujuan($id_surat, 1);
		$count_tujuan = $this->M_mailbox->count_tujuan($id_surat, 1);

		if ($count_tujuan->nilai > 0) {
			$tmpl2 = array('table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">');
			$this->table->set_template($tmpl2);

			foreach ($tujuan_surat as $row_to) {
				$this->table->add_row($row_to->div_name);
			}
			$this->data['table_get_to'] = $this->table->generate();
			$this->table->clear();
		} else {
			$this->data['table_get_to'] = "-";
		}

		$tujuan_surat = $this->M_mailbox->tujuan($id_surat, 2);

		$detail_employee = $this->M_mailbox->get_emp_detail($body_surat->npp_pengirim);

		$this->data['from'] = $body_surat->npp_pengirim . ' - ' . $detail_employee->nama . ' - ' . $detail_employee->div_name;

		$this->data['content'] = "templates/show_mailbox";
		$this->load->view($this->template, $this->data);
	}
}
