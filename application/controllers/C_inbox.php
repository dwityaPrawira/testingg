<?php 

class C_Inbox extends Frontend_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_inbox');
		$this->load->model('M_main');
		$this->load->model('M_surat');
		
		$this->load->library('table');
	}	

	var $template = "web/template";
	var $limit = 15;

    public function index(){
        $this->data['title'] = 'Arsip';
        $this->data['content'] = 'templates/list';
        $this->data['table_width'] = '';
        $this->data['table_header'] = array('No', 'TANGGAL', 'SUBJECT');
        $this->data['ajax_target'] = 'C_inbox/ajax_target';		
		$this->data['useDatatable'] = TRUE;
        $this->data['filter'] = array(
            array('column_number' => 2,'filter_type' => '\'text\'','filter_delay' => 500),
            // array('column_number' => 3,'filter_type' => '\'text\'','filter_delay' => 500)
        );
        
        $this->load->view($this->template,$this->data);
    }

    public function ajax_target(){
        $id_tujuan  		= $this->nativesession->get('id_employee');
        $id_div				= $this->nativesession->get('id_div');
        $group_level		= $this->nativesession->get('group_level'); 
        $id_group 			= $this->nativesession->get('id_group');
        $group_suppervisor 	= $this->nativesession->get('group_suppervisor'); 

        $draw = $_GET['draw'];
        $offset = $_GET['start'];
        $length = $_GET['length'];
        $param_search = array();

        // if(($_GET['columns'][2]['search']['value']!='')) $param_search['generate_number'] = $_GET['columns'][2]['search']['value'];
        if(($_GET['columns'][2]['search']['value']!='')) $param_search['subyek_surat'] = strtolower($_GET['columns'][2]['search']['value']);

        //$recordsTotal = $this->M_problem->get_problem_row();
        //$recordsFiltered = $this->M_problem->get_problem_row($param_search);
        //$dataReal = $this->M_problem->get_problem($param_search,$offset,$length);

        if($group_level == 1){ // div head. id_tujuan, id_status
            $param_search['id_tujuan'] = $id_div;
            $param_search['id_status'] = 5;
            $dataReal = $this->M_inbox->get_data_gm_new($param_search, $offset, $this->limit);
            $recordsFiltered = $this->M_inbox->count_data_gm_new($param_search);
			// $recordsTotal = $this->M_inbox->count_data_gm_new($param_search);
            $recordsTotal = $this->M_inbox->count_data_gm2();
        } else if ($group_level == 2){ // tier 2
            $param_search['id_tujuan'] = $id_group;
            $param_search['div_tracking'] = $id_div;
            // $param_search['id_approval'] = $group_suppervisor;

            $dataReal = $this->M_inbox->get_data_two_new($param_search, $offset, $this->limit);
            $recordsFiltered = $this->M_inbox->count_data_two_new($param_search);
            $recordsTotal = $this->M_inbox->count_data_two_new($param_search);
        } else {
            // not decided yet
            $param_search['id_tujuan'] = $id_tujuan;
            $param_search['div_tracking'] = $id_div;
            $param_search['id_approval'] = $group_suppervisor;
            $dataReal = $this->M_inbox->get_data_two_new($param_search, $offset, $this->limit);
            $recordsFiltered = $this->M_inbox->count_data_two_new($param_search);
            $recordsTotal = $this->M_inbox->count_data_two_new();
        }
        $data = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => array()
        );
        $i = 1 + $offset;

        foreach($dataReal as $row){
            // $id  = base64_encode($this->encrypt->encode($row->id, 'Test@123'));
            // $id_surat = base64_encode($this->encrypt->encode($row->id_surat, 'Test@123'));

			$id = $row->id;
			$id_surat = $row->id_surat;

            $data['data'][] = array(
                $i, format_datetime($row->tgl_create), anchor('c_inbox/show/' . $id . '/' . $id_surat,limit_to_numwords($row->subyek_surat, 5),'style="font-weight: bold; text-decoration: underline;"')
            );
            $i++;
        }
        die(json_encode($data));
    }
	
	
	public function show($id, $id_surat){
		$username 		= $this->nativesession->get('username');
		$id_div			= $this->nativesession->get('id_div');
		$id_pengirim 	= $this->nativesession->get('id_employee');
		$group_level	= $this->nativesession->get('group_level'); // check level 
		$id_employee	= $this->nativesession->get('id_employee');
		$div_name 		= $this->nativesession->get('div_name');		
		$id_group 		= $this->nativesession->get('id_group');
		$group_suppervisor = $this->nativesession->get('group_suppervisor');

		$notes_approval = $this->M_inbox->get_comment_approval($id_surat);

		// TODO: cek ada bawahan atau tidak.
		$a = $this->M_inbox->cek_subordinat($id_group);
		if(empty($a)) {
			$this->data['flag_subordinat'] = "1";
		}

		if(!empty($notes_approval)) {
			$tmpl2 = array ( 'table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">' );
			$this->table->set_template($tmpl2); 

			foreach($notes_approval as $notesapp) {
				$this->table->add_row("<blockquote class='message' style='font-size: inherit;'>".$notesapp->notes_approval." </blockquote><i class='fa fa-calendar'></i> ". $notesapp->tgl_approve." &nbsp;<i class='fa fa-user'></i> ".$notesapp->group_name);				
			}
			$this->data['table_notes_approval'] = $this->table->generate();	
			$this->table->clear();
		} else {
			$this->data['table_notes_approval'] = "-";
		}
		
		// update status flow.
		// $id 		= $this->encrypt->decode(base64_decode($id), 'Test@123');
		// $id_surat 	= $this->encrypt->decode(base64_decode($id_surat), 'Test@123');
		$this->M_inbox->update_flow_status($id, $id_surat);
		
		if($group_level == 3){ // tidak bisa disposisi
			$this->data['flag_level'] = "3";
		} else if($group_level == 1) {
			$this->data['flag_level'] = "1";
		} else {
			$this->data['flag_level'] = "2";
		}

		$this->data['id_surat'] = $id_surat;
		if($group_level == 1){ // untuk inbox gm
			$id_status = 5;
			$query = $this->M_inbox->show_gm($id, $id_surat, $id_div, $id_status);
		} else  {
			$query = $this->M_inbox->show($id, $id_surat, $id_employee, $id_div, $group_level, $id_group);
		} 

		$body_surat = $query;

		if($body_surat->subyek_surat == null){
			redirect('c_inbox/index'); // doesnt have permission
		} 

		// get id_status surat
		$id_status_surat = !empty($body_surat->id_status) ? $body_surat->id_status : ""; 

		$this->data['id_status_surat'] = $id_status_surat;

		if($id_status_surat == '6'){
			$arrCihuy = array();
			$parents = get_parents($id_group);
			$new_arr = explode(',',$parents,-1);
			$unique = array_unique($new_arr);
			$dupes = array_diff_key( $new_arr, $unique ); 
			
// 			echo print_r(array_unique($dupes)). ' <<<';
			
			$test = "'".implode("','", $dupes)."'";
			$test = str_replace(' ', '', $test);
			
			$id_status_disposisi = 8;
			$get_msg_disposisi = $this->M_inbox->get_disposisi($test, $id_status_disposisi, $id_surat, $id_div);
						
			$arrPesan = array();
			$tmpl = array ( 'table_open' => '<table border="0" cellpadding="0" cellspacing="0">', 'row_alt_start' => '<tr class="zebra">', 'row_alt_end' => '</tr>' );
			$this->table->set_template($tmpl);
			$this->table->set_empty("&nbsp;");
			
			foreach ($get_msg_disposisi as $row) {
			 	$this->table->add_row("<blockquote class='message' style='font-size: inherit;'>".$row->pesan."</blockquote><i class='fa fa-calendar'></i> ".$row->tgl_disposisi." &nbsp;<i class='fa fa-user'></i> ".$row->group_name." - ". $row->nama ."<br>");
			}
			 
			$this->data['pesan_surat'] = $this->table->generate();
			$this->table->clear();
			$this->data['pesan_tolak'] = "";
			
			//////////////////////// menampilkan tracking surat
			$get_div_kirim = $this->M_main->get_div_send($id_surat);
			$div_kirim = $get_div_kirim->id_divisi;
			$get_tracking = $this->M_main->tracking_surat($div_kirim, $id_surat);
			$tmpl = array ( 'table_open' => '<table border="0" cellpadding="0" cellspacing="0"', 'row_alt_start' => '<tr class="zebra">', 'row_alt_end' => '</tr>' );
			$this->table->set_template($tmpl);
			$this->table->set_empty("&nbsp;");
			
			foreach ($get_tracking as $row) {
			 	$this->table->add_row("<i class='fa fa-user'></i> ".$row->group_name." : ".$row->JENJANG);
			}

			$this->data['tracking_surat'] = $this->table->generate();
			$this->table->clear();
		} else if($id_status_surat == '4'){
			$this->data['pesan_tolak'] = $body_surat->pesan;
			$this->data['tracking_surat'] = "";
		} else {
			$this->data['tracking_surat'] = "";
		}

		// update surat sudah terbaca
		$this->M_inbox->update_read($id_div, $id_surat);

		// display body surat
		$this->data['tipe'] 			= $body_surat->tipe_surat;
		$this->data['subyek_surat'] 	= $body_surat->subyek_surat;
		$this->data['content_surat'] 	= $body_surat->content;
		$this->data['tgl_create'] 		= $body_surat->tgl_create;
		$this->data['id_pengirim']		= $body_surat->id_pengirim;
		$this->data['id_divisi']		= $body_surat->id_divisi;
		$this->data['id_group']			= $body_surat->id_group;
		$this->data['npp_pengirim']		= $body_surat->npp_pengirim;
		$this->data['nomor_surat']		= $body_surat->generate_number;
		$this->data['biro']		= $body_surat->biro;
		$this->data['dukungan']		= $body_surat->dukungan;
		$this->data['tglawal']		= $body_surat->tglawal;
		$this->data['tglakhir']		= $body_surat->tglakhir;
		$this->data['waktuawal']		= $body_surat->waktuawal;
		$this->data['waktuakhir']		= $body_surat->waktuakhir;
				
		if($group_level == '3') {
		    $this->data['comment'] = $body_surat->comment;
		} 
		$tujuan_surat = $this->M_inbox->tujuan($id_surat, 1);
		$count_tujuan = $this->M_inbox->count_tujuan($id_surat, 1);

		if($count_tujuan->nilai > 0){
			$tmpl2 = array ( 'table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">' );
			$this->table->set_template($tmpl2); 
			
			foreach($tujuan_surat as $row_to) {
				$this->table->add_row($row_to->div_name);
			}
			$this->data['table_get_to'] = $this->table->generate();	
			$this->table->clear();

		} else {
			$this->data['table_get_to'] = "-";
		}
		
		$tujuan_surat = $this->M_inbox->tujuan($id_surat, 2);

		// display attachment surat
		$attachment_surat = $this->M_inbox->attachment($id_surat);
		$count_attachment = $this->M_inbox->count_attachment($id_surat);

		if($count_attachment->nilai > 0){

			$tmpl2 = array ( 'table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">' );
			$this->table->set_template($tmpl2); 
			
			foreach($attachment_surat as $row) {
				$atts = array('target' => '_blank');
				$this->table->add_row(anchor(base_url().'uploads/'.$row->nama_file, $row->nama_file, $atts));
			}
			$this->data['attach'] = $this->table->generate();	
			$this->table->clear();

		} else {
			$this->data['attach'] = "-";
		}

        $this->data['option_disposisi'] = array();

		if($group_level == '1') {
			$get_subordinate = $this->M_inbox->get_subordinat_tier2();
		} else {
			$get_subordinate = $this->M_inbox->get_subordinat($id_group);
		}
        
        $id_tujuan_subordinate = null;
        foreach ($get_subordinate as $row){
            if($row->group_level == '2'){
                $id_tujuan_subordinate = $row->id_group;
            } else {
                $id_tujuan_subordinate = $row->id;
            }
            $this->data['option_disposisi'][$id_tujuan_subordinate] = $row->group_name." - ".$row->nama;
        }
        
		$this->data['from'] = $div_name;
		$this->data['content'] = "templates/show_inbox";
		$this->load->view($this->template, $this->data);
	}

	public function disposisi($id){
		$option_disposisi 	= $this->input->post('option_disposisi');
		$option_disposisi 	= array_unique($option_disposisi); // filter duplicate arr value

		$pesan_disposisi 	= $this->input->post('pesan_disposisi');
		$id_surat			= $this->input->post('id_surat');
		$id_pengirim 		= $this->input->post('id_pengirim');
		$id_divisi			= $this->input->post('id_div');
		$id_group_original	= $this->input->post('id_group');
		$id_pengirim_ori	= $this->input->post('id_pengirim');
		$npp_pengirim_ori	= $this->input->post('npp_pengirim');
		
		$id_group 			= $this->nativesession->get('id_group');
		$id_employee 		= $this->nativesession->get('id_employee');
		$group_level		= $this->nativesession->get('group_level');
		$username			= $this->nativesession->get('username');
		$id_div_user		= $this->nativesession->get('id_div');
		
		$npp_disposisi		= $this->session->userdata("user_email");
		
		$tanggal 			= date('Y-m-d H:i:s');
		$status_disposisi 	= 8;
		// update untuk menu disposisi
		$this->M_inbox->update_disposisi($status_disposisi, $id_group, $pesan_disposisi, $tanggal, $id_surat, $id);
		// reinsert data dan get tujuan
		$tujuan_surat = $option_disposisi;
		$flow_terkirim = array(
			'id_surat' 		=> $id_surat,
        	'id_status' 	=> 6,
        	'id_approval' 	=> $id_group,
        	'pesan'		 	=> $pesan_disposisi,
        	'tgl_disposisi'	=> $tanggal,
			'id_pengirim'	=> $id_pengirim,
			'id_group'		=> $id_group_original,
			'div_tracking'	=> $id_div_user,
			'id_pengirim'	=> $id_pengirim,
			'npp_pengirim'	=> $npp_pengirim_ori,
			'npp_disposisi'	=> $npp_disposisi,
		);
		tujuan_disposisi($tujuan_surat, $flow_terkirim);
		$this->session->set_flashdata('msg_success', 'Event berhasil di disposisikan');
		redirect('c_inbox/index');
	}
	
	public function upd_status(){
        $status = $this->input->post('update_status');
        $id_surat = $this->input->post('id_surat');
        $id = $this->input->post('id');
        
        $this->M_inbox->upd_status($status, $id_surat, $id);
        redirect('c_inbox/index');
        
	}
	
}