<?php

class C_Kalender extends Frontend_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_inbox');
		$this->load->model('M_main');
		$this->load->model('M_surat');
		$this->load->model('M_kalender');

		$this->load->library('table');
	}

	var $template = "web/template";
	var $limit = 15;

	public function index_alternate()
	{

		$this->data['title'] = 'Kalender';
		$this->data['content'] = 'templates/list';
		$this->data['table_width'] = '';
		$this->data['table_header'] = array('No', 'TANGGAL', 'NAMA EVENT', 'STATUS');
		$this->data['ajax_target'] = 'c_kalender/ajax_target';
		$this->data['filter'] = array(
			array('column_number' => 2, 'filter_type' => '\'text\'', 'filter_delay' => 500),
			array('column_number' => 3, 'filter_type' => '\'text\'', 'filter_delay' => 500)
		);

		$this->load->view($this->template, $this->data);
	}

	public function ajax_target()
	{
		$id_tujuan  		= $this->nativesession->get('id_employee');
		$id_div				= $this->nativesession->get('id_div');
		$group_level		= $this->nativesession->get('group_level'); // check level
		$id_group 			= $this->nativesession->get('id_group');
		$group_suppervisor 	= $this->nativesession->get('group_suppervisor'); // get supervisor id

		$draw = $_GET['draw'];
		$offset = $_GET['start'];
		$length = $_GET['length'];
		$param_search = array();

		// if(($_GET['columns'][2]['search']['value']!='')) $param_search['generate_number'] = $_GET['columns'][2]['search']['value'];
		if (($_GET['columns'][2]['search']['value'] != '')) $param_search['subyek_surat'] = strtolower($_GET['columns'][3]['search']['value']);

		//$recordsTotal = $this->M_problem->get_problem_row();
		//$recordsFiltered = $this->M_problem->get_problem_row($param_search);
		//$dataReal = $this->M_problem->get_problem($param_search,$offset,$length);


		$param_search['tipe_surat'] = "event";
		// $param_search['id_status'] = 5;
		$dataReal = $this->M_kalender->get_kalender($param_search, $offset, $this->limit);
		$recordsFiltered = $this->M_kalender->count_kalender($param_search);
		$recordsTotal = $this->M_kalender->count_kalender();

		$data = array(
			'draw' => $draw,
			'recordsTotal' => $recordsTotal,
			'recordsFiltered' => $recordsFiltered,
			'data' => array()
		);
		$i = 1 + $offset;

		// print_r($dataReal);

		foreach ($dataReal as $row) {
			$id  = base64_encode($this->encrypt->encode($row->id, 'Test@123'));
			$id_surat = base64_encode($this->encrypt->encode($row->id_surat, 'Test@123'));
			// $generate_number = is_null($row->generate_number)?'':$row->generate_number;


			$date_now = date("Y-m-d");

			if ($row->id_status == '5') {

				if ($date_now < $row->tglawal) {
					$colors = "<div class='p-3 mb-2 bg-info text-white'>Disetujui</div>";
				} else {
					$colors = "<div class='p-3 mb-2 bg-success text-white'>Sudah berjalan</div>"; // sudah berjalan
				}
			} else {
				$colors = "<div class='p-3 mb-2 bg-danger text-white'>Ditolak</div>";
			}

			$data['data'][] = array(
				$i, format_datetime($row->tgl_create), limit_to_numwords($row->subyek_surat, 5), $colors
			);
			$i++;
		}
		die(json_encode($data));
	}
	public function index()
	{
		$this->data['pagination'] = '';

		$id_tujuan  		= $this->nativesession->get('id_employee');
		$id_div				= $this->nativesession->get('id_div');
		$group_level		= $this->nativesession->get('group_level'); // check level 
		$id_group 			= $this->nativesession->get('id_group');
		$group_suppervisor 	= $this->nativesession->get('group_suppervisor'); // get supervisor id

		$uri_segment = 3;
		if ($this->uri->segment($uri_segment) == NULL) {
			$val_segment = 1;
		} else {
			$val_segment = $this->uri->segment($uri_segment);
		}

		$offset = $this->uri->segment($uri_segment);


		$id_status = 5;
		$query = $this->M_kalender->get_data_gm($this->limit, $offset, $id_div, $id_status);
		$count_query = $this->M_kalender->count_data_gm($id_div, $id_status);

		$data_inbox = $query;
		$num_rows = $count_query;

		if ($num_rows > 0) {
			$this->config_pagination['base_url'] 	= site_url('c_kalender/index/');
			$this->config_pagination['total_rows'] 	= $num_rows;
			$this->config_pagination['per_page'] 	= $this->limit;
			$this->config_pagination['uri_segment'] = $uri_segment;

			$this->pagination->initialize($this->config_pagination);
			$this->data['pagination'] 	= $this->pagination->create_links();

			$tmpl = array('table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="table table-striped table-condensed">', 'row_alt_start' => '<tr class="zebra">', 'row_alt_end' => '</tr>');
			$this->table->set_template($tmpl);
			$this->table->set_empty("&nbsp;");
			$this->table->set_heading('No', 'TANGGAL EVENT', 'SUBJECT', 'PENGIRIM', 'STATUS');
			$i = 0 + $offset;

			foreach ($data_inbox as $row) {

				$date_now = date("Y-m-d");

				if ($row->id_status == '5') {
					if ($date_now < $row->tglawal) {
						$colors = "<div class='p-3 mb-2 bg-info'>&nbsp DISETUJUI</div>";
					} else {
						$colors = "<div class='p-3 mb-2 bg-success'>&nbsp BERJALAN</div>"; // sudah berjalan
					}
				} else {
					$colors = "<div class='p-3 mb-2 bg-danger'>&nbsp DITOLAK</div>";
				}

				$id  = base64_encode($this->encrypt->encode($row->id, 'Test@123'));
				$id_surat = base64_encode($this->encrypt->encode($row->id_surat, 'Test@123'));
				$this->table->add_row(++$i,  $row->tglawal . ' s/d ' . $row->tglakhir, '<b>' . limit_to_numwords($row->subyek_surat, 5) . '</b>', $row->group_name, $colors);
			}
			$this->data['table'] = $this->table->generate();
			$this->table->clear();
		} else { // empty data
			$this->data['table'] = "Tidak Ada Data.";
			$this->session->set_flashdata("msg_inbox", "Tidak ada data");
		}

		$this->data['content'] = "templates/kalender";
		$this->load->view($this->template, $this->data);
	}

	public function show($id, $id_surat)
	{
		$username 		= $this->nativesession->get('username');
		$id_div			= $this->nativesession->get('id_div');
		$id_pengirim 	= $this->nativesession->get('id_employee');
		$group_level	= $this->nativesession->get('group_level'); // check level 
		$id_employee	= $this->nativesession->get('id_employee');
		$div_name 		= $this->nativesession->get('div_name');
		$id_group 		= $this->nativesession->get('id_group');
		$group_suppervisor = $this->nativesession->get('group_suppervisor');

		// update status flow.
		$id 		= $this->encrypt->decode(base64_decode($id), 'Test@123');
		$id_surat 	= $this->encrypt->decode(base64_decode($id_surat), 'Test@123');
		$this->M_inbox->update_flow_status($id, $id_surat);

		if ($group_level == 3) { // tidak bisa disposisi
			$this->data['flag_level'] = "3";
		} else if ($group_level == 1) {
			$this->data['flag_level'] = "1";
		} else {
			$this->data['flag_level'] = "2";
		}

		$this->data['id_surat'] = $id_surat;
		if ($group_level == 1) { // untuk inbox gm
			$id_status = 5;
			$query = $this->M_inbox->show_gm($id, $id_surat, $id_div, $id_status);
		} else {
			$query = $this->M_inbox->show($id, $id_surat, $id_employee, $id_div, $group_level, $id_group);
		}

		$body_surat = $query;

		if ($body_surat->subyek_surat == null) {
			redirect('c_inbox/index'); // doesnt have permission
		}

		// get id_status surat
		$id_status_surat = !empty($body_surat->id_status) ? $body_surat->id_status : "";

		if ($id_status_surat == '6') {
			$arrCihuy = array();

			$parents = get_parents($id_group);

			$new_arr = explode(',', $parents, -1);
			$unique = array_unique($new_arr);
			$dupes = array_diff_key($new_arr, $unique);
			$test = "'" . implode("','", $dupes) . "'";
			$test = str_replace(' ', '', $test);

			$id_status_disposisi = 8;
			$get_msg_disposisi = $this->M_inbox->get_disposisi($test, $id_status_disposisi, $id_surat, $id_div);

			$arrPesan = array();
			$tmpl = array('table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="table borderless">', 'row_alt_start' => '<tr class="zebra">', 'row_alt_end' => '</tr>');
			$this->table->set_template($tmpl);
			$this->table->set_empty("&nbsp;");

			foreach ($get_msg_disposisi as $row) {
				$this->table->add_row("<blockquote class='message' style='font-size: inherit;'>" . $row->pesan . "</blockquote><i class='fa fa-calendar'></i> " . $row->tgl_disposisi . " &nbsp;<i class='fa fa-user'></i> " . $row->group_name . " - " . $row->nama . "<br>");
			}

			$this->data['pesan_surat'] = $this->table->generate();
			$this->table->clear();
			$this->data['pesan_tolak'] = "";
			//////////////////////// menampilkan tracking surat
			$get_div_kirim = $this->M_main->get_div_send($id_surat);
			$div_kirim = $get_div_kirim->id_divisi;

			$get_tracking = $this->M_main->tracking_surat($div_kirim, $id_surat);
			$tmpl = array('table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="table table-striped borderless">', 'row_alt_start' => '<tr class="zebra">', 'row_alt_end' => '</tr>');
			$this->table->set_template($tmpl);
			$this->table->set_empty("&nbsp;");

			foreach ($get_tracking as $row) {
				$this->table->add_row("<i class='fa fa-user'></i> " . $row->group_name . " : " . $row->JENJANG);
			}

			$this->data['tracking_surat'] = $this->table->generate();
			$this->table->clear();
		} else if ($id_status_surat == '4') {
			$this->data['pesan_tolak'] = $body_surat->pesan;
			$this->data['tracking_surat'] = "";
		} else {
			$this->data['tracking_surat'] = "";
		}

		// update surat sudah terbaca
		$this->M_inbox->update_read($id_div, $id_surat);

		// display body surat
		$this->data['tipe'] 			= $body_surat->tipe_surat;
		$this->data['subyek_surat'] 	= $body_surat->subyek_surat;
		$this->data['content_surat'] 	= $body_surat->content;
		$this->data['tgl_create'] 		= $body_surat->tgl_create;
		$this->data['id_pengirim']		= $body_surat->id_pengirim;
		$this->data['id_divisi']		= $body_surat->id_divisi;
		$this->data['id_group']			= $body_surat->id_group;
		$this->data['npp_pengirim']		= $body_surat->npp_pengirim;
		$this->data['nomor_surat']		= $body_surat->generate_number;

		$this->data['biro']		= $body_surat->biro;
		$this->data['dukungan']		= $body_surat->dukungan;
		$this->data['tglawal']		= $body_surat->tglawal;
		$this->data['tglakhir']		= $body_surat->tglakhir;
		$this->data['waktuawal']		= $body_surat->waktuawal;
		$this->data['waktuakhir']		= $body_surat->waktuakhir;

		// display tujuan surat dan cc
		$tujuan_surat = $this->M_inbox->tujuan($id_surat, 1);
		$count_tujuan = $this->M_inbox->count_tujuan($id_surat, 1);

		if ($count_tujuan->nilai > 0) {
			$tmpl2 = array('table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">');
			$this->table->set_template($tmpl2);

			foreach ($tujuan_surat as $row_to) {
				$this->table->add_row($row_to->div_name);
			}
			$this->data['table_get_to'] = $this->table->generate();
			$this->table->clear();
		} else {
			$this->data['table_get_to'] = "-";
		}


		$tujuan_surat = $this->M_inbox->tujuan($id_surat, 2);
		$count_tujuan_cc = $this->M_inbox->count_tujuan($id_surat, 2);

		if ($count_tujuan_cc->nilai > 0) {
			$tmpl2 = array('table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">');
			$this->table->set_template($tmpl2);

			foreach ($tujuan_surat as $row_to) {
				$this->table->add_row($row_to->div_name);
			}
			$this->data['table_get_cc'] = $this->table->generate();
			$this->table->clear();
		} else {
			$this->data['table_get_cc'] = "-";
		}

		// display attachment surat
		$attachment_surat = $this->M_inbox->attachment($id_surat);
		$count_attachment = $this->M_inbox->count_attachment($id_surat);

		if ($count_attachment->nilai > 0) {

			$tmpl2 = array('table_open'  => '<table border="0" cellpadding="0" cellspacing="0" style="margin-left:-3px" class="mytable">');
			$this->table->set_template($tmpl2);

			foreach ($attachment_surat as $row) {
				$atts = array('target' => '_blank');
				$this->table->add_row(anchor(base_url() . 'uploads/' . $row->nama_file, $row->nama_file, $atts));
			}
			$this->data['attach'] = $this->table->generate();
			$this->table->clear();
		} else {
			$this->data['attach'] = "-";
		}

		// dropdown bawahan.
		$data_disposisi = $this->M_inbox->get_bawahan($id_group, $id_div);
		$this->data['option_disposisi'][''] = " -- Pilih Tujuan Disposisi -- ";
		foreach ($data_disposisi as $row) {
			$this->data['option_disposisi'][$row->tujuan] = $row->group_name . " - " . $row->nama;
		}

		/*$this->data['option_disposisi'] = array();
		if($group_level == 1) { // berdasarkan group id
			$data_disposisi = $this->M_inbox->get_bawahan_tier1($id_group);
			$this->data['option_disposisi'][''] = " -- Pilih Tujuan Disposisi -- ";
            foreach($data_disposisi as $row){
                $this->data['option_disposisi'][$row->id_group] = $row->group_name." - ".$row->nama;
            }

		} else if($group_level == 2) { // berdasarkan emp id
			$data_disposisi = $this->M_inbox->get_bawahan_tier2($id_group);
			$this->data['option_disposisi'][''] = " -- Pilih Tujuan Disposisi -- ";
            foreach($data_disposisi as $row){
                $this->data['option_disposisi'][$row->id] = $row->group_name." - ".$row->nama;
            }
		}*/

		// variable penunjang like divisi pengirim, surat footer nama divisi, nama pemimpin, tanda tangan, dan jabatan pemimpin
		$this->data['from'] = $div_name;

		$this->data['content'] = "templates/show_inbox";
		$this->load->view($this->template, $this->data);
	}

	public function filter()
	{

		$tglawal = $this->input->post('tglawal');
		$tglakhir = $this->input->post('tglakhir');

		$result = $this->M_kalender->filter($tglawal, $tglakhir);

		$tmpl = array('table_open' => '<table border="0" cellpadding="0" cellspacing="0" class="table table-striped table-condensed">', 'row_alt_start' => '<tr class="zebra">', 'row_alt_end' => '</tr>');
		$this->table->set_template($tmpl);
		$this->table->set_empty("&nbsp;");
		$this->table->set_heading('No', 'TANGGAL EVENT', 'SUBJECT', 'PENGIRIM', 'STATUS');
		$i = 0 + $offset;

		foreach ($result as $row) {

			$date_now = date("Y-m-d");

			if ($row->id_status == '5') {
				if ($date_now < $row->tglawal) {
					$colors = "<div class='p-3 mb-2 bg-info'>&nbsp DISETUJUI</div>";
				} else {
					$colors = "<div class='p-3 mb-2 bg-success'>&nbsp BERJALAN</div>"; // sudah berjalan
				}
			} else {
				$colors = "<div class='p-3 mb-2 bg-danger'>&nbsp DITOLAK</div>";
			}

			$id  = base64_encode($this->encrypt->encode($row->id, 'Test@123'));
			$id_surat = base64_encode($this->encrypt->encode($row->id_surat, 'Test@123'));
			$this->table->add_row(++$i,  $row->tglawal . ' s/d ' . $row->tglakhir, '<b>' . limit_to_numwords($row->subyek_surat, 5) . '</b>', $row->group_name, $colors);
		}
		$this->data['table'] = $this->table->generate();
		$this->table->clear();

		$this->data['content'] = "templates/kalender";
		$this->load->view($this->template, $this->data);
	}
}
