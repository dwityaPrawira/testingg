-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 05, 2018 at 07:32 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paper_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL COMMENT 'Upload Date',
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Unblock, 0=Block'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE `pictures` (
  `pic_id` int(11) NOT NULL,
  `pic_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pic_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `pic_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pictures`
--

INSERT INTO `pictures` (`pic_id`, `pic_title`, `pic_desc`, `pic_file`) VALUES
(1, 'title test', 'desc', 'hires_ok1.png');

-- --------------------------------------------------------

--
-- Table structure for table `ppof_tbl_div`
--

CREATE TABLE `ppof_tbl_div` (
  `id_div` int(11) NOT NULL,
  `div_name` varchar(60) NOT NULL,
  `div_identity` varchar(60) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppof_tbl_div`
--

INSERT INTO `ppof_tbl_div` (`id_div`, `div_name`, `div_identity`, `active`) VALUES
(1, 'UNIT TEKNOLOGI', 'UIT', '1'),
(2, 'HUMAN RESOURCE DIVISION', 'HRD', '1'),
(3, 'TRESURY DIVISION', 'TRS', '1'),
(4, 'ACCOUNTING DIVISON', 'ACD', '1'),
(5, 'MICRO DIVISON', 'MCD', '1'),
(6, 'INTERNAL AUDIT DIVISION', 'IAD', '1');

-- --------------------------------------------------------

--
-- Table structure for table `ppof_tbl_emp`
--

CREATE TABLE `ppof_tbl_emp` (
  `id` int(11) NOT NULL,
  `npp` varchar(60) DEFAULT NULL,
  `nama` varchar(60) DEFAULT NULL,
  `id_group` varchar(60) DEFAULT NULL,
  `signature` varchar(45) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppof_tbl_emp`
--

INSERT INTO `ppof_tbl_emp` (`id`, `npp`, `nama`, `id_group`, `signature`, `photo`) VALUES
(1, '712712', 'Dummy Users', 'G1', NULL, 'user-default.png'),
(2, '71313', 'Dummy two', 'G2', NULL, 'user-default.png'),
(3, '73333', 'Nama Manager 2', 'G3', NULL, 'user-default.png'),
(4, '74444', 'Asisten Manager 2', 'G4', NULL, 'user-default.png'),
(5, '75555', 'Asisten Kedua Manager 2', 'G4', NULL, 'user-default.png'),
(6, '76666', 'Nama ManKel 2', 'G1', NULL, 'user-default.png'),
(7, '77777', 'Asisten ManKel 2', 'G7', NULL, 'user-default.png'),
(8, '88888', 'Bambang', 'G8', NULL, 'user-default.png'),
(9, '81111', 'Adi', 'G9', NULL, 'user-default.png'),
(10, '82222', 'Seno', 'G10', NULL, 'user-default.png'),
(11, '83333', 'Joni', 'G11', NULL, 'user-default.png'),
(12, '84444', 'Romi', 'G12', NULL, 'user-default.png'),
(13, '85555', 'Heri', 'G12', NULL, 'user-default.png');

-- --------------------------------------------------------

--
-- Table structure for table `ppof_tbl_flow`
--

CREATE TABLE `ppof_tbl_flow` (
  `id` int(11) NOT NULL,
  `id_surat` int(11) DEFAULT NULL,
  `id_status` int(11) DEFAULT NULL,
  `id_divisi` int(11) DEFAULT NULL,
  `id_group` varchar(60) DEFAULT NULL,
  `id_tujuan` varchar(60) DEFAULT NULL,
  `id_pengirim` varchar(11) DEFAULT NULL,
  `tgl_approve` datetime DEFAULT NULL,
  `tgl_disposisi` datetime DEFAULT NULL,
  `pesan` text,
  `id_approval` varchar(60) DEFAULT NULL,
  `npp_pengirim` int(11) DEFAULT NULL,
  `flag_read` int(11) DEFAULT '0',
  `npp_disposisi` int(11) DEFAULT NULL,
  `div_tracking` int(11) DEFAULT NULL,
  `group_tracking` varchar(45) DEFAULT NULL,
  `sent_status` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppof_tbl_flow`
--

INSERT INTO `ppof_tbl_flow` (`id`, `id_surat`, `id_status`, `id_divisi`, `id_group`, `id_tujuan`, `id_pengirim`, `tgl_approve`, `tgl_disposisi`, `pesan`, `id_approval`, `npp_pengirim`, `flag_read`, `npp_disposisi`, `div_tracking`, `group_tracking`, `sent_status`) VALUES
(1, 1, 7, 1, 'G1', NULL, '2', '2018-04-29 21:37:27', NULL, NULL, 'G1', 71313, 0, NULL, NULL, 'G2', NULL),
(2, 1, 3, 1, 'G2', NULL, '2', '2018-04-29 21:37:12', NULL, NULL, NULL, 71313, 0, NULL, NULL, 'G1', '5'),
(3, 1, 8, 1, 'G1', '1', '1', '2018-04-29 21:37:27', '2018-04-30 21:38:15', 'adasd', 'G1', 71313, 1, 712712, 1, NULL, NULL),
(4, 1, 5, 1, 'G1', '3', '1', '2018-04-29 21:37:27', NULL, NULL, 'G1', 71313, 0, NULL, NULL, NULL, NULL),
(5, 1, 8, 1, 'G1', '2', '1', '2018-04-29 21:37:27', '2018-04-29 21:38:21', 'disposisi dari pemimpin hcd', 'G8', 71313, 1, NULL, 2, NULL, NULL),
(6, 1, 6, NULL, NULL, 'G9', NULL, NULL, '2018-04-29 21:38:21', 'disposisi dari pemimpin hcd', 'G8', NULL, 1, 88888, 2, NULL, NULL),
(7, 2, 8, 8, 'G8', '2', '8', '2018-04-30 14:11:54', '2018-05-05 10:03:46', '', 'G8', 88888, 1, 88888, 2, NULL, NULL),
(8, 2, 5, 8, 'G8', '3', '8', '2018-04-30 14:11:54', NULL, NULL, 'G8', 88888, 0, NULL, NULL, NULL, NULL),
(9, 2, 8, 8, 'G8', '1', '8', '2018-04-30 14:11:54', '2018-04-30 14:37:59', 'disposisi cek npp disposisi', 'G1', 88888, 1, 712712, 1, NULL, NULL),
(10, 2, 3, 2, 'G8', NULL, '8', '2018-04-30 14:11:54', NULL, NULL, NULL, 88888, 0, NULL, NULL, '', '5'),
(11, 2, 8, NULL, NULL, 'G2', NULL, NULL, '2018-05-01 21:39:52', 'disposisi kembali dari 71313', 'G2', NULL, 1, 71313, 1, NULL, NULL),
(12, 2, 6, NULL, NULL, 'G6', NULL, NULL, '2018-04-30 14:37:59', 'disposisi cek npp disposisi', 'G1', NULL, 0, 712712, 1, NULL, NULL),
(13, 1, 8, NULL, NULL, 'G2', NULL, NULL, '2018-05-01 20:35:29', 'ini saya disposisikan', 'G2', NULL, 1, 71313, 1, NULL, NULL),
(14, 1, 6, NULL, NULL, 'G6', NULL, NULL, '2018-04-30 21:38:15', 'adasd', 'G1', NULL, 1, 712712, 1, NULL, NULL),
(15, 1, 6, NULL, NULL, '3', NULL, NULL, '2018-05-01 20:35:29', 'ini saya disposisikan', 'G2', NULL, 0, 71313, 1, NULL, NULL),
(16, 2, 6, NULL, NULL, '3', NULL, NULL, '2018-05-01 21:39:52', 'disposisi kembali dari 71313', 'G2', NULL, 0, 71313, 1, NULL, NULL),
(17, 3, 8, 8, 'G8', '1', '8', '2018-05-01 21:40:31', '2018-05-01 21:40:50', 'coba disposisi', 'G1', 88888, 1, 712712, 1, NULL, NULL),
(18, 3, 5, 8, 'G8', '3', '8', '2018-05-01 21:40:31', NULL, NULL, 'G8', 88888, 0, NULL, NULL, NULL, NULL),
(19, 3, 3, 2, 'G8', NULL, '8', '2018-05-01 21:40:31', NULL, NULL, NULL, 88888, 0, NULL, NULL, '', '5'),
(20, 3, 6, NULL, NULL, 'G2', NULL, NULL, '2018-05-01 21:40:50', 'coba disposisi', 'G1', NULL, 0, 712712, 1, NULL, NULL),
(21, 3, 8, NULL, NULL, 'G6', NULL, NULL, '2018-05-01 22:28:04', 'test ke asisten mankel', 'G6', NULL, 1, 76666, 1, NULL, NULL),
(22, 4, 8, 8, 'G8', '1', '8', '2018-05-01 21:42:10', '2018-05-01 21:42:26', 'testing', 'G1', 88888, 1, 712712, 1, NULL, NULL),
(23, 4, 3, 2, 'G8', NULL, '8', '2018-05-01 21:42:10', NULL, NULL, NULL, 88888, 0, NULL, NULL, '', '5'),
(24, 4, 8, NULL, NULL, 'G2', NULL, NULL, '2018-05-01 21:45:22', 'disposisi kembali untuk cek tracking surat', 'G2', NULL, 1, 71313, 1, NULL, NULL),
(25, 4, 6, NULL, NULL, 'G6', NULL, NULL, '2018-05-01 21:42:26', 'testing', 'G1', NULL, 1, 712712, 1, NULL, NULL),
(26, 4, 6, NULL, NULL, '3', NULL, NULL, '2018-05-01 21:45:22', 'disposisi kembali untuk cek tracking surat', 'G2', NULL, 0, 71313, 1, NULL, NULL),
(27, 3, 6, NULL, NULL, '7', NULL, NULL, '2018-05-01 22:28:04', 'test ke asisten mankel', 'G6', NULL, 1, 76666, 1, NULL, NULL),
(28, 5, 4, 1, 'G7', '7', '7', '2018-05-02 11:15:50', NULL, 'sdad', 'G6', 77777, 1, NULL, 1, NULL, NULL),
(29, 5, 3, 1, 'G7', NULL, '7', '2018-05-02 10:58:26', NULL, NULL, NULL, 77777, 0, NULL, NULL, 'G6', '2'),
(30, 6, 4, 1, 'G4', '4', '4', '2018-05-03 11:30:26', NULL, 'surat ini saya tolak', 'G3', 74444, 1, NULL, 1, NULL, NULL),
(31, 6, 3, 1, 'G4', NULL, '4', '2018-05-03 11:30:08', NULL, NULL, NULL, 74444, 0, NULL, NULL, 'G3', '2'),
(32, 7, 4, 1, 'G4', '4', '4', '2018-05-03 14:16:44', NULL, 'ini adalah pesan tolak surat', 'G3', 74444, 1, NULL, 1, NULL, NULL),
(33, 7, 3, 1, 'G4', NULL, '4', '2018-05-03 13:44:11', NULL, NULL, NULL, 74444, 0, NULL, NULL, 'G3', '2'),
(34, 8, 4, 1, 'G4', '4', '4', '2018-05-03 14:42:02', NULL, 'pesan tolaknya', 'G3', 74444, 0, NULL, 1, NULL, NULL),
(35, 8, 3, 1, 'G4', NULL, '4', '2018-05-03 14:21:42', NULL, NULL, NULL, 74444, 0, NULL, NULL, 'G3', '99'),
(36, 10, 2, 1, 'G6', NULL, '7', NULL, NULL, NULL, 'G7', 77777, 0, NULL, NULL, NULL, NULL),
(37, 10, 3, 1, 'G7', NULL, '7', '2018-06-08 15:17:41', NULL, NULL, NULL, 77777, 0, NULL, NULL, 'G6', '2'),
(38, 11, 2, 1, 'G6', NULL, '7', NULL, NULL, NULL, 'G7', 77777, 0, NULL, NULL, NULL, NULL),
(39, 11, 3, 1, 'G7', NULL, '7', '2018-06-08 15:28:33', NULL, NULL, NULL, 77777, 0, NULL, NULL, 'G6', '2'),
(40, 12, 2, 1, 'G6', NULL, '7', NULL, NULL, NULL, 'G7', 77777, 0, NULL, NULL, NULL, NULL),
(41, 12, 3, 1, 'G7', NULL, '7', '2018-06-08 15:32:22', NULL, NULL, NULL, 77777, 0, NULL, NULL, 'G6', '2');

-- --------------------------------------------------------

--
-- Table structure for table `ppof_tbl_generate`
--

CREATE TABLE `ppof_tbl_generate` (
  `id` int(11) NOT NULL,
  `id_surat` int(11) DEFAULT NULL,
  `generate_number` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppof_tbl_generate`
--

INSERT INTO `ppof_tbl_generate` (`id`, `id_surat`, `generate_number`) VALUES
(1, 1, 'UIT/1'),
(2, 2, 'HRD/2'),
(3, 3, 'HRD/3'),
(4, 4, 'HRD/4'),
(5, 9, 'UIT/9');

-- --------------------------------------------------------

--
-- Table structure for table `ppof_tbl_group`
--

CREATE TABLE `ppof_tbl_group` (
  `id_group` varchar(60) NOT NULL,
  `group_name` varchar(60) NOT NULL,
  `group_suppervisor` varchar(60) NOT NULL,
  `group_level` int(11) NOT NULL,
  `id_div` int(11) NOT NULL,
  `idx` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppof_tbl_group`
--

INSERT INTO `ppof_tbl_group` (`id_group`, `group_name`, `group_suppervisor`, `group_level`, `id_div`, `idx`) VALUES
('G1', 'Kepala Bagian', '', 1, 1, 1),
('G2', 'Manager Kelompok', 'G1', 2, 1, 2),
('G3', 'Manager 2', 'G2', 2, 1, 3),
('G4', 'Asisten Manager 1', 'G3', 3, 1, 4),
('G5', 'Asisten Manager 2', 'G3', 3, 1, 5),
('G6', 'Manager Kelompok 2', 'G1', 2, 1, 6),
('G7', 'Asisten Kelompok 2', 'G6', 3, 1, 7),
('G8', 'Pimpinan HRD', '', 1, 2, 8),
('G9', 'Koordinator HRD', 'G8', 2, 2, 9),
('G10', 'Manager HRD', 'G9', 2, 2, 10),
('G11', 'Manager HRD 2', 'G9', 2, 2, 11),
('G12', 'Asisten Mgr HRD', 'G10', 3, 2, 12);

-- --------------------------------------------------------

--
-- Table structure for table `ppof_tbl_mastermenu`
--

CREATE TABLE `ppof_tbl_mastermenu` (
  `id` int(11) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `url` varchar(60) NOT NULL,
  `parent` int(11) NOT NULL,
  `submenu` int(11) DEFAULT NULL,
  `urutan` int(11) NOT NULL,
  `icons` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppof_tbl_mastermenu`
--

INSERT INTO `ppof_tbl_mastermenu` (`id`, `nama`, `url`, `parent`, `submenu`, `urutan`, `icons`) VALUES
(1, 'Buat Surat', 'c_surat', 0, NULL, 1, 'fa fa-edit'),
(2, 'Inbox', '#', 1, NULL, 22, 'fa fa-inbox'),
(3, 'Sent Item', 'c_sentitem', 0, NULL, 5, 'fa fa-share'),
(4, 'Disposisi', '#', 2, 1, 2, 'fa fa-sort-amount-asc'),
(5, 'Approval', 'c_approval', 0, NULL, 3, 'fa fa-folder-open'),
(6, 'Sudah Approve', 'c_approval/done', 0, NULL, 4, 'fa fa-check-square-o'),
(7, 'Inbox', 'c_inbox', 0, NULL, 2, 'fa fa-inbox'),
(8, 'Disposisi', 'c_disposisi', 0, NULL, 7, 'fa fa-sort-amount-asc'),
(9, 'Draft Surat', 'c_draft', 0, NULL, 8, 'fa fa-save'),
(10, 'Employee', 'c_employee', 0, NULL, 9, 'fa fa-user');

-- --------------------------------------------------------

--
-- Table structure for table `ppof_tbl_menuprevilege`
--

CREATE TABLE `ppof_tbl_menuprevilege` (
  `id` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_previlege` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppof_tbl_menuprevilege`
--

INSERT INTO `ppof_tbl_menuprevilege` (`id`, `id_menu`, `id_previlege`) VALUES
(1, 1, 1),
(3, 3, 1),
(6, 1, 2),
(8, 3, 2),
(10, 5, 1),
(11, 7, 1),
(12, 7, 2),
(13, 8, 1),
(14, 8, 2),
(15, 5, 2),
(16, 1, 3),
(17, 7, 3),
(18, 3, 3),
(19, 6, 1),
(20, 6, 2),
(21, 9, 3),
(22, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ppof_tbl_surat`
--

CREATE TABLE `ppof_tbl_surat` (
  `id` int(11) NOT NULL,
  `tipe_surat` varchar(255) DEFAULT NULL,
  `subyek_surat` varchar(255) DEFAULT NULL,
  `content` text,
  `tgl_create` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppof_tbl_surat`
--

INSERT INTO `ppof_tbl_surat` (`id`, `tipe_surat`, `subyek_surat`, `content`, `tgl_create`) VALUES
(1, 'surat', 'daritek satu', 'content', '2018-04-29 21:37:12'),
(2, 'surat', 'testing untuk disposisi', 'content disposisi', '2018-04-30 14:11:54'),
(3, 'surat', 'Dari HR yang pertama', 'content yang pertama dari HR', '2018-05-01 21:40:31'),
(4, 'surat', 'kirim hcd yang kedua', 'content yang kedua', '2018-05-01 21:42:10'),
(5, 'surat', 'coba tolak', 'content coba tolak', '2018-05-02 10:58:26'),
(6, 'surat', 'testing tolak 1', 'testing tolak 1', '2018-05-03 11:30:08'),
(7, 'surat', 'subject coba tolak dengan update', 'contentnnya', '2018-05-03 13:44:11'),
(8, 'surat', 'cek subject', 'cek content', '2018-05-03 14:21:42'),
(9, 'surat', 'Google Translate', '<p xss=removed>I know this is an old thread and that you\'ve picked an answer, but I thought I\'d post this as it is relevant for anyone else that is currently looking.</p><p xss=removed>There is no reason to create new CSS rules, simply undo the current rules and the borders will disappear.</p><p xss=removed>I know this is an old thread and that you\'ve picked an answer, but I thought I\'d post this as it is relevant for anyone else that is currently looking.</p><p xss=removed>There is no reason to create new CSS rules, simply undo the current rules and the borders will disappear.</p><p xss=removed>I know this is an old thread and that you\'ve picked an answer, but I thought I\'d post this as it is relevant for anyone else that is currently looking.</p><p xss=removed>There is no reason to create new CSS rules, simply undo the current rules and the borders will disappear.</p><p xss=removed>I know this is an old thread and that you\'ve picked an answer, but I thought I\'d post this as it is relevant for anyone else that is currently looking.</p><p xss=removed>There is no reason to create new CSS rules, simply undo the current rules and the borders will disappear.</p><p xss=removed>I know this is an old thread and that you\'ve picked an answer, but I thought I\'d post this as it is relevant for anyone else that is currently looking.</p><p xss=removed>There is no reason to create new CSS rules, simply undo the current rules and the borders will disappear.</p>', '2018-05-04 16:59:17'),
(10, 'surat', 'Test Kirim Surat', '<p>Test Kirim Surat<br></p>', '2018-06-08 15:17:41'),
(11, 'surat', 'Test Kirim Surat Draft', '<p>Test Kirim Surat Draft<br></p>', '2018-06-08 15:28:33'),
(12, 'surat', 'Kirim draft surat ke HCD', '<p>Kirim draft surat ke HCD<br></p>', '2018-06-08 15:32:22');

-- --------------------------------------------------------

--
-- Table structure for table `ppof_tbl_surat_draft`
--

CREATE TABLE `ppof_tbl_surat_draft` (
  `id` int(11) NOT NULL,
  `creator` int(11) NOT NULL,
  `tipe_surat` varchar(255) DEFAULT NULL,
  `subyek_surat` varchar(255) DEFAULT NULL,
  `content` text,
  `tgl_create` datetime DEFAULT NULL,
  `tgl_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppof_tbl_surat_draft`
--

INSERT INTO `ppof_tbl_surat_draft` (`id`, `creator`, `tipe_surat`, `subyek_surat`, `content`, `tgl_create`, `tgl_update`) VALUES
(1, 7, 'surat', 'Test Draft', '<p><span xss=removed>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p><p><span xss=removed>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p><p><span xss=removed>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?</span></p><p><span xss=removed>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</span><span xss=removed><br></span></p><p><span xss=removed>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.</span><span xss=removed><br></span><br></p>', '2018-06-30 09:42:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ppof_tbl_tujuan`
--

CREATE TABLE `ppof_tbl_tujuan` (
  `id` int(11) NOT NULL,
  `id_div` int(11) DEFAULT NULL,
  `flag_tujuan` int(11) DEFAULT NULL,
  `flag_read` int(11) DEFAULT '0',
  `id_surat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppof_tbl_tujuan`
--

INSERT INTO `ppof_tbl_tujuan` (`id`, `id_div`, `flag_tujuan`, `flag_read`, `id_surat`) VALUES
(1, 1, 1, 1, 1),
(2, 3, 2, 0, 1),
(3, 2, 1, 1, 2),
(4, 3, 1, 0, 2),
(5, 1, 1, 1, 3),
(6, 3, 2, 0, 3),
(7, 2, 1, 1, 1),
(8, 3, 2, 0, 1),
(9, 1, 1, 1, 2),
(10, 1, 1, 1, 3),
(11, 1, 1, 1, 4),
(12, 2, 1, 0, 5),
(13, 3, 2, 0, 5),
(14, 2, 1, 0, 6),
(15, 3, 2, 0, 6),
(16, 2, 1, 0, 7),
(17, 3, 1, 0, 7),
(18, 2, 1, 0, 8),
(19, 3, 2, 0, 8),
(20, 2, 1, 1, 9),
(21, 2, 1, 0, 10),
(22, 4, 1, 0, 10),
(23, 3, 2, 0, 10),
(24, 3, 1, 0, 11),
(25, 5, 2, 0, 11),
(26, 2, 1, 0, 12),
(27, 5, 2, 0, 12),
(28, 4, 2, 0, 12);

-- --------------------------------------------------------

--
-- Table structure for table `ppof_tbl_tujuan_draft`
--

CREATE TABLE `ppof_tbl_tujuan_draft` (
  `id` int(11) NOT NULL,
  `id_div` int(11) DEFAULT NULL,
  `flag_tujuan` int(11) NOT NULL,
  `id_surat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppof_tbl_tujuan_draft`
--

INSERT INTO `ppof_tbl_tujuan_draft` (`id`, `id_div`, `flag_tujuan`, `id_surat`) VALUES
(1, 2, 1, 1),
(2, 4, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ppof_tbl_upload`
--

CREATE TABLE `ppof_tbl_upload` (
  `id` int(11) NOT NULL,
  `id_surat` int(11) DEFAULT NULL,
  `nama_file` varchar(255) DEFAULT NULL,
  `ukuran_file` varchar(255) DEFAULT NULL,
  `month` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ppof_tbl_upload_draft`
--

CREATE TABLE `ppof_tbl_upload_draft` (
  `id` int(11) NOT NULL,
  `id_surat` int(11) DEFAULT NULL,
  `nama_file` varchar(255) DEFAULT NULL,
  `ukuran_file` varchar(255) DEFAULT NULL,
  `month` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_email` varchar(255) NOT NULL DEFAULT '',
  `user_pass` varchar(60) NOT NULL DEFAULT '',
  `user_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_email`, `user_pass`, `user_date`, `user_modified`, `user_last_login`) VALUES
(1, '712712', '$2a$08$b9.wcHIav.HkxVVKEv2sQOuDWWilLtQGp2ZnlBDKC.bJJEUKa13CK', '2017-04-08 20:30:06', '2017-04-08 20:30:06', '2018-07-05 19:25:19'),
(2, '71313', '$2a$08$b9.wcHIav.HkxVVKEv2sQOuDWWilLtQGp2ZnlBDKC.bJJEUKa13CK', '2017-04-22 18:57:56', '2017-04-22 18:57:56', '2018-05-05 09:21:18'),
(3, '73333', '$2a$08$b9.wcHIav.HkxVVKEv2sQOuDWWilLtQGp2ZnlBDKC.bJJEUKa13CK', '2017-05-07 13:28:53', '2017-05-07 13:28:53', '2018-05-05 09:20:28'),
(4, '74444', '$2a$08$eiJsFm/xAsPo.013S/6o8.RkMxhMvILJ/FZq5FWTWgG/B4ohOMK9e', '2017-05-07 17:55:28', '2018-05-05 21:54:17', '2018-05-05 21:53:56'),
(5, '75555', '$2a$08$b9.wcHIav.HkxVVKEv2sQOuDWWilLtQGp2ZnlBDKC.bJJEUKa13CK', '2017-05-07 17:55:38', '2017-05-07 17:55:38', '2017-08-01 21:16:07'),
(6, '76666', '$2a$08$b9.wcHIav.HkxVVKEv2sQOuDWWilLtQGp2ZnlBDKC.bJJEUKa13CK', '2017-05-07 17:55:45', '2017-05-07 17:55:45', '2018-05-02 10:58:42'),
(7, '77777', '$2a$08$b9.wcHIav.HkxVVKEv2sQOuDWWilLtQGp2ZnlBDKC.bJJEUKa13CK', '2017-05-07 17:55:51', '2017-05-07 17:55:51', '2018-07-02 09:54:40'),
(8, '88888', '$2a$08$b9.wcHIav.HkxVVKEv2sQOuDWWilLtQGp2ZnlBDKC.bJJEUKa13CK', '2017-05-07 22:53:10', '2017-05-07 22:53:10', '2018-07-02 10:59:30'),
(9, '81111', '$2a$08$b9.wcHIav.HkxVVKEv2sQOuDWWilLtQGp2ZnlBDKC.bJJEUKa13CK', '2017-05-07 22:53:16', '2017-05-07 22:53:16', '2018-07-02 10:49:45'),
(10, '82222', '$2a$08$b9.wcHIav.HkxVVKEv2sQOuDWWilLtQGp2ZnlBDKC.bJJEUKa13CK', '2017-05-07 22:53:21', '2017-05-07 22:53:21', '2018-05-05 18:58:19'),
(11, '83333', '$2a$08$b9.wcHIav.HkxVVKEv2sQOuDWWilLtQGp2ZnlBDKC.bJJEUKa13CK', '2017-05-07 22:53:24', '2017-05-07 22:53:24', '2018-05-05 18:58:41'),
(12, '84444', '$2a$08$b9.wcHIav.HkxVVKEv2sQOuDWWilLtQGp2ZnlBDKC.bJJEUKa13CK', '2017-05-07 22:53:28', '2017-05-07 22:53:28', '2018-04-29 19:41:57'),
(13, '85555', '$2a$08$b9.wcHIav.HkxVVKEv2sQOuDWWilLtQGp2ZnlBDKC.bJJEUKa13CK', '2017-05-07 22:55:02', '2017-05-07 22:55:02', '2017-05-10 10:37:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`pic_id`);

--
-- Indexes for table `ppof_tbl_div`
--
ALTER TABLE `ppof_tbl_div`
  ADD PRIMARY KEY (`id_div`);

--
-- Indexes for table `ppof_tbl_emp`
--
ALTER TABLE `ppof_tbl_emp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ppof_tbl_flow`
--
ALTER TABLE `ppof_tbl_flow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ppof_tbl_generate`
--
ALTER TABLE `ppof_tbl_generate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ppof_tbl_group`
--
ALTER TABLE `ppof_tbl_group`
  ADD PRIMARY KEY (`idx`);

--
-- Indexes for table `ppof_tbl_mastermenu`
--
ALTER TABLE `ppof_tbl_mastermenu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `ppof_tbl_menuprevilege`
--
ALTER TABLE `ppof_tbl_menuprevilege`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ppof_tbl_surat`
--
ALTER TABLE `ppof_tbl_surat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ppof_tbl_surat_draft`
--
ALTER TABLE `ppof_tbl_surat_draft`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ppof_tbl_tujuan`
--
ALTER TABLE `ppof_tbl_tujuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ppof_tbl_tujuan_draft`
--
ALTER TABLE `ppof_tbl_tujuan_draft`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ppof_tbl_upload`
--
ALTER TABLE `ppof_tbl_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ppof_tbl_upload_draft`
--
ALTER TABLE `ppof_tbl_upload_draft`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `pic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ppof_tbl_div`
--
ALTER TABLE `ppof_tbl_div`
  MODIFY `id_div` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ppof_tbl_emp`
--
ALTER TABLE `ppof_tbl_emp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `ppof_tbl_flow`
--
ALTER TABLE `ppof_tbl_flow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `ppof_tbl_generate`
--
ALTER TABLE `ppof_tbl_generate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ppof_tbl_group`
--
ALTER TABLE `ppof_tbl_group`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `ppof_tbl_mastermenu`
--
ALTER TABLE `ppof_tbl_mastermenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `ppof_tbl_menuprevilege`
--
ALTER TABLE `ppof_tbl_menuprevilege`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `ppof_tbl_surat`
--
ALTER TABLE `ppof_tbl_surat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `ppof_tbl_surat_draft`
--
ALTER TABLE `ppof_tbl_surat_draft`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ppof_tbl_tujuan`
--
ALTER TABLE `ppof_tbl_tujuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `ppof_tbl_tujuan_draft`
--
ALTER TABLE `ppof_tbl_tujuan_draft`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ppof_tbl_upload`
--
ALTER TABLE `ppof_tbl_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ppof_tbl_upload_draft`
--
ALTER TABLE `ppof_tbl_upload_draft`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
