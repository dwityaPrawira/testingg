/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

    config.toolbar = [
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
        { name: 'links', items: [ 'Link', 'Unlink' ] },
		{ name: 'insert', items: [ 'Table' ] }
    ];

	// Default setting.
//	config.toolbarGroups = [
//        { 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' },
	    // { name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
	    //{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
	    // { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
	    // { name: 'forms' },
	    //'/',
//	    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
//	    { name: 'paragraph',   groups: [ 'list', 'indent'] },
	    // { name: 'links' },
	    // { name: 'insert' },
//	    '/',
	    // { name: 'styles' },
	    // { name: 'colors' },
	    // { name: 'tools' },
	    // { name: 'others' },
	    // { name: 'about' }
//	];
};

