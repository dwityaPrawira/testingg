$(document).ready(function() {
    $('#calendarIO').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        allDay : true,
        timeFormat: 'h:mm',
        displayEventTime: false,
        defaultDate: moment().format('YYYY-MM-DD'),
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        selectable: true,
        selectHelper: true,
        select: function(start, end) {
            $('#calendarIO').fullCalendar('unselect');
        },
        eventDrop: function(event, delta, revertFunc) { // si changement de position
            editDropResize(event);
        },
        eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur
            editDropResize(event);
        },
        eventClick: function(event, element)
        {
            detail(event);
        },
        events: JSON.parse(get_data)
    });

    // $('#calendarMobile').fullCalendar({
    //     header: {
    //         left: 'prev,next today',
    //         center: 'title',
    //         right: 'month,basicWeek,basicDay'
    //     },
    //     defaultView: 'basicDay',
    //     allDay : true,
    //     timeFormat: 'h:mm',
    //     displayEventTime: false,
    //     defaultDate: moment().format('YYYY-MM-DD'),
    //     editable: true,
    //     eventLimit: true, // allow "more" link when too many events
    //     selectable: true,
    //     selectHelper: true,
    //     select: function(start, end) {
    //         $('#calendarIO').fullCalendar('unselect');
    //     },
    //     eventDrop: function(event, delta, revertFunc) { // si changement de position
    //         editDropResize(event);
    //     },
    //     eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur
    //         editDropResize(event);
    //     },
    //     eventClick: function(event, element)
    //     {
    //         detail(event);
    //     },
    //     events: JSON.parse(get_data)
    // });
});

function detail(event)
{
    $('#create_modal input[name=calendar_id]').val(event.id);

    if(event.end === null){
        $('#create_modal input[name=start_date]').val(moment(event.start).format('YYYY-MM-DD'));
    } else {
        $('#create_modal input[name=start_date]').val(moment(event.start).format('YYYY-MM-DD').concat(' - ', moment(event.end).format('YYYY-MM-DD')));
    }
    $('#create_modal input[name=end_date]').val(moment(event.end).format('YYYY-MM-DD'));
    $('#create_modal input[name=title]').val(event.title);
    $('#create_modal input[name=description]').val(event.description);
    $('#create_modal input[name=waktuawal]').val(event.waktuawal.concat(' - ', event.waktuakhir));
    $('#create_modal input[name=biro]').val(event.biro);
    $('#create_modal input[name=dukungan]').val(event.dukungan);
    $('#create_modal select[name=color]').val(event.color);
    $('#create_modal .delete_calendar').show();
    $('#create_modal').modal('show');
}