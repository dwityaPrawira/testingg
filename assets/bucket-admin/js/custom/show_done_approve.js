$(document).ready(function() {
    $('#summernote').summernote({
        height: 300,
        toolbar: [
            ['style', ['bold', 'italic', 'underline']]
        ]
    });
});